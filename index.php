<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
/****************************************/
/* required	first						*/
if (file_exists("config.php")) {
	require_once("config.php");
} else {
	$_ = "<h2>Config.php don't exist !</h2>";
	$_ .= "<p><a href=\"install.php\">Install Ulyxex ?</a></p>";
	$_ .= "<h3 style='color:red'>Delete install.php file after installation to prevent undesirable manipulations !</h3>";
	$_ = "<html><body>".$_."</body></html>";
	echo $_;
	exit; 
}
require_once("pages/core/ulyxex.php");
/****************************************/
/* constants                            */
$u = New Ulyxex();
$home = $u->site_params();
define("ARTICLESPAGE",	$home['ARTICLESPAGE']);
define("SITETAB",		$home['SITETAB']);
define("HTTPSVAL",		$home['HTTPS']);
/****************************************/
/* required	more						*/
require_once("pages/core/html.php");
require_once("pages/core/translate.php");
require_once("pages/core/tree.php"); 
/****************************************/
/* classes 								*/
$h = new Htmlz();
$t = new Translate();
/****************************************/
/* 	associated types and values			*/
$typePage = (isset($_GET['k']))?$_GET['k']:((isset($_POST['k']))?$_POST['k']:Null);
if ($typePage) $typePage = $h->word($typePage);
$paramId = (isset($_GET['v']))?$_GET['v']:((isset($_POST['v']))?$_POST['v']:Null);
if ($paramId) $paramId = $h->word($paramId);

/* params via menu select 				*/
$paramsViaMenu = (isset($_GET['navSelect']))?$_GET['navSelect']:((isset($_POST['navSelect']))?$_POST['navSelect']:Null);
if ($paramsViaMenu) {
	$_ = explode(",",$paramsViaMenu);
	if (count($_) == 2) { // check if have 2 params and from select menu
		$typePage = $h->word($_[0]);
		$paramId = $h->word($_[1]);
	}

}
/****************************************/
/* you are in front end					*/
$index = 1;
/* add controllers 						*/
require_once("pages/core/index_controllers.php");

/****************************************/
/* proceed								*/
if (array_key_exists($typePage,$viewController)){
	require_once($viewController[$typePage]);
} else { // default
	$typePage	= $home['TYPEPAGE'];
	$paramId	= $home['PAGEID'];
	require_once($viewController[$typePage]);
}
?>
