<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
/****************************************/
/* session always before header			*/
session_start();
$_SESSION['countqueries'] = 0;
/****************************************/
/* required	first						*/
require_once("config.php");
require_once("pages/core/ulyxex.php");
/****************************************/
/* constants                            */
$u = New Ulyxex();
$home = $u->site_params();
define("ITEMS",	$home['ITEMS']);
define("HTTPSVAL", $home['HTTPS']);
/****************************************/
/* required	more						*/
require_once("pages/core/html.php");
require_once("pages/core/translate.php");
require_once("pages/core/tree.php");
require_once("pages/core/admin.php");
/****************************************/
/* init classes 						*/
$h = new Htmlz();
$t = new Translate();
$o_admin = new Admin();
/****************************************/
/* login						 		*/
if (isset($_POST['login'])){
	list($_SESSION['userid'],$_SESSION['level'],$admin) = $o_admin->login($h->word($_POST['login']),$h->crypt($_POST['password']));
	if ($_SESSION['userid'] == null || $_SESSION['userid'] == null || $admin == null) {
		/* erase all sessions					*/
		$_SESSION = array();
		/* delete cookies						*/
		if (isset($_COOKIE[session_name()])){
			setcookie(session_name(), '', time()-42000, '/');
		}
		/* destroy sessions						*/
		session_destroy();
		exit($t->w("bad login or password"));
	}
} elseif (isset($_SESSION['userid']) && $_SESSION['userid'] != null && isset($_SESSION['level']) && $_SESSION['level'] != null){
	$admin = 1; // you are already logged in
} else {
	exit("xxx !");
}
/****************************************/
/* catch action							*/
$action =(isset($_GET['action']))?$h->word($_GET['action']):null;

/* init controllers 						*/
require_once("pages/core/admin_controllers.php");

/* create menu (actions links)	 		*/
$adminNavigation = new AdminNavigation($adminController);
/****************************************/
/* proceed								*/
if ( $action && array_key_exists($action , $adminController) ){
	require_once($adminController[$action][1]);
} else {
	/* default action */
	if ($_SESSION['level'] > 0 && $_SESSION['level'] <= 4) {
		require_once("pages/admin_articles.php");
	} else {
		exit("error level !");
	}
}
?>
