-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 10 Juin 2014 à 18:10
-- Version du serveur: 5.6.11-log
-- Version de PHP: 5.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `ulyxex`
--
-- CREATE DATABASE IF NOT EXISTS `ulyxex` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
-- USE `ulyxex`;

-- --------------------------------------------------------

--
-- Structure de la table `uly_files`
--

CREATE TABLE IF NOT EXISTS `uly_files` (
  `ID` mediumint(6) NOT NULL AUTO_INCREMENT,
  `USERID` mediumint(6) NOT NULL,
  `FILENAME` mediumtext NOT NULL,
  `DATEFILE` datetime NOT NULL, 
  PRIMARY KEY (`ID`),
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Structure de la table `uly_articles`
--

CREATE TABLE IF NOT EXISTS `uly_articles` (
  `ID` mediumint(6) NOT NULL AUTO_INCREMENT,
  `DATEPAGE` datetime NOT NULL,
  `SUBJECT` text NOT NULL, 
  `CONTENT` mediumtext NOT NULL,
  `LANG` VARCHAR(2) NOT NULL DEFAULT 'en',
  `HIDE` tinyint(1) NOT NULL,
  `PAGEID` mediumint(6) NOT NULL, 
  `USERID` mediumint(6) NOT NULL,
  PRIMARY KEY (`ID`),
  FULLTEXT KEY `SUBJECT` (`SUBJECT`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;

--
-- Contenu de la table `uly_articles`
--

INSERT INTO `uly_articles` (`ID`, `SUBJECT`, `USERID`, `DATE`, `CONTENT`, `HIDE`, `PAGEID`) VALUES
(1, 'hello', 1, '2014-05-29 12:05:44', '<p>hello wold</p>', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `uly_home_params`
--

CREATE TABLE `uly_home_params` (
  `ID` mediumint(9) NOT NULL,
  `TYPEPAGE` NOT NULL DEFAULT 'page',
  `PAGEID` mediumint(6) NOT NULL,
  `CSS` varchar(255) NOT NULL DEFAULT 'css/view_style.css',
  `ARTICLESPAGE` tinyint(4) NOT NULL DEFAULT '10',
  `ITEMS` tinyint(4) NOT NULL DEFAULT '10',
  `SANDBOXID` mediumint(6) NOT NULL,
  `MAIL` mediumtext NOT NULL,
  `NAVTYPE` varchar(32) NOT NULL DEFAULT 'tree',
  `SITETAB` varchar(6) NOT NULL DEFAULT '_'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `uly_home_params`
--

INSERT INTO `uly_home_params` (`ID`, `TYPEPAGE`, `PAGEID`, `CSS`, `ARTICLESPAGE`, `ITEMS`, `SANDBOXID`, `MAIL`, `NAVTYPE`, `SITETAB`) VALUES
(1, 'page', 1, 'css/view_style.css', 10, 10, 2, '', 'tree', '_');

-- --------------------------------------------------------

--
-- Structure de la table `uly_pages`
--

CREATE TABLE IF NOT EXISTS `uly_pages` (
  `ID` mediumint(6) NOT NULL AUTO_INCREMENT,
  `DATEPAGE` datetime NOT NULL,
  `NAME` mediumtext NOT NULL,
  `DESCRIPTION` mediumtext NOT NULL,
  `PAGEHEAD` mediumtext NOT NULL,
  `TYPEPAGE` VARCHAR(32) NOT NULL DEFAULT 'page',
  `LANG` VARCHAR(2) NOT NULL DEFAULT 'en',
  `PARENTID` mediumint(6) NOT NULL DEFAULT '0',
  `SORTSTYLE` tinyint(1) NOT NULL DEFAULT '0',
  `HIDE` tinyint(1) NOT NULL,
  `USERID` mediumint(6) NOT NULL,
  PRIMARY KEY (`ID`),
  FULLTEXT KEY `NAME` (`NAME`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

--
-- Contenu de la table `uly_pages`
--

INSERT INTO `uly_pages` (`ID`, `NAME`, `DESCRIPTION`, `HIDE`, `USERID`) VALUES
(1, 'Home', 'Description', 0, 1),
(2, 'SANDBOX', 'Training category', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `uly_users`
--

CREATE TABLE IF NOT EXISTS `uly_users` (
  `ID` mediumint(6) NOT NULL AUTO_INCREMENT,
  `DATEPAGE` datetime NOT NULL,
  `USERNAME` text NOT NULL,
  `DESCRIPTION` longtext NOT NULL,
  `MAIL` mediumtext NOT NULL,
  `USERLEVEL` tinyint(1) NOT NULL,
  `LOGIN` mediumtext NOT NULL,
  `PASSWORD` mediumtext NOT NULL,
  `USEROWNER` mediumint(6) NOT NULL,
  PRIMARY KEY (`ID`),
  FULLTEXT KEY `USERNAME` (`USERNAME`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `uly_users`
--

INSERT INTO `uly_users` (`ID`, `USERNAME`, `DESCRIPTION`, `MAIL`, `USERLEVEL`, `LOGIN`, `USEROWNER`, `PASSWORD`) VALUES
(1, 'admin', 'super user to admin all.', 'none', 1, 'admin', 1, 'd033e22ae348aeb5660fc2140aec35850c4da997');

-- --------------------------------------------------------

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
