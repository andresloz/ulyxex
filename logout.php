<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
/****************************************/
/* clean all sessions 					*/
/* session always before header			*/
session_start();
/* erase all sessions					*/
$_SESSION = array();
/* delete cookies						*/
if (isset($_COOKIE[session_name()])){
	setcookie(session_name(), '', time()-42000, '/');
}
/* destroy sessions						*/
session_destroy();
/****************************************/
/* required								*/
require_once("config.php");
require_once("pages/core/ulyxex.php");
require_once("pages/core/html.php");
require_once("pages/core/translate.php");
/****************************************/
/* page elements						*/
$h = new Htmlz();
$t = new Translate();
$css = $h->css("css/admin_style.css","external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title("logout");
$head = $h->head($ico.$meta.$css.$title);

$u = New Ulyxex();
$home = $u->site_params();
define("HTTPSVAL",	$home['HTTPS']);

$topPage = $h->h1($t->w("logout done"));

$bottomPage = $h->ulyxCredits();
/****************************************/
/* page data							*/	

$data = $h->h3($t->wr("home").$h->ahref($h->root_url(),$h->root_url()));
/****************************************/
/* show									*/
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
