<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (file_exists("config.php")) {
	exit;
}
require_once("pages/core/ulyxex.php");
require_once("pages/core/translate.php");
require_once("pages/core/html.php");
define("L",					0);

/****************************************/
/* page elements						*/
$now = time();
$date = date("Y-m-d H:i:s",$now);
$h = new Htmlz();
$t = new Translate();
$css = $h->css("css/view_style.css","external");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title("Install Ulyxex");
$head = $h->head($meta.$css.$title);

$topPage = $h->p("Web site ".$h->ahref($h->root_url(),$h->root_url()));
$data = $h->h2("Install Ulyxex");
if (isset($_POST['host']) && isset($_POST['database']) && isset($_POST['user']) && isset($_POST['prefix']) && isset($_POST['adminpassword'])) {
	$data .= $h->h3("Don't forget to delete \"intall.php\" after installation","style='color:red'");
	$data .= $h->h3("If install fails then delete \"config.php\" and try again", "style='color:red'");
	
	$sqlQuieries = array();
	$hostMySql = $_POST['host'];
	$baseMySql = $_POST['database'];
	$userMySql = $_POST['user'];
	
	if (isset($_POST['sqlpassword'])) {
		$passMySql = $_POST['sqlpassword'];
	} else {
		$passMySql = ""; // avoid to crypt empty field
	}
	
	$prefixTableMySql = $_POST['prefix'];
	$adminPassword = $h->crypt($_POST['adminpassword']);
	
	/* Constants							*/
	define("HOSTMYSQL",			$hostMySql);
	define("BASEMYSQL",			$baseMySql);
	define("USERMYSQL",			$userMySql);
	define("PASSMYSQL",			$passMySql);
	
//~ en theorie la base existe déjà
//~ $sqlQuieries[] = <<<SQLVAL
//~ CREATE DATABASE IF NOT EXISTS $baseMySql DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
//~ USE $baseMySql;
//~ SQLVAL;
	
	$table = $prefixTableMySql."_articles";
	$sqlQuieries[] = <<<SQLVAL
CREATE TABLE IF NOT EXISTS $table (
  ID mediumint(6) NOT NULL AUTO_INCREMENT,
  DATEPAGE datetime NOT NULL,
  SUBJECT text NOT NULL, 
  CONTENT mediumtext NOT NULL,
  LANG VARCHAR(2) NOT NULL DEFAULT 'en',
  HIDE tinyint(1) NOT NULL,
  PAGEID mediumint(6) NOT NULL, 
  USERID mediumint(6) NOT NULL,
  PRIMARY KEY (ID),
  FULLTEXT KEY SUBJECT (SUBJECT)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;
SQLVAL;
	
	$sqlQuieries[] = <<<SQLVAL
INSERT INTO $table (ID, SUBJECT, USERID, DATEPAGE, CONTENT, HIDE, PAGEID) VALUES
(1, 'hello', 1, '$date', '<p>hello wold</p>', 0, 1);
SQLVAL;
	
	$table = $prefixTableMySql."_home_params";
	$sqlQuieries[] = <<<SQLVAL
CREATE TABLE IF NOT EXISTS $table (
  ID mediumint(6) NOT NULL AUTO_INCREMENT,
  PAGEID mediumint(6) NOT NULL,
  TYPEPAGE VARCHAR(32) NOT NULL DEFAULT 'page',
  NAVTYPE VARCHAR(32) NOT NULL DEFAULT 'tree',
  SITETAB VARCHAR(6) NOT NULL DEFAULT '_',
  CSS VARCHAR(255) NOT NULL DEFAULT 'css/view_style.css',
  ARTICLESPAGE TINYINT NOT NULL DEFAULT '10',
  ITEMS TINYINT NOT NULL DEFAULT '10',
  SANDBOXID mediumint(6) NOT NULL,
  MAIL mediumtext NOT NULL,
  HTTPS TINYINT NOT NULL DEFAULT '0',
  PRIMARY KEY (ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;
SQLVAL;
 
	$sqlQuieries[] = <<<SQLVAL
INSERT INTO $table (ID, PAGEID, MAIL, SANDBOXID) VALUES
(1, 1, 'none@none.org', 2);
SQLVAL;
 
	$table = $prefixTableMySql."_pages";
	$sqlQuieries[] = <<<SQLVAL
CREATE TABLE IF NOT EXISTS $table (
  ID mediumint(6) NOT NULL AUTO_INCREMENT,
  DATEPAGE datetime NOT NULL,
  NAME mediumtext NOT NULL,
  DESCRIPTION mediumtext NOT NULL,
  PAGEHEAD mediumtext NOT NULL,
  TYPEPAGE VARCHAR(32) NOT NULL DEFAULT 'page',
  LANG VARCHAR(2) NOT NULL DEFAULT 'en',
  LINK tinytext NOT NULL,
  PARENTID mediumint(6) NOT NULL DEFAULT '0',
  SORTSTYLE tinyint(1) NOT NULL DEFAULT '0',
  HIDE tinyint(1) NOT NULL,
  USERID mediumint(6) NOT NULL,
  PRIMARY KEY (ID),
  FULLTEXT KEY NAME (NAME)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;
SQLVAL;
 
	$sqlQuieries[] = <<<SQLVAL
INSERT INTO $table (ID, NAME, DESCRIPTION, PAGEHEAD, LINK, DATEPAGE, HIDE, USERID) VALUES
(1, 'Home', 'Description', '', '', '$date', 0, 1),
(2, 'SANDBOX', 'Training category', '', '', '$date', 1, 1);
SQLVAL;
 
	$table = $prefixTableMySql."_users";
	$sqlQuieries[] = <<<SQLVAL
CREATE TABLE IF NOT EXISTS $table (
  ID mediumint(6) NOT NULL AUTO_INCREMENT,
  DATEPAGE datetime NOT NULL,
  USERNAME text NOT NULL,
  DESCRIPTION longtext NOT NULL,
  MAIL mediumtext NOT NULL,
  USERLEVEL tinyint(1) NOT NULL,
  LOGIN mediumtext NOT NULL,
  PASSWORD mediumtext NOT NULL,
  USEROWNER mediumint(6) NOT NULL,
  PRIMARY KEY (ID),
  FULLTEXT KEY USERNAME (USERNAME)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;
SQLVAL;

	$sqlQuieries[] = <<<SQLVAL
INSERT INTO $table (ID, USERNAME, DESCRIPTION, DATEPAGE, MAIL, USERLEVEL, LOGIN, USEROWNER, PASSWORD) VALUES
(1, 'admin', 'super user to admin all.', '$date', 'none', 1, 'admin', 1, '$adminPassword');
SQLVAL;

	$table = $prefixTableMySql."_files";
	$sqlQuieries[] = <<<SQLVAL
CREATE TABLE $table (
  ID mediumint(6) NOT NULL AUTO_INCREMENT,
  USERID mediumint(6) NOT NULL,
  FILENAME mediumtext NOT NULL,
  DATEFILE datetime NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
SQLVAL;

	foreach($sqlQuieries as $query) {
		$dbLink = @mysqli_connect(HOSTMYSQL,USERMYSQL,PASSMYSQL,BASEMYSQL) or die("Error database connection aborted ".HOSTMYSQL.",".USERMYSQL.",".PASSMYSQL.",".BASEMYSQL); 
		$result = $dbLink->query($query);
		if ($result) {
			$data .= $h->p("DONE >>> ".$query, "style='color:green'");
		} else {
			$data .= $h->p("NOT >>>> ".$query, "style='color:red'");
		}
		mysqli_close($dbLink);
	}
	
	// write config file
	$Lines = file("config-example.php");
	foreach($Lines as $Line) {
		// change line if needed
		if (substr($Line, 0, 10) == '$hostMySql') {
			$Line = "\$hostMySql"." = \"".HOSTMYSQL."\";\n";
		}
		if (substr($Line, 0, 10) == '$baseMySql') {
			$Line = "\$baseMySql"." = \"".BASEMYSQL."\";\n";
		}
		if (substr($Line, 0, 10) == '$userMySql') {
			$Line = "\$userMySql"." = \"".USERMYSQL."\";\n";
		}
		if (substr($Line, 0, 10) == '$passMySql') {
			$Line = "\$passMySql"." = \"".PASSMYSQL."\";\n";
		}
		if (substr($Line, 0, 17) == '$prefixTableMySql') {
			$Line = "\$prefixTableMySql"." = \"".$prefixTableMySql."_\";\n";
		}
		$configData[] = $Line;
	}
	$File = fopen("config.php","w") or die("can't open file config.php");
	foreach($configData as $Line) {
		fwrite($File, $Line);
	}
	fclose($File);
	
	$data .= $h->p("web site ".$h->ahref($h->root_url(),$h->root_url()));
	
	/* write infos for updates			*/
	$version = VERSION;
	$date = date("Y-m-d H:i:s",time());
$updateFileContent = <<<END
update=$version
date=$date
END;

	$f = fopen("update.txt","w");
	fwrite($f,$updateFileContent);
	fclose($f);

} else {
	/****************************************/
	/* page data 							*/
	$data .= $h->h3("Don't forget to delete \"intall.php\" after installation","style='color:red'");	
	$data .= $h->h3("SQL settings :", "style='color:black'");
	$data .= $h->p("If install fails delete \"config.php\" and try again", "style='color:red'");
	$inputs = $h->p($t->wr("Sql host").$h->input("text","host","localhost","class='login'"));
	$inputs .= $h->p($t->wr("Sql database").$h->input("text","database","essais","class='login'"));
	$inputs .= $h->p($t->wr("Sql login").$h->input("text","user","root","class='login'"));
	$inputs .= $h->p($t->wr("Sql password").$h->input("text","sqlpassword","","class='login'"));
	$inputs .= $h->p("*****************************************");
	$inputs .= $h->h3("Ulyxex settings :", "style='color:black'");
	$inputs .= $h->p($t->wr("Ulyxex tables prefix").$h->input("text","prefix","uly_".substr(strval(mt_rand()),0,5),"class='login'"));
	$inputs .= $h->p($t->wr("admin").$t->wr("password").$h->input("text","adminpassword","admin","class='login'"));
	$inputs .= $h->input("submit",null,"INSTALL");
	$data .= $h->form($inputs,"admin","post","install.php");
}
$bottomPage = $h->ulyxCredits();

/****************************************/
/* show									*/
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>



