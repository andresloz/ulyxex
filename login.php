<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
/****************************************/
/* clean all sessions 					*/
/* session always before header			*/
session_start();
/* erase all sessions					*/
$_SESSION = array();
/* delete cookies						*/
if (isset($_COOKIE[session_name()])){
	setcookie(session_name(), '', time()-42000, '/');
}
/* destroy sessions						*/
session_destroy();
/****************************************/
/* required								*/
require_once("config.php");
require_once("pages/core/ulyxex.php");
require_once("pages/core/html.php");
require_once("pages/core/translate.php");
/****************************************/
/* page elements						*/
$h = new Htmlz();
$t = new Translate();
$css = $h->css("css/admin_style.css","external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$js = $h->script("js/ulyxex.js","external");
$title = $h->title("login");

$head = $h->head($ico.$meta.$css.$js.$title);

$u = New Ulyxex();
$home = $u->site_params();
define("HTTPSVAL",	$home['HTTPS']);

$topPage = $h->h3($h->ahref($h->root_url(),$h->root_url()));

$bottomPage = $h->ulyxCredits();
/****************************************/
/* page data							*/	
$inputs = $h->p($t->wr("login").$h->input("text","login",null,"class='login'"));
$inputs .= $h->p( $t->wr("psswd").$h->input("password","password",null,"class='login'")." ".$h->input("submit",null,$t->w("Submit")) );
$inputs .= $h->p( $h->input("checkbox","checkbox",null,"onclick=\"showPassword(['password'])\"").$t->wl("show/hide password") );
$data = $h->form($inputs,"admin","post","admin.php");
/****************************************/
/* show									*/
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
