<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
/****************************************/
/*			*	*	*	*	*			*/
/****************************************/
/*		IMPORTANT INFORMATION   		*/
/****************************************/
/* THIS CONFIG SCRIPT WILL BE NEVER		*/
/* MODIFIED BY ULYXEX UPDATE PROCESS	*/
/* NEVER!								*/
/* IF NEEDED YOU WILL MODIFY IT			*/
/* MANUALLY BY YOURSELF					*/
/****************************************/
/*			*	   SQL		*			*/
/****************************************/
/* fill with yours sql paramaters		*/

$hostMySql =				"localhost";
$baseMySql =				"ulyxex";
$userMySql =				"root";
$passMySql =				"password";

/* change if you have multiple sites on	*/
/* the same database					*/

$prefixTableMySql =			"uly_";

/* Constants							*/

define("HOSTMYSQL",			$hostMySql);
define("BASEMYSQL",			$baseMySql);
define("USERMYSQL",			$userMySql); 
define("PASSMYSQL",			$passMySql);
define("PARAMS",			$prefixTableMySql."home_params");
define("ARTICLES",			$prefixTableMySql."articles");
define("PAGES",				$prefixTableMySql."pages");
define("FILES",				$prefixTableMySql."files");
define("USERS",				$prefixTableMySql."users");

/****************************************/
/*			*	CONSTANTS	*			*/
/****************************************/

/* define default langage: 0=english, 	*/
/* 1=french, 2=spanish					*/

define("L",					0);

/* Admin constants						*/

define("FIELDWIDTH",		82);
define("ADMINCSS",			"admin_style.css");

/****************************************/
/*		add more content to ulyxex		*/
/****************************************/
/* default controllers are in scripts	*/
/* pages/core/index_controllers.php and	*/
/* pages/core/admin_controllers.php		*/
/* if you want to add more controllers	*/
/* put them below only in this script 	*/
/*										*/
/* example : add a view controllers		*/
/* open example 'pages/view_example.php'*/
/* and study the structure				*/
/* then add it to dictionnary like this	*/
/* controller entry and php script		*/
/****************************************/

// $viewController["example"] = "pages/view_example.php";

/****************************************/
/* adding adminController needs more	*/
/* configuration, you have to set		*/
/* more elements in an array			*/
/*										*/
/* controller elements array :			*/
/* element [0] = title of action		*/
/* element [1] = php script to work 	*/
/* with or any url						*/
/* element [2] = "action" to add it 	*/
/* as an action in menu or "link" as 	*/
/* a link in menu or "null" 			*/
/* to hide it in menu					*/
/* element [3] = level of user [1,2,3,4]*/
/* to define who can use the controller	*/
/****************************************/

// $adminController["example"]	= array("example","pages/admin_example.php","action",4);
// $adminController["ulyxexsite"]	= array("ulyxex site","http://ulyxex.logz.org","link",4);


?>
