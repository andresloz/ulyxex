# Ulyxex Content Management System

Andres Lozano Gallego a.k.a Loz, 2012-2025  
Copyleft: this work is free, you can copy, distribute and modify it  
under the terms of the Free Art License [http://www.artlibre.org](http://www.artlibre.org)  
[Download latest release](https://ulyxex.logz.org/index.php?k=downloads&v=14)  

## Ulyxex name is inspired by Ulyxes (grec odysseus)

Ulyxex is a web content generator for people who wants an universal  
web site for all devices, Ulyxex is absolutely compatible with all browsers  
since mosaic or netscape 1 (1994) to the latest browsers (included text browser).  
Ulyxex is also usefull with mobile phones or special devices (new and olds  
smartphones, tablets, barebones and so on).  

If you can access internet then you can create and edit your posts.

Features : 100% every navigator compatible,  
php/mysql oriented objet, html.  
perfect for console browsing.  

This program follows the "retroweb 1.9" rules of webdesign  
wrote here [provisoire.org/web.1.9](http://provisoire.org/web.1.9)

## Easy to learn and easy to use

Only you need to read one page to learn how to use this  
software. 
The page is [help ulyxex](http://ulyxex.logz.org/index.php?k=page&v=3)

## Only a week

I wrote the first version of this software for multi-user managment  
in only a week. I used php object-oriented programming and mysql.  
With this program you can create text articles in separate pages.  
This basic program can be easily modified and enriched, the code is very  
readable and understandable. It can be a starting point to learn how to create  
php application.

## To install this program you can use two methods

First transfert all files in this archive your server.  
Go to your website root through your browser, start install  
and follow instructions.

If the install script don't work you can edit manually  
the file "config-example.php" with yours settings and rename it to "config.php"  
then import the file "ulyxex.sql" into your database.

After installation don't forget to delete the file "install.php" to prevent abuses.

I hope you enjoy this work and you will share your improvements.  
This program is licensed under copyleft. You can use, copy, modify and  
redistribute it freely.  

## Add your own scripts

Ulyxex offer you a usefull system to publish articles in a structured  
way of pages and articles, howerer you can easily add you own  
sctructure and scripts adding them into your config.php.  

After installation the file config.php will be never changed by  
updates.

## Download Ulyxex

here you can find latest releases http://ulyxex.logz.org/download/

To log first in :  
login = admin  
password = admin (or password you created with install)
	
Web site [ulyxex.logz.org](http://ulyxex.logz.org)

