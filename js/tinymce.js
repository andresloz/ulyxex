tinymce.init({
    selector: "textarea#content",
    themes: "modern",
	height: 500,
	plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css:"css/view_style.css",
   toolbar: "insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons | fullscreen", 
   fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt 60pt",
   style_formats: [
        {title: 'Header 1', block: 'h1'},
		{title: 'Header 2', block: 'h2'},
		{title: 'Header 3', block: 'h3'},
		{title: 'Div', block: 'div'},
		{title: 'Paragraph', block: 'p'},
		{title: 'Span', inline: 'span'},
		{title: 'Pre', pre: 'pre'},
		// very important to relate this with the main css style of the web site
		// {title: 'Article', block: 'article'},
		// {title: 'Footer', block: 'footer'},
		// {title: 'Navigation links', block: 'nav'},
		// {title: 'Mark', block: 'mark'},
    ]
 });
