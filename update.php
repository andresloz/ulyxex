<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
require_once("config.php");
require_once("pages/core/ulyxex.php");
if (!defined('FILES')) define("FILES",$prefixTableMySql."files");
require_once("pages/core/html.php");

$h = new Htmlz();
$uly = new Ulyxex();
$css = $h->css("css/view_style.css","external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title("Update Ulyxex Mysql Process");
$head = $h->head($ico.$meta.$css.$title);

$page = array();
$page[] = $h->p("Web site is ".$h->ahref($h->root_url(),$h->root_url()));
$page[] = $h->h2("Update Mysql");

$version = "0.0.0.0";

if ($update = @file("update.txt")){
	foreach($update as $line){
		if ( preg_match("/update/i",$line) ){
			$tmp = explode("=",$line);
			$version = trim($tmp[1]);
		} 
		if ( preg_match("/date/",$line) ){
			$tmp = explode("=",$line);
			$oldUpdateDate = trim($tmp[1]);
		}
	}
	$page[] = $h->p("Update.txt file exist and last updated VERSION is ".$version." date ".$oldUpdateDate);
} else {
	$page[] = $h->p("Update.txt file don't exist then VERSION is 0.0.0.0");
	$version = "0.0.0.0";
}

$newVersion = $version;

if ($version < "0.1.1"){
	$newVersion = "0.1.1";
	$up = array();
	$query = "ALTER TABLE ".PAGES." ADD `SORTSTYLE` tinyint NOT NULL";
	$up[] =  $h->p($uly->qTest($query));
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "0.1.2"){
	$newVersion = "0.1.2";
	$up = array();
	$query = "ALTER TABLE ".PAGES." ADD `DATE` datetime NOT NULL";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".USERS." ADD `DATE` datetime NOT NULL";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".LINKS." ADD `DATE` datetime NOT NULL";
	$up[] =  $h->p($uly->qTest($query));
	$page[] = $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "0.1.3"){
	$newVersion = "0.1.3";
	$up = array();
	$query = "ALTER TABLE ".PAGES." ADD `PAGEHEAD` mediumtext NOT NULL";
	$up[] =  $h->p($uly->qTest($query));
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "1.1.1"){
	$newVersion = "1.1.1";
	$up = array();
	$query = "ALTER TABLE ".PARAMS." ADD `TYPE` VARCHAR(32) NOT NULL DEFAULT 'page';";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".PAGES." ADD `TYPE` VARCHAR(32) NOT NULL DEFAULT 'page' AFTER `PAGEHEAD`";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".PAGES." ADD `LINK` TINYTEXT NOT NULL AFTER `TYPE`;";
	$up[] =  $h->p($uly->qTest($query));
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "1.1.2"){
	$newVersion = "1.1.2";
	$up = array();
	$query = "ALTER TABLE ".PARAMS." CHANGE `TYPE` `TYPEPAGE` VARCHAR(32) NOT NULL DEFAULT 'page';";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".PAGES." CHANGE `TYPE` `TYPEPAGE` VARCHAR(32) NOT NULL DEFAULT 'page';";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".ARTICLES." CHANGE `DATE` `DATEPAGE` DATETIME NOT NULL;";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".PAGES." CHANGE `DATE` `DATEPAGE` DATETIME NOT NULL;";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".USERS." CHANGE `DATE` `DATEPAGE` DATETIME NOT NULL;";
	$up[] =  $h->p($uly->qTest($query));
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "1.1.3"){
	$newVersion = "1.1.3";
	$up = array();
	$query = "ALTER TABLE ".PARAMS." ADD `NAVTYPE` VARCHAR(32) NOT NULL DEFAULT 'tree';";
	$up[] =  $h->p($uly->qTest($query));
	if ( defined('MENUSTYLE') ) {
		$query = UPDATE.PARAMS.SET."NAVTYPE = '".MENUSTYLE."'".WHERE."ID = 1;";
		$up[] =  $h->p($uly->qTest($query));
	}
	
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "1.3.6"){
	$newVersion = "1.3.6";
	$up = array();
	$query = "ALTER TABLE ".PAGES." ADD `LANG` VARCHAR(2) NOT NULL DEFAULT 'en';";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".ARTICLES." ADD `LANG` VARCHAR(2) NOT NULL DEFAULT 'en';";
	$up[] =  $h->p($uly->qTest($query));
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "1.4.0"){
	$newVersion = "1.4.0";
	$up = array();
	$query = "CREATE TABLE ".FILES." (
	ID mediumint(6) NOT NULL AUTO_INCREMENT,
	USERID mediumint(6) NOT NULL,
	FILENAME mediumtext NOT NULL,
	DATEFILE datetime NOT NULL,
	PRIMARY KEY (ID)
	) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$up[] =  $h->p($uly->qTest($query));
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "1.4.4"){
	$newVersion = "1.4.4";
	$up = array();
	$query = "ALTER TABLE ".PARAMS." ADD `CSS` VARCHAR(255) NOT NULL DEFAULT 'css/view_style.css';";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".PARAMS." ADD `ARTICLESPAGE` TINYINT NOT NULL DEFAULT '10';";
	$up[] =  $h->p($uly->qTest($query));
	$query = "ALTER TABLE ".PARAMS." ADD `ITEMS` TINYINT NOT NULL DEFAULT '10';";
	$up[] =  $h->p($uly->qTest($query));
	if (defined('ADMINITEMSBYPAGE') && defined('ARTICLESBYPAGE') && defined('VIEWCSS')) {
		$query = UPDATE.PARAMS.SET."ITEMS = '".ADMINITEMSBYPAGE."', ARTICLESPAGE = '".ARTICLESBYPAGE."', CSS = '".VIEWCSS."'".WHERE."ID = 1;";
		$up[] =  $h->p($uly->qTest($query));
	}
	
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "1.5.0.6"){
	$newVersion = "1.5.0.6";
	$up = array();
	$query = "ALTER TABLE ".PARAMS." ADD `SITETAB` VARCHAR(6) NOT NULL DEFAULT '_';";
	$up[] =  $h->p($uly->qTest($query));
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}
if ($version < "1.5.2.6"){
	$newVersion = "1.5.2.6";
	$up = array();
	$query = "ALTER TABLE ".PARAMS." ADD `HTTPS` TINYINT NOT NULL DEFAULT '0';";
	$up[] =  $h->p($uly->qTest($query));
	$page[] =  $h->p("Update mysql tables to ".$newVersion."\n".implode("\n",$up));
}

if ($versfile = @file("version.txt")){
	foreach($versfile as $line){
		if ( preg_match("/version/i",$line) ){
			$tmp = explode("=",$line);
			$newVersion = trim($tmp[1]);
		}
	}
}

/* write infos 						*/
$date = date("Y-m-d H:i:s",time());
$updateFileContent = <<<END
update=$newVersion
date=$date
END;

if ($newVersion > $version) {
	if ($f = fopen("update.txt","w")){
		$page[] =  $h->p("Write update.txt");
		$page[] =  $h->p("Update version ".$version." -to-> ".$newVersion);
		fwrite($f,$updateFileContent);
		fclose($f);
	} else {
		$page[] = $h->p("Can't write update.txt");
	}
} else {
	$page[] = $h->p("Nothing to update !");
}

$page[] = $h->p("Update done !");
$body = $h->body(implode("\n",$page));
$page = $h->html($head."\n".$body);

echo $page;
?>




