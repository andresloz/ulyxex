<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");
/* every user can modify his own values	*/
require_once("core/admin_user_modify.php");

/****************************************/
/* page vars							*/
/* check post 							*/
$userId = isset($_GET['user'])?$h->num($_GET['user']):$h->num($_POST['user']);
/****************************************/
/* user elements						*/
$o_usr = new UserModify($userId);
/****************************************/
/* page elements						*/
$userInfos = $o_usr->user_params($userId);
/* check if user own the user			*/
/* user can't be his user owner			*/
if ($_SESSION['userid'] == 1){
	// ok for admin
} elseif ($userInfos['USEROWNER'] != $_SESSION['userid'] && $userInfos['ID'] != $_SESSION['userid']){
	exit("you can't modify this user");
}
$css = $h->css("css/".ADMINCSS,"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$js = $h->script("js/ulyxex.js","external");
$title = $h->title($t->wr("modify user of").$userInfos['USERNAME']);
$head = $h->head($ico.$meta.$css.$js.$title);

$topPage = $h->h1($t->wr("modify user of").$h->span($userInfos['USERNAME'],"class='username'"));
$topPage .= $h->h2($adminNavigation->links);

$bottomPage = $h->ulyxCredits($firstLine=False);
/****************************************/
/* page data							*/			
$data = isset($_POST['update'])?$o_usr->update_user():"";

$data .= $o_usr->data_user();
/****************************************/
/* show									*/
$bottomPage .= $h->countQ();
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
