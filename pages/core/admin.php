<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class Admin extends Ulyxex  {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
	}
	public function mysql_version(){
		$dbLink = mysqli_connect(HOSTMYSQL,USERMYSQL,PASSMYSQL,BASEMYSQL) or die("Error database connection aborted"); 
		$version = mysqli_get_server_info($dbLink);
		mysqli_close($dbLink);
		return $version;
	}
	public function mysql_charset(){
		$dbLink = mysqli_connect(HOSTMYSQL,USERMYSQL,PASSMYSQL,BASEMYSQL) or die("Error database connection aborted"); 
		$charset = mysqli_character_set_name($dbLink);
		mysqli_close($dbLink);
		return $charset;
	}
	public function login($login,$password){
		$query = SELECT."ID,PASSWORD,USERLEVEL".FROM.USERS.WHERE."LOGIN = '".$this->h->safe($login)."'";
		$query = $this->q($query);
		$dat = $this->fetch($query);
		if ($dat['PASSWORD'] == $password){
			return array($dat['ID'],$dat['USERLEVEL'],1);
		} else {
			return array(null,null,null);
			//~ exit($this->t->w("bad login or password"));
		}
	}
	function getPageTypes(){
		$pageTypes['page'] = "page";
		$pageTypes['link'] = "link";
		$query = 	SELECT.
						"TYPEPAGE".
					FROM.
						PAGES.GROUPBY."TYPEPAGE";
		$query = $this->q($query);
		while ( $line = $this->fetch($query) ){
			$pageTypes[$line['TYPEPAGE']] = $line['TYPEPAGE'];
		}
		return $pageTypes;
	}
	function sum_pages_by_users(){
		$total = array();
		$query = 	SELECT.
						"USERID, COUNT(ID) AS NUM ".
					FROM.
						PAGES.GROUPBY."USERID";
		$query = $this->q($query);
		while ( $line = $this->fetch($query) ){
			$total[$line['USERID']] = $line['NUM'];
		}
		return $total;
	}
	function sum_arts_by_users(){
		$total = array();
		$query = 	SELECT.
						"USERID, COUNT(ID) AS NUM ".
					FROM.
						ARTICLES.GROUPBY."USERID";
		$query = $this->q($query);
		while ( $line = $this->fetch($query) ){
			$total[$line['USERID']] = $line['NUM'];
		}
		return $total;
	}
	function sum_arts_by_pages(){
		$total = array();
		$query = 	SELECT.
						"PAGEID, COUNT(ID) AS NUM ".
					FROM.
						ARTICLES.GROUPBY."PAGEID";
		$query = $this->q($query);
		while ( $line = $this->fetch($query) ){
			$total[$line['PAGEID']] = $line['NUM'];
		}
		return $total;
	}
	public function array_users($user=Null){
		$users = array();
		$query = 	SELECT.
						"ID,USERNAME,LOGIN".
					FROM.
						USERS;
		if ($_SESSION['userid'] != 1) $query .= WHERE."USEROWNER = ".$_SESSION['userid']._OR_."ID = ".$_SESSION['userid']._OR_."ID = ".$user;
		$query = $this->q($query);
		while ( $line = $this->fetch($query) ){
			$users[$line['USERNAME']." (".$line['LOGIN'].")"] = $line['ID'];
		}
		return $this->h->select($users,"userid",$user);
	}
	public function reset_form($action=Null){
		$inputs = $this->h->p($this->h->input("submit","",$this->t->w("reset values")).$this->h->input("hidden","reset",1));
		return $this->h->form($inputs,"reset","post","admin.php?action=".$action);
	}
	public function go_id($var=Null,$action=Null){
		$inputs = $this->h->p($this->h->input("submit","",$this->t->w("edit id"))." ".$this->h->input("text",$var,"","size=4"));
		return $this->h->form($inputs,"go_id","post","admin.php?action=".$action);
	}
	public function search_form($var=Null,$text=Null,$action=Null,$search=""){
		$inputs = $this->h->p($this->h->input("submit","",$this->t->w($text))." ".$this->h->input("text",$var,$search)); 
		return $this->h->form($inputs,"search","post","admin.php?action=".$action);
	}
	function bordered($val){
		return $this->h->span($val,"class='framed'");
	}
}
/****************************************/
/* class for admin menu					*/
class AdminNavigation {
	function __construct($index=array()){
		$h = New Htmlz();
		$t = New Translate();
		foreach ($index as $action => $vals) {
			$name = $vals[0];
			$link = $vals[1];
			$inMenu = $vals[2];
			$level = $vals[3];
			/* menu admin by levels */
			if ($inMenu == "action") { // actions in menu 
				for ($i = 1; $i <= 4; $i++) {
					if ($level >= $i) $levels[$i][] = $h->ahref("admin.php?action=".$action,$t->w($name),"class='action'");
				}
			} else if ($inMenu == "link") { // direct links in menu 
				for ($i = 1; $i <= 4; $i++) {
					if ($level >= $i) $levels[$i][] = $h->ahref($link,$t->w($name),"class='action'");
				}
			}  else {
				// null = not in menu
			}
		}
		$_ = array("",implode(" ",$levels[1]),implode(" ",$levels[2]),implode(" ",$levels[3]),implode(" ",$levels[4]));
		$this->links = $_[$_SESSION['level']];
	}
}
/* next pages class 					*/
class NextPages {
	function __construct($items,$action){
		$this->h = new Htmlz();
		$this->items = $items;
		$this->action = $action;
		$this->pos = isset($_GET['pg'])?$this->h->num($_GET['pg']):0;
	}
	public function numbers(){
		/* total pages */
		$pages = $this->items/ITEMS; 
		$pages++;
		$result = array();
		if ($pages > 2){
			for($i=1;$i < $pages;$i++){
				$num = $i?strval($i):"0";
				if ($i == $this->pos || ($this->pos == 0 && $i == 1)){
					$result[] = "[".$this->h->ahref("admin.php?".$this->action."&amp;pg=".$num,$num)."]";
				} else {
					$result[] = $this->h->ahref("admin.php?".$this->action."&amp;pg=".$num,$num);
				}
			}
		}
		return implode(" . ",$result);
	}
}
?>
