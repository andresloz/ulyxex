<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class UserModify extends Admin {
	function __construct($userId=Null){
		$this->h = New Htmlz();
		$this->t = New Translate();
		$this->user_id = $this->h->num($userId);
	}
	public function update_user(){
		$h = $this->h;$t = $this->t;
		/* not check if login is in use because login is read only */
		/* update user */
		$now = time();
		$date = date("Y-m-d H:i:s",$now);
		$passWarning = "";
		if ($_POST['password'] == $t->w("change password ?")) { // check if password modified
			$_POST['password'] = "";
		} 
		if ($h->num($_POST['level']) < $_SESSION['level']){
			$result = $h->h3($t->w("wrong level !"));
		} elseif ($_POST['userlogin'] == ""){
			$result = $h->h3($t->w("login field empty !"));
		} elseif ($this->user_id == 1 && $_SESSION['userid'] != 1){ 
			$result = $h->h3($t->w("only admin can modify admin !"));
		} else {
			$query = UPDATE.USERS.SET."LOGIN = '".$h->safe($_POST['userlogin'])."',".
										"USERNAME = '".$h->safe($_POST['username'])."',".
										"DESCRIPTION = '".$h->safe($_POST['description'])."',".
										"MAIL = '".$h->safe($_POST['mail'])."',".
										"USERLEVEL = ".$h->num($_POST['level']);
			if (!empty($_POST['password'])) {
				$query .=	",PASSWORD = '".$h->crypt($_POST['password'])."'";
				$passWarning = " (password changed)";
			}
			$query .= 					isset($_POST['updateDate'])?",DATEPAGE = '".$date."'":"";
			if (isset($_POST['userid']) && $_POST['userid'] != $_POST['old_userid']) $query .=	",USEROWNER = ".$h->num($_POST['userid']); // change userId
			$query .=					WHERE."ID = ".$this->user_id;
			$query = $this->q($query);
			$result = $h->h3($t->wr("user").strip_tags($_POST['username']).$t->wl("updated !").$passWarning,"class='warning'");
		}
		return $result;
	}
	public function data_user(){
		$h = $this->h;$t = $this->t;
		$query = SELECT.ALLFIELDS.FROM.USERS.WHERE."ID = ".$this->user_id.";";
		$query = $this->q($query);
		if ($dat = $this->fetch($query)){
			if ( $_SESSION['userid'] == $dat['ID'] || $dat['USEROWNER'] == $_SESSION['userid'] || $_SESSION['userid'] == 1){
				$_[] = $h->input("hidden","update",1);
				$_[] = $h->input("hidden","user",$dat['ID']);
				$_[] = $h->p("(id:".$dat['ID'].") ".$t->wr("login").$h->input("text","userlogin",$dat['LOGIN'],"readonly='readonly'"));
				$_[] = ($_SESSION['userid'] == 1 && $dat['ID'] !=1)?$h->input("hidden","old_userid",$dat['USEROWNER']).$h->p($t->wr("choose owner").$this->array_users($dat['USEROWNER'])):Null;
				$_[] = $h->p($t->wr("password").$h->input( "text","password",$t->w("change password ?")));
				$_[] = $h->p( $h->input("checkbox","checkbox",null,"onclick=\"showPassword(['password'])\"").$t->wl("show/hide password") );
				if ($this->user_id == 1){
					$_[] = $h->input("hidden","level",$dat['ID']);
				} else {
					$levels = array();
					for ($i=0;$i < (5 - $_SESSION['level']);$i++){
						$x = 4 - $i;
						$levels["level ".$x] =  $x;
					}
					$_[] = $h->p($t->w("level").$h->select($levels,"level",$dat['USERLEVEL']));
				}
				$_[] = $h->p($t->wr("name").$h->input("text","username",$h->uly_htmlentities($dat['USERNAME'])));
				$_[] = $h->p($t->w("description")).$h->p($h->textarea($dat['DESCRIPTION'],"description",array(8,FIELDWIDTH)));
				$_[] = $h->p($t->wr("mail").$h->input("text","mail",$dat['MAIL'],"size='30'"));
				$_[] = $h->p($t->wr("update date")."(".$dat['DATEPAGE'].")".$h->input("checkbox","updateDate","1"));
				$_[] = $h->p($h->input("submit","save",$t->w("update")));
				$inputs = implode("",$_);
				return $h->form($inputs,"user","post","admin.php?action=modify_user");
			} else {
				return $h->p("error");
			}
		} else {
			return $h->p("error");
		}
	}
}
?>
