<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class Articles extends Admin {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
		if (isset($_GET['pg'])) {
			$this->pagePos = $this->h->num($_GET['pg']);
			$_SESSION['pga'] = $_GET['pg'];
		} elseif (isset($_SESSION['pga']) && !empty($_SESSION['pga']))  {
			$this->pagePos = $this->h->num($_SESSION['pga']);
			$_GET['pg'] = $_SESSION['pga'];
		} else {
			$this->pagePos = 0;
		}
		
		$this->sortMethods = array("",ARTICLES.".DATEPAGE DESC",ARTICLES.".SUBJECT",ARTICLES.".PAGEID",ARTICLES.".USERID","");  
		$this->Select = SELECT.
							ARTICLES.".ID ARTID,".ARTICLES.".SUBJECT,".ARTICLES.".DATEPAGE,".ARTICLES.".HIDE,".
							USERS.".USERNAME,".USERS.".ID IDUSERID,".USERS.".USERLEVEL,".
							PAGES.".NAME PAGENAME,".PAGES.".ID PAGEID,".PAGES.".USERID USERPAGEID";
		$this->From = 	FROM.
							ARTICLES.
							INNERJOIN.PAGES._ON_.ARTICLES.".PAGEID = ".PAGES.".ID".
							INNERJOIN.USERS._ON_.ARTICLES.".USERID = ".USERS.".ID";
		$this->SelectCount = 	SELECT."COUNT(".ARTICLES.".ID) AS NUM";
		$this->OrderSort = "";
		$this->Cond = array();
		if (!isset($_SESSION['admin_article_sort'])) $_SESSION['admin_article_sort'] = $this->sortMethods[1];
	}
	public function update_articles(){
		$formValues = array();
		$values = $_POST;
		foreach($values as $k=>$v){
			if ( preg_match("/\_/",$k) ){
				list($key,$id) = explode("_",$k);
				$formValues[$id][$key] = $v;
			}
		}
		foreach($formValues as $id=>$value){
			$query = 0;
			if (isset($value['delete'])){
				$query = DELETE.FROM.ARTICLES.WHERE."ID = ".$id;
				$query = $this->q($query);
			}
			if (isset($value['hide']) && $value['oldhide'] == 0){
				$query = UPDATE.ARTICLES.SET."HIDE = 1".WHERE."ID = ".$id;
				$query = $this->q($query);
			} elseif (!isset($value['hide']) && $value['oldhide'] == 1){
				$query = UPDATE.ARTICLES.SET."HIDE = 0".WHERE."ID = ".$id;
				$query = $this->q($query);
			}
		}
		return $this->h->h3($this->t->w("some values have been modified !"),"class='warning'");
	}
	public function list_articles(){
		$h = $this->h;$t = $this->t;
		$p = New Tree();
		$paths = $p->paths();
		$pathById = array();
		foreach ($paths as $pth=>$arr) {
			$pathById[$arr[1]] = $pth;
		}
		$pgPos =($this->pagePos)?($this->pagePos - 1) * ITEMS:0;
		if (isset($_POST['sort'])) $_SESSION['admin_article_sort'] = $this->sortMethods[$_POST['sort']];
		if (isset($_GET['ownerFilter'])) $_SESSION['admin_articles_filter'] = ARTICLES.".USERID = ".$h->num($_GET['ownerFilter']);
		if (isset($_GET['pageFilter'])) $_SESSION['admin_articles_filter'] = ARTICLES.".PAGEID = ".$h->num($_GET['pageFilter']);
		if (isset($_POST['search_subject'])) $_SESSION['admin_articles_search_subject'] = " MATCH(".ARTICLES.".SUBJECT) AGAINST('".$h->safe($_POST['search_subject'])."') ";
		if (isset($_POST['reset'])){
			if (isset($_SESSION['admin_articles_filter'])) $_SESSION['admin_articles_filter'] = "";
			if (isset($_SESSION['admin_article_sort'])) $_SESSION['admin_article_sort'] = $this->sortMethods[1];
			if (isset($_SESSION['admin_articles_search_subject'])) $_SESSION['admin_articles_search_subject'] = "";
		}
		// always page position = 1 if use search, only ITEMS first results
		if ( isset($_SESSION['admin_articles_search_subject']) && !empty($_SESSION['admin_articles_search_subject']) ) $pgPos = 1;
		$this->OrderSort = ORDERBY.$_SESSION['admin_article_sort']." LIMIT ".$pgPos.",".ITEMS;
		// 1) can create users etc. (high trust level)
		// 2) can create articles and pages
		// 3) can create articles everywhere
		// 4) can create articles only in sandbox
		switch ($_SESSION['level']){
			case 1:
				// user level 1 can't edit others user level 1
				if ($_SESSION['userid'] != 1){ // restrain user level if not user admin
					$this->Cond[] = "(".USERS.".USERLEVEL > ".$_SESSION['level']._OR_.USERS.".ID = ".$_SESSION['userid'].")"; // all level 2, 3 and 4 user's articles, and user's articles 
					$this->Cond[] = USERS.".ID != 1"; // but not admin articles
				}
				if (isset($_SESSION['admin_articles_filter']) && !empty($_SESSION['admin_articles_filter'])) $this->Cond[] = $_SESSION['admin_articles_filter'];
				if ( isset($_SESSION['admin_articles_search_subject']) && !empty($_SESSION['admin_articles_search_subject']) ) $this->Cond[] = $_SESSION['admin_articles_search_subject'];
				break;
			case 2:
				$this->Cond[] = "(".USERS.".ID = ".$_SESSION['userid']._OR_.PAGES.".USERID = ".$_SESSION['userid'].")"; // user's articles and articles in user's pages
				$this->Cond[] = USERS.".ID != 1"; // but not admin articles
				if (isset($_SESSION['admin_articles_filter']) && !empty($_SESSION['admin_articles_filter']) ) $this->Cond[] = $_SESSION['admin_articles_filter'];
				if ( isset($_SESSION['admin_articles_search_subject']) && !empty($_SESSION['admin_articles_search_subject']) ) $this->Cond[] = $_SESSION['admin_articles_search_subject'];
				break;
			default:
				$this->Cond[] = ARTICLES.".USERID = ".$_SESSION['userid']; // user's articles only
				if ( isset($_SESSION['admin_articles_search_subject']) && !empty($_SESSION['admin_articles_search_subject']) ) $this->Cond[] = $_SESSION['admin_articles_search_subject'];
		}
		$query = $this->Select.$this->From.$this->Where($this->Cond).$this->OrderSort;
		$query = $this->q($query);
		
		$result = "";
		
		$sort = array($t->w("date")=>1,$t->w("subject")=>2,$t->w("page")=>3,$t->w("user")=>4);
		$type = array_search($_SESSION['admin_article_sort'],$this->sortMethods);
		$inputs = $h->p($h->input("submit","",$t->w("sort/group by"))." ".$h->select($sort,"sort",$type));
		$result .= $h->form($inputs,"sort","post","admin.php?action=articles");
		
		if (isset($_POST['search_subject'])) {
			$searchValue = $_POST['search_subject'];
		} else {
			$searchValue = "";
		}
		$searchForm = $this->search_form("search_subject","search subject","articles",$searchValue);
		$goIdForm = $this->go_id("art","modify_article");
		$resetForm = $this->reset_form("articles");
		$result .= $searchForm.$goIdForm.$resetForm;
		
		$artListSubmit = $h->p($h->input("submit","",$t->w("update all")));
		$artListTitle = $h->h3($t->w("articles list"));
		$artList = $h->input("hidden","update",1);
		
		while ( $line = $this->fetch($query) ){
			$id = "id".$h->f4d($line['ARTID']);
			$date = substr($line['DATEPAGE'],0,10);
			$checked = ($line['HIDE'])?" checked=\"checked\"":null;
			$hide = $t->wr("hide").$h->input("checkbox","hide_".$line['ARTID'],"on",$checked).$h->input("hidden","oldhide_".$line['ARTID'],$line['HIDE']);
			$modify = $h->ahref("admin.php?action=modify_article&amp;art=".$line['ARTID'],$t->w("modify"),"class='formframe'");
			$delete = $t->wr("del").$h->input("checkbox","delete_".$line['ARTID'],"on");
			$subject = $h->strWidth(strip_tags($line['SUBJECT']),28);
			$view = $t->wx("subj").$h->ahref("index.php?k=article&amp;v=".$line['ARTID'],$subject,"class='formframe' target=_blank title='".
			$h->uly_htmlentities(strip_tags($line['SUBJECT']))."'");
			
			$page = $t->wx("pag").$h->ahref("admin.php?action=articles&amp;pageFilter=".$line['PAGEID'],$h->strWidth($line['PAGENAME'],20),"class='formframe' title='".
			$h->uly_htmlentities($pathById[intval($line['PAGEID'])])."'");
			$by = $t->wx("by").$h->ahref("admin.php?action=articles&amp;ownerFilter=".$line['IDUSERID'],$h->strWidth($line['USERNAME'],12),"class='formframe' title='".$h->uly_htmlentities($line['USERNAME'])."'");
			$_ = array($this->bordered($id),$this->bordered($date),$this->bordered($hide),$modify,$this->bordered($delete),$view,$page,$by);
			$content = implode(" ",$_);
			$artList .= $h->p($content);
		}
		
		$nextPages = $this->next_pages();
		$artList = $artListTitle.$artListSubmit.$artList.$artListSubmit.$nextPages;
		$result .= $h->form($artList,"articles","post","admin.php?action=articles&amp;pg=".$this->pagePos);
		return $result;
	}
	public function next_pages(){
		$query = $this->SelectCount.$this->From.$this->Where($this->Cond);
		$query = $this->q($query);
		$dat = $this->fetch($query);
		$pagesNumbers = new NextPages($dat['NUM'],"action=articles");
		$result = $this->h->p($pagesNumbers->numbers());
		$result .= $this->h->p("(".$this->t->wr("total articles")."= ".$dat['NUM'].")");
		return $result;
	}
}
?>
