<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ViewFiles extends Admin {
	function __construct($dir=null){
		$this->dir = $dir;
		$this->h = New Htmlz();
		$this->t = New Translate();
		
		if (isset($_GET['pg'])) {
			$this->pagePos = $this->h->num($_GET['pg']);
			$_SESSION['pgf'] = $_GET['pg'];
		} elseif (isset($_SESSION['pgf']) && !empty($_SESSION['pgf']))  {
			$this->pagePos = $this->h->num($_SESSION['pgf']);
			$_GET['pg'] = $_SESSION['pgf'];
		} else {
			$this->pagePos = 0;
		}

		$this->sortMethods = array("",FILES.".DATEFILE DESC",FILES.".FILENAME",FILES.".USERID","");
		if (!isset($_SESSION['admin_files_sort'])) $_SESSION['admin_files_sort'] = $this->sortMethods[1];
		if (isset($_POST['sort'])) {
			$_SESSION['admin_files_sort'] = $this->sortMethods[$_POST['sort']];
		}
		
		if (isset($_POST['reset'])){
			if (isset($_SESSION['admin_files_sort'])) $_SESSION['admin_files_sort'] = $this->sortMethods[1];
		}
		
		$pgPos =($this->pagePos)?($this->pagePos - 1) * ITEMS:0;
		$this->OrderSort = ORDERBY.$_SESSION['admin_files_sort']." LIMIT ".$pgPos.",".ITEMS;
		if ($_SESSION['level'] < 2) {
			$this->Select = SELECT.FILES.".ID,".FILES.".FILENAME,".FILES.".DATEFILE,".USERS.".USERNAME".FROM.FILES.INNERJOIN.USERS._ON_.FILES.".USERID = ".USERS.".ID";
			$this->SelectCount = SELECT."COUNT(ID) AS NUM FROM ".FILES;
		} else {
			$_[] = "USERID='".$_SESSION['userid']."'";
			$query = $this->q(SELECT."ID,USERNAME".FROM.USERS.WHERE." USEROWNER='".$_SESSION['userid']."'");
			while ( $line = $this->fetch($query) ){
				$_[] = "USERID='".$line['ID']."'";
			}
			$this->Select = SELECT.FILES.".ID,".FILES.".FILENAME,".FILES.".DATEFILE,".USERS.".USERNAME".FROM.FILES.INNERJOIN.USERS._ON_.FILES.".USERID = ".USERS.".ID".$this->whereOr($_);
			$this->SelectCount = SELECT."COUNT(ID) AS NUM".FROM.FILES.$this->whereOr($_);
		}
	}
	public function getFilesUrlInput() {
		$h = $this->h;$t = $this->t;
		$sort = array($t->w("date")=>1,$t->w("file name")=>2,$t->w("user")=>3);
		if (isset($_SESSION['admin_files_sort'])) {
			$type = array_search($_SESSION['admin_files_sort'],$this->sortMethods);
		} else {
			$type = 1;
		}
		$inputs = $h->p($h->input("submit","",$t->w("sort/group by"))." ".$h->select($sort,"sort",$type));
		$result = $h->form($inputs,"sort","post","admin.php?action=files");
		$result .= $this->reset_form("files");
		
		$_[] = $h->p($h->input("submit","save",$t->w("update")));
		$_[] = $t->wr("add file").$h->input("file","addfile","","size=30");
		$query = $this->Select.$this->OrderSort;
		$query = $this->q($query);
		while ( $line = $this->fetch($query) ) {
			if (file_exists($line["FILENAME"])) {
				$size = $h->format_file_size(filesize($line["FILENAME"]));
				$params = stat($line["FILENAME"]);
				$_[] = $h->p(implode(" ",array($this->bordered($line["DATEFILE"]),$this->bordered($line["USERNAME"]),$this->bordered($t->wr("untie").$h->input("checkbox","delete|".$line["ID"]."|".getcwd()."/".$line["FILENAME"],"on")),$h->ahref($h->root_url($line["FILENAME"]),$line["FILENAME"],"target='_blank'"),$h->b($size))));
			} else {
				$_[] = $h->p(implode(" ",array($h->color("file doesn't exist","red"),$this->bordered($t->wr("delete").$h->input("checkbox","delete|".$line["ID"]."|".$line["FILENAME"],"on")),$h->ahref($h->root_url($line["FILENAME"]),$line["FILENAME"]),$h->b("none"),$line["USERNAME"])));
			}
		}
		$_[] = $h->input("hidden","update",1);
		$_[] = $h->p($h->input("submit","save",$t->w("update")));
		$inputs = implode("",$_);
		$result .= $h->form($inputs,"files","post","admin.php?action=files","multipart/form-data");
		$query = $this->SelectCount;
		$query = $this->q($query);
		$dat = $this->fetch($query);
		$pagesNumbers = new NextPages($dat["NUM"],"action=files");
		$result .= $h->p($pagesNumbers->numbers()." (".$dat["NUM"]." fics)");
		return $result;
	}
	public function getFilesUrl() {
		$h = $this->h;
		$t = $this->t;
		$sort = array($t->w("date")=>1,$t->w("file name")=>2,$t->w("user")=>3);
		if (isset($_SESSION['admin_files_sort'])) {
			$type = array_search($_SESSION['admin_files_sort'],$this->sortMethods);
		} else {
			$type = 1;
		}
		$inputs = $h->p($h->input("submit","",$t->w("sort/group by"))." ".$h->select($sort,"sort",$type));
		$result = $h->form($inputs,"sort","post","admin.php?action=list_files");
		$result .= $this->reset_form("files");
		$pgPos =($this->pagePos)?($this->pagePos - 1) * ITEMS:0;
		$orderSort = ORDERBY." DATEFILE DESC LIMIT ".$pgPos.",".ITEMS;
		$query = $this->Select.$orderSort;
		$query = $this->q($query);
		$files = array();
		while ( $line = $this->fetch($query) ) {
			if (file_exists($line["FILENAME"])) {
				$files[] = $h->root_url($line["FILENAME"]);
			}
		}
		$data = implode(" ",array_map('add_link',$files));
		$data = $h->ol($data);
		$query = $this->q($this->SelectCount);
		$dat = $this->fetch($query);
		$pagesNumbers = new NextPages($dat["NUM"],"action=list_files");
		$data .= $h->p($pagesNumbers->numbers()." (".$dat["NUM"]." fics)");
		$result .= $data;
		return $result;
	}
	public function update_files(){
		$h = $this->h;
		$t = $this->t;
		$now = time();
		$date = date("Y-m-d H:i:s",$now);
		$values = $_POST;
		foreach($values as $k=>$v){
			if ( preg_match("/delete\*\|/",$k) ){
				list($del,$id,$file) = explode("|",$k);
				@unlink($file);
			} elseif ( preg_match("/delete\|/",$k) ){
				list($del,$id,$file) = explode("|",$k);
				@unlink($file);
				$query = DELETE.FROM.FILES.WHERE." ID = ".$id;
				$query = $this->q($query);
			}
		}
		if (!empty($_FILES['addfile']["name"])) {
			$fileName = $_FILES['addfile']["name"];
			$ext = $h->get_ext($fileName);
			if ( !in_array($ext, $h->extUploadAllowed) && $_SESSION['level'] > 1) exit("file extension not allowed ! ".$ext);// accept all file in level 1
			$sendFile = move_uploaded_file($_FILES['addfile']["tmp_name"],"files/".$fileName);
			if ($sendFile) {
				$query = INSERTINTO.FILES." (FILENAME,USERID,DATEFILE) VALUES ('files/".$fileName."',".$h->num($_SESSION['userid']).",'".$date."')";
				//~ echo $query;
				$query = $this->q($query);
			}
		}
		return $this->h->h3($t->w("some values have been modified !"),"class='warning'");
	}
}
?>
