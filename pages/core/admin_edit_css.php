<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class EditCss extends Admin {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
	}
	function css_files(){
		$list = array();
		$handle = opendir("css/");
		while ($file = readdir($handle)){
			$file_parts = pathinfo($file);
			if ($file_parts['extension'] == "css" && $file != "admin_style.css")  {
				$list[] = "css/".$file;
			}
		} // end while
		closedir($handle);
		return $list;
	}
	public function get_css_list(){
		$h = $this->h;$t = $this->t;
		$options = array();
		$css_files = $this->css_files();
		foreach($css_files as $css) {
			$options[$css] = $css;
		}
		$_[] = $h->input("hidden","choose",1);
		$_[] = $h->p($t->wr("css list").$h->select($options,"css")." ".$h->input("submit","save",$t->w("choose")));
		$inputs = implode("",$_);
		return $h->form($inputs,"home","post","admin.php?action=edit_css");
	}
	public function form_css(){
		$h = $this->h;$t = $this->t;
		$data = "";
		if (file_exists($_POST['css'])) {
				$Lines = file($_POST['css']);
				foreach($Lines as $Line) {
					$data .= $Line;
				}
		}
		$_[] = $h->h3("edit: ".$_POST['css']);
		$_[] = $h->input("hidden","editcss",1);
		$_[] = $h->input("hidden","css",$_POST['css']);
		$_[] = $h->p($h->textarea($data,"content",array(12,FIELDWIDTH)));
		$_[] = $h->input("submit","save",$t->w("update css"));
		$inputs = implode("",$_);
		return $h->form($inputs,"home","post","admin.php?action=edit_css");
	}
	public function updated_css(){
		$h = $this->h;$t = $this->t;
		$data = "";
		$File = fopen($_POST['css'],"w") or die("can't open ".$_POST['css']);
		fwrite($File, $_POST['content']);
		fclose($File);
		$_[] = $h->h3("updated: ".$_POST['css']."!","class='warning'");
		$_[] = $h->input("hidden","editcss",1);
		$_[] = $h->input("hidden","css",$_POST['css']);
		$_[] = $h->p($h->textarea($_POST['content'],"content",array(12,FIELDWIDTH)));
		$_[] = $h->input("submit","save",$t->w("update css"));
		$inputs = implode("",$_);
		return $h->form($inputs,"home","post","admin.php?action=edit_css");
	}
}
?>