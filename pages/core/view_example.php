<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ViewExample extends Ulyxex {
	function __construct($pageId=0){
		$this->param = $pageId;
		$this->name = "example test";
	}
	public function example_data(){
		$h = New Htmlz();
		$dat = $this->page_params($this->param);
		$data[] = $h->h1("this is an example !");
		
		$list[] = $h->li("the param of this example is : ".$h->b($this->param));
		$list[] = $h->li("the description of this example is : ".$h->b($dat['DESCRIPTION']));
		$list[] = $h->li("the link of this example is : ".$h->b($dat['LINK']));
		$list[] = $h->li("the user of this example is : ".$h->b($dat['USERID']));
		$list[] = $h->li("the type of this example is : ".$h->b($dat['TYPEPAGE']));
		$data[] = $h->ul(implode("\n",$list));
		
		$data[] = $h->h2("Lorem Ipsum");
		$data[] = $h->p("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in dolor eget lorem consequat finibus nec eu sapien. In facilisis neque mauris, ut auctor arcu porta ut. Sed congue justo dui, vitae porta nisl mattis vitae. Aenean eu ultrices ante. Sed at neque fermentum, imperdiet ligula ut, tristique ante. Sed non euismod lectus. Cras pulvinar tincidunt maximus. Pellentesque viverra tincidunt eleifend. Maecenas ultrices tellus ut erat dictum, id sodales ante mollis. Nulla feugiat ullamcorper consequat. Praesent elementum lacus ut ante ultricies, eu imperdiet leo sagittis. In ultricies tincidunt eros non posuere.");
		$data[] = $h->p("Suspendisse suscipit vulputate lorem, sit amet ultricies metus cursus volutpat. Mauris auctor id nisi vitae pulvinar. Nullam interdum tempus ipsum. Praesent nec diam eget purus ornare tempus eleifend nec nunc. Phasellus nunc risus, consequat ac interdum nec, mollis a velit. Donec maximus felis a felis porttitor ullamcorper. Donec dapibus hendrerit sem, condimentum mollis eros facilisis nec. Sed ultrices eros eu rhoncus elementum. Phasellus in orci lobortis, faucibus odio eget, venenatis nisi.");
		$data[] = $h->p("Nulla fermentum tempus augue a hendrerit. Fusce turpis leo, porta sed tortor in, pretium gravida eros. Fusce commodo ut mauris eu egestas. Duis cursus tempor pulvinar. Etiam cursus, enim id sollicitudin posuere, felis augue volutpat elit, ullamcorper sollicitudin eros risus ac ligula. Fusce blandit justo nec turpis dapibus, ut auctor sapien sollicitudin. Donec nisl libero, pharetra id mattis ac, molestie id turpis. Suspendisse auctor at dui ut dictum. Maecenas convallis tortor vel magna lobortis pellentesque. Praesent blandit, tortor a feugiat sagittis, nulla libero luctus justo, vel vestibulum sem purus ac purus.");
		return implode("\n",$data);
	}
}
?>