<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class PageModify extends Admin {
	function __construct($pageId=Null){
		$this->h = New Htmlz();
		$this->t = New Translate();
        $this->tree = New Tree();
		$this->page_id = $pageId;
		$this->Select = SELECT.
							PAGES.".*,".USERS.".USERNAME,".USERS.".ID USERID,".USERS.".USEROWNER";
		$this->From = 	FROM.
							PAGES.
							INNERJOIN.USERS._ON_.PAGES.".USERID = ".USERS.".ID";
		$this->Cond = array();
	}
	public function update_page(){
		$h = $this->h;$t = $this->t;
		$now = time();
		$date = date("Y-m-d H:i:s",$now);
		$hide = (isset($_POST['hide']))?1:0;
		if (!empty($_FILES['addfile']["name"])) {
			$fileName = $_FILES['addfile']["name"];
			$ext = $h->get_ext($fileName);
			if ( !in_array($ext, $h->extUploadAllowed) && $_SESSION['level'] > 1) exit("file extension not allowed ! ".$ext);
			$fileName = sprintf("%03d",rand(0,999))."_".$h->text2ascii($fileName);
			$sendFile = move_uploaded_file($_FILES['addfile']["tmp_name"],"files/".$fileName);
			if ($sendFile) {
				$query = INSERTINTO.FILES." (FILENAME,USERID,DATEFILE) VALUES (".
										"'"."files/".$fileName."',".$h->num($_POST['userid']).
										",'".$date."');";
				$query = $this->q($query);
				$addFile = $h->br($h->tag_by_fileType("files/".$fileName,$_FILES['addfile']["type"]));
			} else {
				$addFile = $h->br($h->color("file ".$fileName." not uploaded!","red"));
			}
		} else {
			$addFile = "";
		}
		if (empty($_POST['link'])) {
			if (empty($_POST['type'])) $_POST['type'] = $_POST['set_type'];
		} else {
			$_POST['type'] = "link";
		}
		if (empty($_POST['name'])){return $h->h3($t->wr("title empty !"));}
		$query = UPDATE.PAGES.SET."NAME = '".$h->safe($_POST['name'])."',".
									"PARENTID = '".$h->num($_POST['parent'])."',".
									"DESCRIPTION = '".$addFile.$h->safe($_POST['content'])."',".
									"TYPEPAGE = '".$h->safe($_POST['type'])."',".
									"LANG = '".$h->safe($_POST['lang'])."',".
									"LINK = '".$h->safe($_POST['link'])."',".
									"SORTSTYLE = '".$h->num($_POST['sortstyle'])."',".
									"PAGEHEAD = '".$h->safe($_POST['head'])."',".
									"HIDE = '".$hide."'";
		$query .= isset($_POST['updateDate'])?",DATEPAGE = '".$date."'":"";
		if ($_POST['userid'] != $_POST['old_userid']) $query .=	",USERID = ".$h->num($_POST['userid']); // change userId
		$query .=					WHERE."ID = ".$this->page_id;
		if ($this->tree->pathTest($page=$this->page_id,$parent=$_POST['parent'])){
			$query = $this->q($query);
			return $h->h3($t->wx("page").strip_tags($_POST['name']).$t->wl("updated !"),"class='warning'");
		} else {
			return $h->h3($t->w("bad lineage !"),"class='warning'");
		}
	}
	public function data_page(){
		$h = $this->h;$t = $this->t;
		// 1) can create users etc.
		// 2) can create articles and pages
		// 3) can create articles everywhere
		// 4) can create articles only in sandbox
		switch ($_SESSION['level']){
			case 1: 
				if ($_SESSION['userid'] != 1){
					$this->Cond[] = "(".USERS.".USERLEVEL > ".$_SESSION['level']._OR_.USERS.".ID = ".$_SESSION['userid'].")"; // all level 2, 3 and 4 user's pages, user's pages 
					$this->Cond[] = USERS.".ID != 1"; // but not admin articles
				}
				$this->Cond[] = PAGES.".ID = ".$this->page_id;
				break;
			case 2:
				$this->Cond[] = PAGES.".ID = ".$this->page_id;
				break;
		}
		$query = $this->Select.$this->From.$this->Where($this->Cond);
		$query = $this->q($query);
		if ($dat = $this->fetch($query)){
			$sandboxParams = "";
			if ($dat['NAME'] == "SANDBOX"){
				$sandboxParams = "readonly='readonly'";
			}
			$hideParams = ($dat['HIDE'])?" checked=\"checked\"":null;
			$optionsDate = array($t->w("date")=>0,$t->w("subject")=>1,$t->w("reverse date")=>2);
			$_[] = $h->input("hidden","page",$dat['ID']);
			$_[] = $h->input("hidden","update",1);
            $_page = $t->w("child page of").$this->tree->pathsSelect($dat['PARENTID'],1,"page");
            
            //~ modification délicate
			$_user = ($_SESSION['level'] == 1)?$h->input("hidden","old_userid",$dat['USERID']).$t->wr("choose user").$this->array_users($dat['USERID']):$h->input("hidden","old_userid",$dat['USERID']).$h->input("hidden","userid",$dat['USERID']);
			
			$_[] = $h->p( $_page." ".$_user );
			$_[] = $h->p($t->wr("redirect to link").$h->input("text","link",$dat['LINK'],"size=".(FIELDWIDTH - 10)));
			$_[] = $h->p( $t->wr("type").$t->w("page").$h->select($this->getPageTypes(),"set_type",$dat['TYPEPAGE']).$t->wlr("or add new type").$h->input("text","type").$t->wlr("lang").$h->input("text","lang",$dat['LANG'],"size=3") );
			$_[] = $h->p($t->w("sort articles by").$h->select($optionsDate,"sortstyle",$dat['SORTSTYLE']).$t->wlr("hide").$t->wr("page").$h->input("checkbox","hide","on",$hideParams)." (id ".$dat['ID'].") -- ".$t->wlr("update date")."(".$dat['DATEPAGE'].") ".$h->input("checkbox","updateDate","1"));
			$_[] = $h->p($h->span("&lt;head&gt;","class='tag'").$h->span(" &lt;title&gt; ","class='tag'").$h->input("text","name",$h->uly_htmlentities($dat['NAME']),$sandboxParams." size=".(FIELDWIDTH - 10)).$h->span(" &lt;/title&gt; ","class='tag'") );
			$_[] = $h->p($h->textarea($dat['PAGEHEAD'],"head",array(3,FIELDWIDTH)).$h->span(" &lt;/head&gt;","class='tag'"));
			$_[] = $h->p( $h->span("&lt;body&gt; &lt;header&gt; ".$t->w("top content"),"class='tag'")." ".$h->input("submit","save",$t->w("update")) );
			$_[] = $h->p( $t->wr("add file").$h->input("file","addfile","","size=30").$t->wlr("or")."[".$h->ahref("admin.php?action=list_files",$t->w("list files"),"target='_blank'")."]");
			$_[] = $h->p($h->textarea($dat['DESCRIPTION'],"content",array(12,FIELDWIDTH)));
			$_[] = $h->p( $h->span("&lt;/header&gt; ... ".$t->w("articles")." ... &lt;/body&gt; ","class='tag'").$h->input("submit","save",$t->w("update")) );
			$inputs = implode("",$_);
			return $h->form($inputs,"page","post","admin.php?action=modify_page","multipart/form-data");
		} else {
			return $h->p("error");
		}
	}
}
?>
