<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ArticleAdd extends Admin {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
		$this->tree = New Tree();
	}
	public function add_article(){
		$h = $this->h;$t = $this->t;
		$now = time();
		$date = date("Y-m-d H:i:s",$now);
		$hide = (isset($_POST['hide']))?1:0;
		$addFile = "";
		if ($_SESSION['level'] < 4) {
			if (!empty($_FILES['addfile']["name"])) {
				$fileName = $_FILES['addfile']["name"];
				$ext = $h->get_ext($fileName);
				if ( !in_array($ext, $h->extUploadAllowed) && $_SESSION['level'] > 1) exit("file extension not allowed ! ".$ext);
				$fileName = sprintf("%03d",rand(0,999))."_".$h->text2ascii($fileName);
				$sendFile = move_uploaded_file($_FILES['addfile']["tmp_name"],"files/".$fileName);
				if ($sendFile) {
					$query = INSERTINTO.FILES." (FILENAME,USERID,DATEFILE) VALUES (".
											"'"."files/".$fileName."',".$h->num($_POST['userid']).
											",'".$date."');";
					$query = $this->q($query);
					$addFile = $h->br($h->tag_by_fileType("files/".$fileName,$_FILES['addfile']["type"]));
				} else {
					$addFile = $h->br($h->color("file ".$fileName." not uploaded!","red"));
				}
			}
		}
		if ($_POST['subject'] == "") $_POST['subject'] = $t->w("no subject");
		/* in case of page no exist		*/
		$siteDat = $this->site_params();
		$pageId = isset($_POST['parent'])?$h->num($_POST['parent']):$siteDat['SANDBOXID'];
		$content = $addFile.$h->safe($_POST['content']);
		$query = INSERTINTO.ARTICLES." 	(SUBJECT,USERID,DATEPAGE,CONTENT,LANG,HIDE,PAGEID) VALUES (".
										"'".$h->safe($_POST['subject'])."',".$h->num($_POST['userid']).",'".
										$h->safe($_POST['date'])."','".$content."','".$h->safe($_POST['lang'])."',".
										$h->num($hide).",".$pageId.");";
		$query = $this->q($query);
		return $h->h3($t->wr("article").strip_tags($_POST['subject']).$t->wl("created !"),"class='warning'");
	}
	public function add_article_form(){
		$now = time();
		$date = date("Y-m-d H:i:s",$now);
		$siteDat = $this->site_params();
		$h = $this->h;$t = $this->t;
		$_[] = $h->input("hidden","add",1);
		$_[] = $h->input("hidden","userid",$_SESSION['userid']);
		$_[] = $h->p( );
		/* define unique user at level 3 */
		if ($_SESSION['level'] <= 2) {
			$_[] = $h->p($t->wr("choose page").$this->tree->pathsSelect(null,0,"page"));
		} else if ($_SESSION['level'] == 3) {
			/* define unique user at level 3 */
			$_[] = $h->p($t->wr("choose page").$this->tree->pathsSelect(null,0,"page",$_SESSION['userid']));
		} else {
			$_[] = $h->h1("Add article to SandBox").$h->input("hidden","pageid",$siteDat['SANDBOXID']);
		}
		$_hideArticle = $t->wr("hide").$t->wr("article").$h->input("checkbox","hide","on");
		
		$_[] = $h->p($t->wr("subject (of post)")."- ".$_hideArticle." -".$t->wlr("date").$h->input("text","date",$date,"size=19")." -".$t->wlr("lang").$h->input("text","lang",$t->getLang(),"size=3"));
		$_[] = $h->p($h->input("text","subject","new article","size=".FIELDWIDTH));
		$tmp = ($_SESSION['level'] <= 3)?$t->wr("add file").$h->input("file","addfile","","size=30").$t->wlr("or"):null;
		$_[] = $h->p($tmp."[".$h->ahref("admin.php?action=list_files",$t->w("list files"),"target='_blank'")."]");
		$_[] = $h->p($h->span("&lt;article&gt; ","class='tag'").$h->input("submit","save",$t->w("save")));
		$_[] = $h->p($h->textarea("","content",array(12,FIELDWIDTH)));
		$_[] = $h->p($h->span("&lt;/article&gt;","class='tag'")." ".$h->input("submit","save",$t->w("save")));
		$inputs = implode("",$_);
		return $h->form($inputs,"article","post","admin.php?action=add_article","multipart/form-data");
	}
}
?>
