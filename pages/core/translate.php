<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
#[\AllowDynamicProperties]
class Translate {
	public function __construct(){
		$this->translation = array(
			"Contact website"				=>array("Contact website","Contacter site","Contactar sitio"),
			"Login"							=>array("Login","Connexion","Connectarse"),
			"Submint"						=>array("Submit","Envoyer","Enviar"),
			"Made with Ulyxex"				=>array("Made with Ulyxex","Réalisé avec Ulyxex","Realizado con Ulyxex"),
			"Copyleft Free Art License"		=>array("Copyleft Free Art License","Copyleft Licence Art Libre","Copyleft Licencia Arte Libre"),
			"article content"				=>array("article content","contenu de l'article","contenido del articulo"),
			"add article of"				=>array("add article of","ajouter article de","añadir articulo de"),
			"modify article of"				=>array("modify article of","modifier article de","modificar articulo de"),
			"you can't modify this article" =>array("you can't modify this article","vous ne pouvez pas modifier cet article","no se puede modificar este articulo"), 
			"articles of"					=>array("articles of","articles de","articulos de"),
			"multiple uploads of"			=>array("multiple uploads of","multiples téléversations de","subir varios ficheros de"),
			"admin"							=>array("admin","administrateur","administrator"),
			"site by"						=>array("site by","site par","sitio por"),
			"add link of"					=>array("add link of","ajouter lien de","añadir link de"),
			"no subject"					=>array("no subject","pas de sujet","sin sujeto"),
			"article"						=>array("article","article","articulo"),
			"created !"						=>array("created !","créé !","registrado"),
			"hide"							=>array("hide","cacher","esconder"),
			"choose page"					=>array("choose page","choisir page","elegir la página"),
			"choose user"					=>array("choose user","choisir utilisateur","elegir usuario"),
			"choose owner"					=>array("choose owner","choisir propriétaire","elegir proprietario"),
			"subject (of post)"				=>array("subject (of post)","objet (du post)","objeto (del post)"),
			"content"						=>array("content","contenu","contenido"),
			"save"							=>array("save","enregistrer","salvar"),
			"site"							=>array("site","site","sitio"),
			"articles"						=>array("articles","articles","articulos"),
			"total articles"				=>array("total articles","total articles","total articulos"),
			"uploads"						=>array("uploads","téléversements","archivos"),
			"pages"							=>array("pages","pages","páginas"),
			"Pages"							=>array("Pages","Pages","Páginas"),
			"links"							=>array("links","liens","enlaces"),
			"users"							=>array("users","utilisateurs","usuarios"),
			"my-profile"					=>array("my-profile","mon-profil","mi-perfil"),
			"edit css"						=>array("edit css","éditer css","cambiar css"),
			"help"							=>array("help","aide","ayuda"),
			"login"							=>array("login","login","login"),
			"logout"						=>array("logout","déconnexion","salida"),
			", (some values have been modified !)"	=>array(", (some values have been modified !)",", (certaines valeurs ont été modifiées !)",", (Algunos valores se han modificado!)"),
			"some values have been modified !"	=>array("some values have been modified !","certaines valeurs ont été modifiées !","Algunos valores se han modificado!"),
			"sort/group by"					=>array("sort/group by","trier/regrouper","ordenar/reagruparse"),
			"search subject"				=>array("search subject","chercher objet","buscar sujeto"),
			"update all"					=>array("update all","tout mettre à jour","actualizar todo"),
			"{page-SandBox}"				=>array("{page-SandBox}","{page-SandBox}","{página-SandBox}"),
			"modify"						=>array("modify","modifier","modificar"),
			"delete"						=>array("delete","supprimer","suprimir"),
			"delete*"						=>array("delete*","supprimer*","suprimir*"),
			"del"							=>array("del","suppr","supr"),
			"del*"							=>array("del*","suppr*","supr*"),
			"edit"							=>array("edit","edit","edit"),
			"view"							=>array("view","afficher","examinar"),
			"articles list"					=>array("articles list","liste des articles","lista de articulos"),
			"by"							=>array("by","par","por"),
			"add article"					=>array("add article","ajouter article","añadir articulo"),
			"update date"					=>array("update date","mettre à jour date","actualizar fecha"),
			"update"						=>array("update","mettre à jour","actualizar"),
			"field"							=>array("field","champ","campo"),
			"empty"							=>array("empty","vide","vacío"),
			"uploaded"						=>array("uploaded","téléversé","subido"),
			"upload file"					=>array("upload file","téléverser fichier","subir fichero"),
			"list files"					=>array("list files","liste fichiers","ficheros lista"),
			"not allowed file"				=>array("not allowed file","fichiers interdits","archivos no permitidos"),
			"files allowed:"				=>array("files allowed:","fichiers autorisés","archivos permitidos"),
			"upload files to"				=>array("upload files to","téléverser fichier vers","subir ficheros para"),
			"directory"						=>array("directory","dossier","carpeta"),
			"directories"					=>array("directories","dossiers","carpetas"),
			"files"							=>array("files","fichiers","ficheros"),
			"modify link of"				=>array("modify link of","modifier lien de","modificar enlace de"),
			"links of"						=>array("links of","liens de","enlaces de"),
			"name"							=>array("name","nom","titulo"),
			"link"							=>array("link","lien","enlace"),
			"links list"					=>array("link list","liste liens","lista enlaces"),
			"description"					=>array("description","description","descripción"),
			"search by name"				=>array("search by name","chercher par nom","buscar con apellido"),
			"search by title"				=>array("search by title","chercher par titre","buscar con titulo"),
			"add link"						=>array("add link","ajouter lien","añadir enlace"),
			"linktext"						=>array("linktext","textelien","enlacetexto"),
			"page name in use for home is"	=>array("page name in use for home is","page choisie pour acceuil","página principal"),
			"select home page"				=>array("select home page","choisir page d'accueil","seleccione la página principal"),
			"home"							=>array("home","accueil","página principal"),
			"type"							=>array("type","type","tipo"),
			"param"							=>array("parameter","paramètre","parametro"),
			"or"							=>array("or","ou","o"),
			"website administrator mail"	=>array("website administrator mail","mail administrateur","mail administrador"),
			"add page of"					=>array("add page of","ajouter page de","añadir página de"),
			"modify page of"				=>array("modify page of","modifier page de","modificar página de"),
			"pages of"						=>array("pages of","pages de","páginas de"),
			"title"							=>array("title","titre","titulo"),
			"protected"						=>array("protected","protégé","protegido"),
			"add page"						=>array("add page","ajouter page","añadir página"),
			"pages list"					=>array("pages list","liste des pages","lista páginas"),
			"add user of"					=>array("add user of","ajoute utilisateur de","añadir usuario de"),
			"modify user of"				=>array("modify user of","modifier utilisateur de","modificar usuario de"),
			"users of"						=>array("users of","utilisateurs de","usuarios de"),
			"user"							=>array("user","utilisateur","usuario"),
			"login"							=>array("login","login","login"),
			"password"						=>array("password","mot de passe","contraseña"),
			"confirm password"				=>array("confirm password","confirmer mot de passe","confirma contraseña"),
			"level"							=>array("level","niveau","nivel"),
			"mail"							=>array("mail","mail","mail"),
			"contact user (mail)"			=>array("contact user (mail)","contactez utilisateur (mail)","contactar usuario (mail)"),
			"error password !"				=>array("error password !","erreur mot de passe","error contraseña !"),
			"password empty !"				=>array("password empty !","mot de passe vide !","falta contraseña!"),
			"title empty !"					=>array("title empty !","titre vide !","falta titulo !"),
			"wrong password or level !"		=>array("wrong password or level !","erreur de mot de passe ou niveau","error de contraseña o nivel !"),
			"login field empty !"			=>array("login field empty !","champ login vide !","no login !"),
			"username field empty !"		=>array("username field empty !","champ utilisateur vide !","no usuario !"),
			"login in use !"				=>array("login in use !","login déjà utilisé","login utilizado"),
			"wrong level !"					=>array("wrong level !","erreur niveau","error nivel"),
			"updated !"						=>array("updated !","mis à jour","actualizado"),
			"level-"						=>array("level-","niveau-","nivel-"),
			"users list"					=>array("users list","liste utilisateurs","lista usuarios"),
			"delete selected"				=>array("delete selected","supprimer sélection","suprimir selección"),
			"add user"						=>array("add user","ajouter utilisateur","añadir usuario"),
			"page"							=>array("page","page","página"),
			"pag"							=>array("pag","pag","pág "),
			"no user !"						=>array("no user !","pas d'utilisateur !","error usuario !"),
			"bad login or password"			=>array("bad login or password","mauvais login ou mot de passe","login o contraseña falsos"),
			"no link !"						=>array("no link !","pas de lien !","error enlace !"),
			"no page !"						=>array("no page !","pas de page !","sin página !"),
			"contact"						=>array("contact","contact","contacto"),
			"no article !"					=>array("no article !","pas d'article !","sin articulo !"),
			"child page of"					=>array("child page of","page fille de","pagina heredera de"),
			"login in use !"				=>array("login in use !","login utilisé","login utilizado"),
			"only admin can modify admin !"	=>array("only admin can modify admin !","uniquement admin peut modifier admin","solo admin pode modificar admin"),
			"date"							=>array("date","date","fecha"),
			"reset list"					=>array("reset list","réinitialiser liste","restablecer lista"),
			"reset values"					=>array("reset values","réinitialiser variables","restablecer variables"),
			"bad lineage !"					=>array("bad lineage !","mauvaise filiation !","linaje falso !"),
			"go id"							=>array("go id","aller à id","ir a id"),
			"edit id"						=>array("edit id","éditer id","cambiar id"),
			"go to"							=>array("go to","aller à","ir a"),
			"sort articles by"				=>array("sort articles by","classer articles avec","clasificar articulos con"),
			"reverse date"					=>array("reverse date","date inversée","fecha inversa"),
			"error"							=>array("error","erreur","error"),
			"last articles"					=>array("last articles","deniers articles","ultimos articulos"),
			"last pages"					=>array("last pages","denieres pages","ultimas páginas"),
			"last links"					=>array("last links","derniers liens","ultimos enlaces"),
			"Navigation"					=>array("Navigation","Navigation","Navegación"),
			"Links"							=>array("Links","Liens","Enlaces"),
			"only A-Z a-z 0-9 chars allowed for login value" =>array("only A-Z a-z 0-9 chars allowed for login value","seuls les caractères A-Z a-z 0-9 sont autorisés pour définir le login","sólo los caracteres A-Z a-z 0-9 se utilizan para definir el login"),
			"user name"						=>array("user name","nom utilisateur","nombre usuario"),
			"top content"					=>array("top content","haut de page","parte superior de la página"),
			"your mail"						=>array("your mail","votre mail","tu mail"),
			"subject"						=>array("subject","objet","objeto"),
			"subj"							=>array("subj","obj","obj"),
			"message"						=>array("message","message","message"),
			"solve"							=>array("solve","calculez","calcula"),
			"mail incorrect"				=>array("mail incorrect","mail incorrect","mail incorecto"),
			"error captcha"					=>array("error captcha","erreur captcha","error captcha"),
			"some field is empty"			=>array("some field is empty","certains champs sont vides","error"),
			"write to"						=>array("write to","écrire à","escribir a"),
			"logout done"					=>array("logout done","déconnexion ok","desconectado"),
			"head code (head tag)"			=>array("head code (head tag)","head code (head balise)","head code (head elemento)"),
			"End"							=>array("End","Fin","Fin"),
			"sql host"						=>array("sql host","sql hôte","sql host"),
			"sql database"					=>array("sql database","sql base","sql base"),
			"sql login"						=>array("sql login","sql login","sql login"),
			"sql password"					=>array("sql password","sql mot de passe","sql password"),
			"ulyxex tables prefix"			=>array("ulyxex tables prefix","ulyxex tables prefixe","ulyxex tables prefix"),
			"navigation"					=>array("navigation","navigation","navegación"),
			"redirect to link"				=>array("redirect to link","rediriger vers lien","redirigir al enlace"),
			"or add new type"				=>array("or add new type","ou ajouter nouveau type","o agregar nuevo typo"),
			"add file"						=>array("add file","ajouter fichier","agregar fichero"),
			"lang"							=>array("lang","langue","lengua"),
			"other"							=>array("other","autre","otro"),
			"main css"						=>array("main css","css principal","css principal"),
			"items by page admin"			=>array("items by page admin","éléments par page admin","elementos por página admin"),
			"articles by page"				=>array("articles by page","articles par page","artículos por página"),
			"files of"						=>array("files of","fichiers de","ficheros de"),
			"ftp of"						=>array("ftp of","ftp de","ftp de"),
			"tabulation"					=>array("tabulation","tabulation","tabulación"),
			"file name"						=>array("file name","nom de fichier","nombre de archivo"),
			"add"							=>array("add","ajouter","agregar"),
			"total files"					=>array("total files","total fichiers","total ficheros"),
			"untie"							=>array("untie","détacher","soltar"),
			"tie"							=>array("tie","attacher","liar"),
			"assign"						=>array("assign","attribuer","asignar"),
			"menu admin"					=>array("menu admin","menu admin","menu admin"),
			"key"							=>array("key","clé","llave"),
			"change password"							=>array("change password","Changer le mot de passe","Cambiar contraseña"),
			"list of uploaded files by FTP in \"files\" directory you can assign to user"							
											=>array(
											"list of uploaded files by FTP in \"files\" directory you can assign to user",
											"liste des fichiers téléchargés par FTP dans le répertoire \"files\" que vous pouvez attribuer l'utilisateur",
											"lista de archivos cargados por FTP en el directorio \"files\" que puede asignar al usuario"),
			"Update all articles and pages to lang"
											=>array("Update all articles and pages to lang",
											"Mettre à jour tous les articles et pages en langue",
											"Actualizar todos los artículos y páginas al idioma"),
			"Transfert data from user (a) to user (b)"     
											=>array("Transfert data from user (a) to user (b)",
											"Transfert de données de l'utilisateur (a) à l'utilisateur (b)",
											"Transferencia de datos del usuario (a) al usuario (b)"),
			"Transfert all articles, pages and files from user (a) to user (b)"     
											=>array("Transfert all articles, pages and files from user (a) to user (b)",
											"Transférer tous les articles, pages et fichiers de l'utilisateur (a) à l'utilisateur (b)",
											"Transfiera todos los artículos, páginas y archivos del usuario (a) al usuario (b)"),
			"from user"						=>array("from user","de l'utlisateur","desde usuario"),					
			"to user"						=>array("to user","vers l'utilisateur","al usuario"),
			"show controllers"				=>array("show controllers","voir controleurs","ver controllers"),
			"show/hide password"			=>array("show/hide password","voir/cacher mot de passe","ver/esconder contraseña"),	
			"change password ?"				=>array("change password ?","changer passe ?","cambiar contraseña ?"),							
			"xxxxxxxxxx"					=>array("en-xxxxxxxx","fr-xxxxxxxxxx","sp-xxxxxxxxxx")
		);
	}
	public function w($str){
		// simple tranlation
		return $this->translate($str);
	}	
	public function wl($str){
		// translation with left space
		return "&nbsp;".$this->translate($str);
	}
	public function wr($str){
		// translation with right space
		return $this->translate($str)."&nbsp;";
	}
	public function wlr($str){
		// translation with left and right space
		return "&nbsp;".$this->translate($str)."&nbsp;";
	}
	public function wx($str){
		// translation with right sign
		return $this->translate($str).">";
		// return $this->translate($str);
	}
	function translate($str){
		// *word* not translate word with stars
		if ($str[0] == "*" && mb_substr($str,-1) == "*") {
			return mb_substr($str,1,-1);
		} elseif (isset($this->translation[$str][L])){
			return preg_replace("/ /","&nbsp;",$this->translation[$str][L]);
		} else { // default return word
			return $str;
		}
	}
	function getLang() {
		$lang = array("en","fr","es");
		return $lang[L];
	}
}
?>
