<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ArticleModify extends Admin {
	function __construct($artId=Null){
		$this->h = New Htmlz();
		$this->t = New Translate();
		$this->tree = New Tree();
		$this->article_id = $artId;
		$this->Select = 	SELECT.
									ARTICLES.".*,".
									USERS.".USERNAME,".USERS.".ID IDUSERID,".USERS.".USERLEVEL,".
									PAGES.".NAME,".PAGES.".ID PAGEID,".PAGES.".USERID USERPAGEID";
		$this->From = 		FROM.
									ARTICLES.
									INNERJOIN.PAGES._ON_.ARTICLES.".PAGEID = ".PAGES.".ID".
									INNERJOIN.USERS._ON_.ARTICLES.".USERID = ".USERS.".ID";
		$this->Cond = array();
	}
	public function update_article(){
		$h = $this->h;
		$now = time();
		$date = date("Y-m-d H:i:s",$now);
		$hide = (isset($_POST['hide']))?1:0;
		$addFile = "";
		if ($_SESSION['level'] < 4) {
			if (!empty($_FILES['addfile']["name"])) {
				$fileName = $_FILES['addfile']["name"];
				$ext = $h->get_ext($fileName);
				if ( !in_array($ext, $h->extUploadAllowed) && $_SESSION['level'] > 1) exit("file extension not allowed ! ".$ext);
				$fileName = sprintf("%03d",rand(0,999))."_".$h->text2ascii($fileName);
				$sendFile = move_uploaded_file($_FILES['addfile']["tmp_name"],"files/".$fileName);
				if ($sendFile) {
					$query = INSERTINTO.FILES." (FILENAME,USERID,DATEFILE) VALUES (".
											"'"."files/".$fileName."',".$h->num($_POST['userid']).
											",'".$date."');";
					$query = $this->q($query);
					$addFile = $h->br($h->tag_by_fileType("files/".$fileName,$_FILES['addfile']["type"]));
				} else {
					$addFile = $h->br($h->color("file ".$fileName." not uploaded!","red"));
				}
			}
		}
		/* in case of no page created		*/
		$content = $addFile.$h->safe($_POST['content']);
		$pageId = isset($_POST['parent'])?$h->num($_POST['parent']):2; // 2 = sandbox id
		$query = UPDATE.ARTICLES.SET."SUBJECT = '".$h->safe($_POST['subject'])."'";
		if (isset($_POST['userid']) && $_POST['userid'] != $_POST['old_userid']) $query .=	",USERID = ".$h->num($_POST['userid']); // change userId
		$query .= isset($_POST['updateDate'])?",DATEPAGE = '".$h->safe($_POST['newDate'])."'":"";
		
		$query .=					",LANG = '".$h->safe($_POST['lang'])."'".
									",CONTENT = '".$content."'".
									",HIDE = ".$hide.
									",PAGEID = ".$pageId.
									WHERE."ID = ".$this->article_id;
		$query = $this->q($query);
		return $this->h->h3("article ".strip_tags($_POST['subject'])." updated !","class='warning'");
	}
	public function data_article(){
		$h = $this->h;$t = $this->t;
		switch ($_SESSION['level']){
			case 1:
				// user level 1 can't edit others user level 1
				if ($_SESSION['userid'] != 1){ // admin edit all
					$this->Cond[] = "(".USERS.".USERLEVEL > ".$_SESSION['level']._OR_.USERS.".ID = ".$_SESSION['userid'].")"; // all level 2, 3 and 4 user's articles, user's articles 
					$this->Cond[] = USERS.".ID != 1"; // but not admin articles
				}
				$this->Cond[] = ARTICLES.".ID = ".$this->article_id;
				break;
			case 2:
				$this->Cond[] = "(".USERS.".ID = ".$_SESSION['userid']._OR_.PAGES.".USERID = ".$_SESSION['userid'].")"; // user's articles and articles in user's pages
				$this->Cond[] = USERS.".ID != 1"; // but not admin articles
				$this->Cond[] = ARTICLES.".ID = ".$this->article_id;
				break;
			default:
				$this->Cond[] = ARTICLES.".USERID = ".$_SESSION['userid']; // user's articles only
				$this->Cond[] = ARTICLES.".ID = ".$this->article_id;
		}
		$query = $this->Select.$this->From.$this->Where($this->Cond);
		$query = $this->q($query);
		if ($dat = $this->fetch($query)){
			$now = time();
			$date = date("Y-m-d H:i:s",$now);
			$hideParams =  ($dat['HIDE'])?" checked=\"checked\"":null;
			$_[] = $h->input("hidden","update",1);
			$_[] = $h->input("hidden","art",$dat['ID']);
			$_page = ($_SESSION['level'] <= 3)?$t->wr("choose page").$this->tree->pathsSelect($dat['PAGEID'],0,"page"):"page: SandBox".$h->input("hidden","pageid",2); // 2 = sandbox id
			$_user = ($_SESSION['level'] == 1)?$h->input("hidden","old_userid",$dat['USERID']).$t->wr("choose user").$this->array_users($dat['USERID']):$h->input("hidden","old_userid",$dat['USERID']).$h->input("hidden","userid",$dat['USERID']);
			
			$_[] = $h->p($_page." ".$_user);
			$_hideArticle = $t->wr("hide").$h->input("checkbox","hide","on",$hideParams);
			$_hideArticle .= " -".$t->wlr("date")."[".$dat['DATEPAGE']."] > ".$h->input("text","newDate",$date,"size=19").$t->wlr("update").$h->input("checkbox","updateDate","1");
			$_hideArticle .= " -".$t->wlr("lang").$h->input("text","lang",$dat['LANG'],"size=3");
			$_[] = $h->p($_hideArticle).$h->p($t->wr("subject").$h->input("text","subject",$h->uly_htmlentities($dat['SUBJECT']),"size=".FIELDWIDTH));
			$tmp = ($_SESSION['level'] <= 3)?$t->wr("add file").$h->input("file","addfile","","size=30").$t->wlr("or"):null;
			$_[] = $h->p($tmp."[".$h->ahref("admin.php?action=list_files",$t->w("list files"),"target='_blank'")."]");
			$_[] = $h->p($h->span("&lt;article&gt; ","class='tag'").$h->input("submit","save",$t->w("update")));
			$_[] = $h->p($h->textarea($dat['CONTENT'],"content",array(12,FIELDWIDTH)));
			$_[] = $h->p($h->span("&lt;/article&gt; ","class='tag'").$h->input("submit","save",$t->w("update")));
			$inputs = implode("",$_);
			return $h->form($inputs,"article","post","admin.php?action=modify_article","multipart/form-data");
		} else {
			return $h->p("error article data ");
		}
	}
}
?>
