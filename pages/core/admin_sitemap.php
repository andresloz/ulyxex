<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
/* exemple sitemap
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <url>
      <loc>http://www.example.com/</loc>
      <lastmod>2005-01-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.8</priority>
   </url>
</urlset>  
*/
class SiteMap extends Ulyxex {
	function __construct($pageId=0,$sortStyle=0){
		$this->h = New Htmlz();
		$this->x = New XmlSitemap();
		//~ articles
		$this->Select = 	SELECT.
								ARTICLES.".ID ARTID,".ARTICLES.".CONTENT,".ARTICLES.".DATEPAGE";
		$this->From = 		FROM.
								ARTICLES.INNERJOIN.PAGES._ON_.ARTICLES.".PAGEID = ".PAGES.".ID";
		//~ pages
		$this->SelectPages = 	SELECT.
								PAGES.".ID PAGID,".PAGES.".DESCRIPTION,".PAGES.".DATEPAGE";
		$this->FromPages = 		FROM.
								PAGES;
	}
	public function sitemap_page_data(){
		$h = $this->h;
		$x = $this->x;
		$query = "SELECT DISTINCT ".PAGES.".TYPEPAGE".FROM.PAGES;
		$query = $this->q($query);
		$typepages = array();
		while ($line = $this->fetch($query) ){
			if ($line['TYPEPAGE'] != 'link') {
				$typepages[] = $line['TYPEPAGE'];
			}

		}
		$this->Cond = array();
		$this->Cond[] = PAGES.".HIDE = 0";
		$this->Cond[] = PAGES.".TYPEPAGE"._IN_."('".implode("','",$typepages)."')";
		$query = $this->SelectPages.$this->FromPages.$this->Where($this->Cond).ORDERBY."DATEPAGE DESC";
		$query = $this->q($query);
		$sitemap_content = "";
		if ($line = $this->fetch($query)){
			$temp = $x->loc($h->root_url("index.php?k=page&amp;v=".$line['PAGID']."&amp;t=".$h->clean_short_url($line['DESCRIPTION'])));
			$temp .= $x->lastmod(substr($line['DATEPAGE'],0,10));
			$sitemap_content .= $x->url($temp);
			while ($line = $this->fetch($query) ){
				$temp = $x->loc($h->root_url("index.php?k=page&amp;v=".$line['PAGID']."&amp;t=".$h->clean_short_url($line['DESCRIPTION'])));
				$temp .= $x->lastmod(substr($line['DATEPAGE'],0,10));
				$sitemap_content .= $x->url($temp);
			}
			return $sitemap_content;
		} else {
			return "";
		}
	}
	public function sitemap_article_data(){
		$h = $this->h;
		$x = $this->x;
		$this->Cond = array();
		$this->Cond[] = ARTICLES.".HIDE = 0";
		$this->Cond[] = PAGES.".HIDE = 0";
		$query = $this->Select.$this->From.$this->Where($this->Cond).ORDERBY."DATEPAGE DESC";
		$query = $this->q($query);
		$sitemap_content = "";
		if ($line = $this->fetch($query)){
			$temp = $x->loc($h->root_url("index.php?k=article&amp;v=".$line['ARTID']."&amp;t=".$h->clean_short_url($line['CONTENT'])));
			$temp .= $x->lastmod(substr($line['DATEPAGE'],0,10));
			$sitemap_content .= $x->url($temp);
			while ($line = $this->fetch($query) ){
				$temp = $x->loc($h->root_url("index.php?k=article&amp;v=".$line['ARTID']."&amp;t=".$h->clean_short_url($line['CONTENT'])));
				$temp .= $x->lastmod(substr($line['DATEPAGE'],0,10));
				$sitemap_content .= $x->url($temp);
			}
			return $sitemap_content;
		} else {
			return "";
		}
	}
}
?>
