<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class UserDataTransfert extends Admin {
	function __construct($pageId=Null){
		$this->h = New Htmlz();
		$this->t = New Translate();
        $this->tree = New Tree();
	}
	public function user_data_transfert(){
		$h = $this->h;$t = $this->t;
		if ($_POST['userfrom'] == $_POST['userto']) return $h->h3("Identical users!","class='warning'");
		$query = SELECT."ID".FROM.USERS.WHERE."LOGIN = '".$h->safe($_POST['userfrom'])."'";
		$query = $this->q($query);
		$dat = $this->fetch($query);
		$userFrom = $dat["ID"];
		
		$query = SELECT."ID".FROM.USERS.WHERE."LOGIN = '".$h->safe($_POST['userto'])."'";
		$query = $this->q($query);
		$dat = $this->fetch($query);
		$userTo = $dat["ID"];
		
		$query = UPDATE.ARTICLES.SET."USERID = '".$userTo."'".
		WHERE.ARTICLES.".USERID = '".$userFrom."'"._AND_.ARTICLES.".PAGEID != 2";
		$query = $this->q($query);
		
		$query = UPDATE.PAGES.SET."USERID = '".$userTo."'".
		WHERE.PAGES.".USERID = '".$userFrom."'"._AND_.PAGES.".NAME != 'SANDBOX'";
		$query = $this->q($query);
		
		$query = UPDATE.FILES.SET."USERID = '".$userTo."'".WHERE.FILES.".USERID = '".$userFrom."'";
		$query = $this->q($query);

		return $h->h3($t->wx("transfert")." from ".$_POST['userfrom']." to ".$_POST['userto'].$t->wl("done !"),"class='warning'");
	}
	public function data_user($login=null){
		$h = $this->h;$t = $this->t;
		if ($_SESSION['level'] == 1) {
			$users = array();
			$query = 	SELECT."USERNAME,LOGIN".FROM.USERS;
			$query = $this->q($query);
			while ( $line = $this->fetch($query) ){
				$users[$line['USERNAME']." (".$line['LOGIN'].")"] = $line['LOGIN'];
			}
			$selectUserFrom = $h->select($users,"userfrom",$h->safe($login));
			$selectUserTo = $h->select($users,"userto");
			
			$_[] = $h->input("hidden","update",1);
			$_[] = $h->h2($t->w("Transfert all articles, pages and files from user (a) to user (b)"));
			$_[] = $h->p( $t->wr("from user")."(a) ".$selectUserFrom);
			$_[] = $h->p( $t->wr("to user")."(b) ".$selectUserTo);
			$_[] = $h->input("submit","save",$t->w("update"));
			$inputs = implode("",$_);
			return $h->form($inputs,"user","post","admin.php?action=user_data_transfert");
		} else {
			return $h->p("error");
		}
	}
}
?>
