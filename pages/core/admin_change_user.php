<?php
/* Ulyxex version 1.5.4.1 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ChangeUser extends Admin {
	function __construct($pageId=Null){
		$this->h = New Htmlz();
		$this->t = New Translate();
        $this->tree = New Tree();
	}
	public function change_user(){
		$h = $this->h;$t = $this->t;
		$query = SELECT."ID".FROM.USERS.WHERE."LOGIN = '".$h->safe($_POST['user'])."'";
		$query = $this->q($query);
		$dat = $this->fetch($query);
		$user = $dat["ID"];
		
		$query = SELECT."ID".FROM.USERS.WHERE."LOGIN = '".$h->safe($_POST['user2'])."'";
		$query = $this->q($query);
		$dat = $this->fetch($query);
		$user2 = $dat["ID"];
		
		$query = UPDATE.ARTICLES.SET."USERID = '".$user2."'".WHERE.ARTICLES.".USERID = '".$user."'";
		$query = $this->q($query);
		$query = UPDATE.PAGES.SET."USERID = '".$user2."'".WHERE.PAGES.".USERID = '".$user."'";
		$query = $this->q($query);
		$query = UPDATE.FILES.SET."USERID = '".$user2."'".WHERE.FILES.".USERID = '".$user."'";
		$query = $this->q($query);

		return $h->h3($t->wx("user").$t->wl("updated !"),"class='warning'");
	}
	public function data_user(){
		$h = $this->h;$t = $this->t;
		if ($_SESSION['level'] == 1) {
			$_[] = $h->input("hidden","update",1);
			$_[] = $h->h2($t->w("Update all articles and pages to user"));
			$_[] = $h->p( $t->wr("user login").$h->input("text","user"));
			$_[] = $h->p( $t->wr("to user login").$h->input("text","user2"));
			$_[] = $h->input("submit","save",$t->w("update"));
			$inputs = implode("",$_);
			return $h->form($inputs,"user","post","admin.php?action=change_user");
		} else {
			return $h->p("error");
		}
	}
}
?>
