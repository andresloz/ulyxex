<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class Pages extends Admin {
	public function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
		if (isset($_GET['pg'])) {
			$this->pagePos = $this->h->num($_GET['pg']);
			$_SESSION['pgp'] = $_GET['pg'];
		} elseif (isset($_SESSION['pgp']) && !empty($_SESSION['pgp']))  {
			$this->pagePos = $this->h->num($_SESSION['pgp']);
			$_GET['pg'] = $_SESSION['pgp'];
		} else {
			$this->pagePos = 0;
		}
		
		$this->Select = SELECT.
							PAGES.".ID,".PAGES.".NAME,".PAGES.".HIDE,".PAGES.".TYPEPAGE,".PAGES.".DATEPAGE,".USERS.".USERNAME,".USERS.".ID USERID,".USERS.".USEROWNER";
		$this->From = 	FROM.
							PAGES.
							INNERJOIN.USERS._ON_.PAGES.".USERID = ".USERS.".ID";
		$this->SelectCount = 	SELECT."COUNT(".PAGES.".ID) AS NUM ";
		$this->OrderSort = "";
		$this->Cond = array();
		$this->sortMethods = array("",PAGES.".DATEPAGE DESC",PAGES.".NAME",USERS.".USERNAME","");  
		if (!isset($_SESSION['admin_page_sort'])) $_SESSION['admin_page_sort'] = $this->sortMethods[1];
		$this->pageArtsTotal = $this->sum_arts_by_pages();
	}
	public function update_pages(){
		$formValues = array();
		$values = $_POST;
		foreach($values as $k=>$v){
			if ( preg_match("/\_/",$k) ){
				list($key,$id) = explode("_",$k);
				$formValues[$id][$key] = $v;
			}
		}
		foreach($formValues as $id=>$value){
			$query = 0;
			if (isset($value['delete'])){
				// put articles is sandbox
				$query = UPDATE.ARTICLES.SET."PAGEID = 2".WHERE."PAGEID = ".$id;
				$query = $this->q($query);
				// new parent id is sandbox
				$query = UPDATE.PAGES.SET."PARENTID = 2".WHERE."PARENTID = ".$id;
				$query = $this->q($query);
				$query = DELETE.FROM.PAGES.WHERE."ID = ".$id;
				$query = $this->q($query);
				
			}
			if (isset($value['hide']) && $value['oldhide'] == 0){
				$query = UPDATE.PAGES.SET."HIDE = 1".WHERE."ID = ".$id;
				$query = $this->q($query);
			} elseif (!isset($value['hide']) && $value['oldhide'] == 1){
				$query = UPDATE.PAGES.SET."HIDE = 0".WHERE."ID = ".$id;
				$query = $this->q($query);
			}
		}
		return $this->h->h3($this->t->w("some values have been modified !"),"class='warning'");
	}
	public function list_pages(){
		$h = $this->h;$t = $this->t;
		$p = New Tree();
		$paths = $p->paths();
		$pathById = array();
		foreach ($paths as $pth=>$arr) {
			$pathById[$arr[1]] = $pth;
		}
		$pgPos =($this->pagePos)?($this->pagePos - 1) * ITEMS:0;
		if (isset($_POST['sort'])) $_SESSION['admin_page_sort'] = $this->sortMethods[$_POST['sort']];
        if (isset($_GET['ownerFilter'])) $_SESSION['admin_pages_filter'] = PAGES.".USERID = ".$h->num($_GET['ownerFilter']);
		if (isset($_POST['search_title'])) $_SESSION['admin_pages_search_title'] = " MATCH(".PAGES.".NAME) AGAINST('".$h->safe($_POST['search_title'])."') ";
		if (isset($_POST['reset'])){
			if (isset($_SESSION['admin_pages_filter'])){$_SESSION['admin_pages_filter'] = "";}
			if (isset($_SESSION['admin_pages_search_title'])){$_SESSION['admin_pages_search_title'] = "";}
			if (isset($_SESSION['admin_page_sort'])) $_SESSION['admin_page_sort'] = $this->sortMethods[1];
		}
		// always page position = 1 if use search, only ITEMS first results
		if ( isset($_SESSION['admin_pages_search_title']) && !empty($_SESSION['admin_pages_search_title']) ) $pgPos = 1;
		$this->OrderSort = ORDERBY.$_SESSION['admin_page_sort']." LIMIT ".$pgPos.",".ITEMS;
		// 1) can create users etc.
		// 2) can create articles and pages
		// 3) can create articles everywhere
		// 4) can create articles only in sandbox
		switch ($_SESSION['level']){
			case 1: 
				if ($_SESSION['userid'] != 1){
					$this->Cond[] = "(".USERS.".USERLEVEL > ".$_SESSION['level']._OR_.USERS.".ID = ".$_SESSION['userid'].")"; // all level 2, 3 and 4 user's pages, user's pages 
					$this->Cond[] = USERS.".ID != 1"; // but not admin articles
				}
				if (isset($_SESSION['admin_pages_filter']) && !empty($_SESSION['admin_pages_filter']) ) $this->Cond[] = $_SESSION['admin_pages_filter'];
				if (isset($_SESSION['admin_pages_search_title']) && !empty($_SESSION['admin_pages_search_title']) ) $this->Cond[] = $_SESSION['admin_pages_search_title'];
				break;
			case 2:
				$this->Cond[] = PAGES.".USERID = ".$_SESSION['userid'];
				if (isset($_SESSION['admin_pages_filter']) && !empty($_SESSION['admin_pages_filter']) ) $this->Cond[] = $_SESSION['admin_pages_filter']; 
				if (isset($_SESSION['admin_pages_search_title']) && !empty($_SESSION['admin_pages_search_title']) ) $this->Cond[] = $_SESSION['admin_pages_search_title'];
				break;
		}
		$query = $this->Select.$this->From.$this->Where($this->Cond).$this->OrderSort;
		$query = $this->q($query);
		
		$sort = array($t->w("date")=>1,$t->w("title")=>2,$t->w("user")=>3);
		$type = array_search($_SESSION['admin_page_sort'],$this->sortMethods);
		$inputs = $h->p($h->input("submit","",$t->w("sort/group by"))." ".$h->select($sort,"sort",$type));
		$sortForm = $h->form($inputs,"sort","post","admin.php?action=pages");
		
		if (isset($_POST['search_title'])) {
			$searchValue = $_POST['search_title'];
		} else {
			$searchValue = "";
		}
		$searchForm = $this->search_form("search_title","search by title","pages",$searchValue);
		$goIdForm = $this->go_id("page","modify_page"); 
		$resetForm = $this->reset_form("pages");
		
		$pageListSubmit = $h->p($h->input("submit","",$t->w("update all")));
		
		$pageListTitle = $h->h3($t->w("pages list"));
		$pageList = $h->input("hidden","update",1);
		
		while ( $line = $this->fetch($query) ){
			$id = $h->input("hidden","id_".$line['ID'],$line['ID'])."id".$h->f4d($line['ID']);
			$id = "id".$h->f4d($line['ID']);
			$date = substr($line['DATEPAGE'],0,10);
			$arts =(isset($this->pageArtsTotal[$line['ID']]))?$h->ahref("admin.php?action=articles&pageFilter=".$line['ID'],$h->f4d($this->pageArtsTotal[$line['ID']])):$h->f4d(0);
			$arts = "arts".$arts;
			$checked = ($line['HIDE'])?" checked=\"checked\"":null;
			$hide = $t->wr("hide").$h->input("checkbox","hide_".$line['ID'],"on",$checked).
			$h->input("hidden","oldhide_".$line['ID'],$line['HIDE']);
			if ($line['TYPEPAGE'] == "page") { // visualisation of type
				$type = null;
			} elseif ($line['TYPEPAGE'] == "link"){
				$type = $h->span($t->w("link"),"style='color:red;font-weight:bold;'");
			} else {
				$type = $h->span($line['TYPEPAGE'],"style='color:red;font-weight:bold;'");
			}
			$modify = $h->ahref("admin.php?action=modify_page&amp;page=".$line['ID'],$t->w("modify"),"class='formframe'");
			$delete = ($line['NAME'] != "SANDBOX")?$t->wr("del").$h->input("checkbox","delete_".$line['ID'],"on"):str_repeat("*",strlen($t->w("del")."**"));
			$view = $t->wx("title").$h->ahref("index.php?k=".$line['TYPEPAGE']."&amp;v=".$line['ID'],$h->strWidth(strip_tags($line['NAME']),28),"class='formframe' target=_blank title='".
			$h->uly_htmlentities($pathById[intval($line['ID'])])."'");
			// only level 1 can browser pages of differents users
			$by = ($_SESSION['level'] == 1)?$t->wx("by").$h->ahref("admin.php?action=pages&amp;ownerFilter=".$line['USERID'],$h->strWidth($line['USERNAME'],12),"class='formframe' title='".$h->uly_htmlentities($line['USERNAME'])."'"):null;
			$_ = array($this->bordered($id),$this->bordered($date),$this->bordered($arts),$this->bordered($hide),$modify,$this->bordered($delete),$view,$by,$type);
			$content = implode(" ",$_);
			$pageList .= $h->p($content);
		}
		
		$nextPages = $this->next_pages();
		$pageList = $pageListTitle.$pageListSubmit.$pageList.$pageListSubmit.$nextPages;
		return $sortForm.$searchForm.$goIdForm.$resetForm.$h->form($pageList,"pages","post","admin.php?action=pages&amp;pg=".$this->pagePos);
	}
	public function list_pages_select($pageId,$name){
		$h = $this->h;
		$query = SELECT.PAGES.".ID,".PAGES.".NAME".FROM.PAGES;
		$query = $this->q($query);
		$options = array();
		while ( $line = $this->fetch($query) ){
			if ($line['ID'] == $pageId){
				$options[$line['NAME']] = $line['ID'];
			} else {
				$options[$line['NAME']] = $line['ID'];
			}
		}
		return $h->select($options,$name,$pageId);
	}
	public function next_pages(){
		$h = $this->h;
		$t = $this->t;
		$query = $this->SelectCount.$this->From.$this->Where($this->Cond);
		$query = $this->q($query);
		$dat = $this->fetch($query);
		$pagesNumbers = new NextPages($dat['NUM'],"action=pages");
		$result = $h->p($pagesNumbers->numbers());
		$result .= $h->p("(".$this->t->wr("total pages")."= ".$dat['NUM'].")");
		$result .= $h->p("(".$this->t->wr("total articles")."= ".array_sum($this->pageArtsTotal).")");
		return $result;
	}
}
?>
