<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
require_once("index_controllers.php");
class Controllers extends Admin {
	function __construct($adminController,$viewController){
		$this->h = New Htmlz();
		$this->t = New Translate();
		$this->adminControllers = $adminController;
		$this->viewControllers = $viewController;
	}
	public function show_controllers(){
		$h = $this->h;$t = $this->t;
		$data[] = $h->h1("Show all controllers");
		$data[] = $h->h3("Admin controllers var \$adminController");
		$table = array();
		$table[] = $this->tr(array($t->w("key"),$t->w("title"),"script",$t->w("menu admin"),$t->w("level")),"th");
		foreach($this->adminControllers as $key => $value) {
			$table[] = $this->tr(array($key,$value[0],$value[1],$value[2],$value[3]));
		}
		$data[] = $h->table(implode("\n",$table));
		
		$data[] = $h->h3("View controllers var \$viewController");
		$table = array();
		$table[] = $this->tr(array($t->w("key"),"script"),"th");
		foreach($this->viewControllers as $key => $value) {
			$table[] = $this->tr(array($key,$value));
		}
		$data[] = $h->table(implode("\n",$table));
		return implode("\n",$data);
	}
	public function tr($tds=array(),$th=Null) {
		$h = $this->h;
		$result = "";
		foreach($tds as $td) {
			if ($td != null) {
				($th)?$result .= $h->th($td):$result .= $h->td($td);
			} else {
				($th)?$result .= $h->th($h->color("null","red")):$result .= $h->td($h->color("null","red"));
			}
		}
		$result = $h->tr($result);
		return $result;
	}
}
?>
