<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ViewUser extends Ulyxex {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
		$this->pos = isset($_GET['pg'])?$this->h->num($_GET['pg']):0;
		$this->numPages = 0;
	}
	public function user_data($userId){
		$h = $this->h;$t = $this->t;
		$query = SELECT.
					"USERNAME,DESCRIPTION,MAIL".
				FROM.
					USERS.
				WHERE."ID=".$userId;
		$query = $this->q($query);
		$result = "";
		$dat = $this->fetch($query);
		if ($dat == 0){
			return $h->h3($t->w("no user !"));
		}else{
			$result .= $h->h1($t->w("user name").": ".$dat['USERNAME'],"class='username'");
			$result .= $h->p($t->w("description").": ".$dat['DESCRIPTION'],"class='userdescription'");
			$result .= $h->p($h->ahref("index.php?k=contact&amp;v=".$userId,$t->w("contact user (mail)")),"class='contenturl'");
			return $result;
		}
	}
	public function user_links($userId){
		$h = $this->h;$t = $this->t;
		$result = "";
		$this->OrderSort = ORDERBY."ID DATEPAGE LIMIT 0,10";
		/***last articles ***************/
		$query = SELECT.
					ARTICLES.".ID,".ARTICLES.".SUBJECT".
				FROM.
					ARTICLES.INNERJOIN.PAGES._ON_.ARTICLES.".PAGEID = ".PAGES.".ID".
				WHERE.
					ARTICLES.".HIDE = 0"._AND_.PAGES.".HIDE = 0"._AND_.ARTICLES.".USERID=".$userId.
				ORDERBY.
					ARTICLES.".DATEPAGE DESC LIMIT 0,10";
			
		$query = $this->q($query);
		$items = "";
		if ($line = $this->fetch($query)){
			$items.= $h->li($h->span("id ".$h->f4d($line['ID'])." ","style='font-size:0.8em'").$h->ahref("index.php?k=article&amp;v=".$line['ID'],$h->strWidthLeft($line['SUBJECT'],56)));
			while ( $line = $this->fetch($query) ){
				$items.= $h->li($h->span("id ".$h->f4d($line['ID'])." ","style='font-size:0.8em'").$h->ahref("index.php?k=article&amp;v=".$line['ID'],$h->strWidthLeft($line['SUBJECT'],56)));
			}
			$result .= $h->h3($t->wr("last articles")."(10 max)","class='articleslisttitle'").$h->ul($items,"class='articleslist'");	
		}
		/***last pages    ***************/
		$query = SELECT.
					"ID,NAME,TYPEPAGE".
				FROM.
					PAGES.
				WHERE.
					"HIDE = 0"._AND_."USERID=".$userId.
				ORDERBY.
					"ID DESC LIMIT 0,10";					
		$query = $this->q($query);
		$items = "";
		if ($line = $this->fetch($query)){
			$items.= $h->li($h->span("id ".$h->f4d($line['ID'])." ","style='font-size:0.8em'").$h->ahref("index.php?k=".$line['TYPEPAGE']."&amp;v=".$line['ID'],$h->strWidthLeft($line['NAME'],56)));
			while ( $line = $this->fetch($query) ){
				$items.= $h->li($h->span("id ".$h->f4d($line['ID'])." ","style='font-size:0.8em'").$h->ahref("index.php?k=".$line['TYPEPAGE']."&amp;v=".$line['ID'],$h->strWidthLeft($line['NAME'],56)));
			}
			$result .= $h->h3($t->wr("last pages")."(10 max)","class='linkslisttitle'").$h->ul($items,"class='linkslist'");
		}
		/* all lists      ***************/
		return $result;
	}
}
?>
