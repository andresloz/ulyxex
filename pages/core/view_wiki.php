 <?php
/* Ulyxex version 1.5.3.4 *****************/
if (!isset($index)) exit("out of index site");
/****************************************/
/* page elements						*/
require_once("core/view_page.php");
$pg_params = $u->page_params($paramId);
$o_pag = new ViewPage($paramId,$pg_params['SORTSTYLE']);
$o_tree = new Tree();

$css = $h->css($home['CSS'],"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title(strip_tags($pg_params['NAME']));
$pageHead = $pg_params['PAGEHEAD'];
$head = $h->head($ico.$meta.$css.$title.$pageHead);

$topPage = $h->nav($o_tree->pagesNavigation($pageId=$paramId,$pageType=$typePage),"class='navigation'");
//~ $topPage .= $h->header($pg_params['DESCRIPTION']);

$nextPages = $h->p($o_pag->next_pages,"class='pagenumbers'");
$navPages = $h->p($h->ahref("index.php?k=".$typePage."&amp;v=".$paramId,$t->wr("page")."url"),"class='pageurl'");
$credits = $h->ulyxCredits();
$bottomPage = $h->footer($nextPages.$navPages.$credits);
/****************************************/
/* page data							*/
// analyse name
// construction de l'url pour l'api wikipedia
//~ $pageRef = $pg_params['NAME'];
$pageRef = $pg_params['PAGEHEAD'];
$endPoint = "https://fr.wikipedia.org/w/api.php";
$params = [
    "action" => "parse",
    "page" => $pageRef,
    "mobileformat" => true,
    "format" => "json"
];

$url = $endPoint."?".http_build_query( $params );
echo $url;
$ch = curl_init($url);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
$output = curl_exec($ch);
curl_close($ch);

// recupération des données json
$result = json_decode($output, true);

$title = $result["parse"]["title"];
$text = $result["parse"]["text"]["*"];

//~ supprime les balises inutiles
$text = strip_tags($text, '<p><ul><li><h2><h3>');
//~ ajoute des retours de ligne pour les balises
$text = str_replace("</h2>","</h2>\n",$text);
$text = str_replace("</p>","</p>\n",$text);
$text = str_replace("</ul>","</ul>\n",$text);
$text = str_replace("</li>","</li>\n",$text);
//~ divise en lignes
$text = preg_split("/\r\n|\n|\r/", $text);

// données
$data = array();
$data[] = $h->h1($title);

$stop = 0;
$addLine = true;
for ($i=0; $i <= count($text); $i++) {
	if (
		// check si la ligne commence par une balise
		isset($text[$i]) and preg_match("/^</i",$text[$i]) and
		!preg_match("/Pour les articles homonymes/i",$text[$i]) and 
		!empty($text[$i])
	) {
		$temp = $text[$i];
		// enlever les appels de notes
		$temp = preg_replace("/\[([0-9]+)\]/","",$temp);
		// differents commentaires
		$temp = str_replace("[style à revoir]","",$temp);
		$temp = str_replace("[modifier | modifier le code]","",$temp);
		// convertir les liens 
		$m = '|([\w\d]*)\s?(https?://([\d\w\.-]+\.[\w\.]{2,6})[^\s\]\[\<\>]*/?)|i';
		$r = '$1 <a href="$2">$3</a>';
		$temp = preg_replace($m,$r,$temp); 
		
		if (preg_match("/\<h/i",$temp)) {
			if (// stop la récupération des données si le header correspond à
				preg_match("/Notes et références/i",$temp) or
				preg_match("/Galerie/i",$temp) or
				preg_match("/Article connexe/i",$temp) or
				preg_match("/Articles connexes/i",$temp) or
				preg_match("/Lien externe/i",$temp) or
				preg_match("/Liens externes/i",$temp) or
				preg_match("/Voir aussi/i",$temp)
			) {
				$addLine = false;
			} else {
				$addLine = true;
			}
		}

		if ($addLine == true) {
			$data[] = $temp;
		}
	}
}
$data[] = $h->p($h->ahref("http://fr.wikipedia.org/wiki/".$title,"* Données extraites de Wikipédia"),"style='font-style: italic;'");
$data = implode("\n",$data);

/* add more articles if exists */
$data .= $o_pag->page_data;
/****************************************/
/* show									*/
$body = $h->body($topPage.$data.$bottomPage,"id='page_".$paramId."'");
$page = $h->html($head.$body,"lang='".$pg_params['LANG']."'");

echo $page;
?>
