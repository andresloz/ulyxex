<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class AdminSite extends Admin {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
	}
	public function site_params(){
		$h = $this->h;$t = $this->t;
		if ($_SESSION['userid'] == 1){
			$query = 	SELECT.
							PARAMS.".*,".PAGES.".NAME".
						FROM.
							PARAMS.
							INNERJOIN.PAGES._ON_.PAGES.".ID = ".PARAMS.".PAGEID";
						WHERE.
							PARAMS.".ID = 1"._AND_.PAGES.".ID = ".PARAMS.".PAGEID";
			$query = $this->q($query);
			$dat = $this->fetch($query);
			$_[] = $h->input("hidden","update",1);
			$_[] = $h->p($h->b("-".str_pad("Site",80,"-")));
			$_[] = $h->p($t->wr("select home page").$this->list_pages_select($dat['PAGEID'],"pageId"));
			$_[] = $h->p($t->wr("page name in use for home is").$h->ahref("index.php?k=".$dat['TYPEPAGE']."&v=".$dat['PAGEID'],$dat['NAME'],"target=_blank class='action-big'")." [index.php?k=".$dat['TYPEPAGE']."&v=".$dat['PAGEID']."]");
			$checked = ($dat['HTTPS'])?" checked=\"checked\"":null;
			$https = $t->wr("https").$h->input("checkbox","https","on",$checked)." ";
			$_[] = $h->p($https.$t->wr("type").$h->input("text","type",$dat['TYPEPAGE'],"size='".(strlen($dat['TYPEPAGE']))."' readonly='readonly' style='border:solid black 1px;font-weight:bold;background-color:gainsboro'")." &#38;(".$t->w("param").") ".$h->input("text","param",$dat['PAGEID'],"size='".(strlen($dat['PAGEID']))."' readonly='readonly' style='border:solid black 1px;font-weight:bold;background-color:gainsboro'"));
			$options = array("scroll menu"=>"tree","inline"=>"one_line","inline nav"=>"line_nav","ul list"=>"ul_list","ol list"=>"ol_list");
			$_[] = $h->p($t->wr("navigation").$h->select($options,"navtype",$dat['NAVTYPE']));
			$options = array();
			$css_files = $this->css_files();
			foreach($css_files as $css) {
				$options[$css] = $css;
			}
			$_[] = $h->p($t->wr("main css").$h->select($options,"css",$dat['CSS']));
			$_[] = $h->p($t->wr("articles by page").$h->input("text","articlespage",$dat['ARTICLESPAGE'],"size='4'"));
			$_[] = $h->p($t->wr("tabulation").$h->input("text","tab",$dat['SITETAB'],"size='6'"));
			$_[] = $h->p($t->wr("lang")." ".$h->b($t->getLang()));
			$_[] = $h->p("favicon.png : ".$h->img("favicon.png","align='absmiddle'"));
			$_[] = $h->p($h->b("-".str_pad("Admin",80,"-")));
			$_[] = $h->p($t->wr("items by page admin").$h->input("text","items",$dat['ITEMS'],"size='4'"));
			$size = isset($dat['MAIL'])?"size='".strlen($dat['MAIL'])."'":"";
			$_[] = $h->p($t->wr("website administrator mail").$h->input("text","mail",$dat['MAIL'],$size));
			$_[] = $h->p($h->input("submit","save",$t->w("update all")));
			$_[] = $h->p("&nbsp;");
			$inputs = implode("",$_);
			return $h->form($inputs,"home","post","admin.php?action=admin_home");
		}
	}
	function css_files(){
		$list = array();
		$handle = opendir("css/");
		while ($file = readdir($handle)){
			$file_parts = pathinfo($file);
			if ($file_parts['extension'] == "css" && $file != "admin_style.css")  {
				$list[] = "css/".$file;
			}
		} // end while
		closedir($handle);
		return $list;
	}
	public function update_site_params(){
		$vals = explode("&",$_POST['pageId']);
		$typePage = explode("=",$vals[0]);
		$typePage = $typePage[1];
		$paramId = explode("=",$vals[1]);
		$paramId = $paramId[1];
		if (isset($_POST['https'])) {
			$https = 1;
		} else {
			$https = 0;
		}
		$query = UPDATE.PARAMS.
					SET."PAGEID = '".$this->h->word($paramId).
					"',TYPEPAGE = '".$this->h->word($typePage).
					"',NAVTYPE = '".$this->h->word($_POST['navtype']).
					"',CSS = '".$this->h->word($_POST['css']).
					"',SITETAB = '".$this->h->safe($_POST['tab']).
					"',HTTPS = '".$https.
					"',ARTICLESPAGE = '".$this->h->num($_POST['articlespage']).
					"',ITEMS = '".$this->h->num($_POST['items']).
					"',MAIL = '".$this->h->safe($_POST['mail'])."'".
				WHERE."ID = 1";
		$query = $this->q($query);
		
		if (file_exists("js/tinymce.js")) {
			$data = array();
			$Lines = file("js/tinymce.js");
			foreach($Lines as $Line) {
				// change line if needed
				if ( preg_match("/content_css/",$Line) ){
					$Line = "   content_css:\"".$this->h->word($_POST['css'])."\",\n";
				}
				$data[] = $Line;
			}
			$File = fopen("js/tinymce.js","w") or die("can't open file js/tinymce.js");
			foreach($data as $Line) {
				fwrite($File, $Line);
			}
			fclose($File);
		}
		return $this->h->h3($this->t->w("some values have been modified !"),"class='warning'");
	}
	public function list_pages_select($pageId,$name){
		$query = SELECT.PAGES.".ID,".PAGES.".NAME,".PAGES.".TYPEPAGE".FROM.PAGES;
		$query = $this->q($query);
		$options = array();
		$optionSelectect = null;
		while ( $line = $this->fetch($query) ){
			if ($line['ID'] == $pageId){
				$optionSelectect = "k=".$line['TYPEPAGE']."&amp;v=".$line['ID'];
				$options[$line['NAME']] = "k=".$line['TYPEPAGE']."&amp;v=".$line['ID'];
			} else {
				$options[$line['NAME']] = "k=".$line['TYPEPAGE']."&amp;v=".$line['ID'];
			}
		}
		return $this->h->select($options,$name,$optionSelectect,"onchange='fillHomeFields(this.value)'");
	}
}
?>
