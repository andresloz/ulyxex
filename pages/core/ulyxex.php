<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!defined('VERSION')) {
	define("VERSION","1.5.4.4.b");
}
if (!defined('FILES') && isset($prefixTableMySql)) {
	define("FILES",$prefixTableMySql."files"); // compatibility with old configs which don't have this constant 
}
/* SQL WORDS CONSTANTS */
define("SELECT"," SELECT ");
define("FROM"," FROM ");
define("WHERE"," WHERE ");
define("_OR_"," OR ");
define("UPDATE"," UPDATE ");
define("INNERJOIN"," INNER JOIN ");
define("DELETE"," DELETE ");
define("ORDERBY"," ORDER BY ");
define("_ON_"," ON ");
define("SET"," SET ");
define("INSERTINTO"," INSERT INTO ");
define("GROUPBY"," GROUP BY ");
define("ALLFIELDS"," * ");
define("_AND_"," AND ");
define("_IN_"," IN ");

class Ulyxex {
	public function q($query=Null,$debug=0){
		if (isset($_SESSION['countqueries'])) $_SESSION['countqueries']++;
		// add ";" if not
		$query = trim($query);
		if (substr($query,-1) != ";") $query .= ";";
		$dbLink = @mysqli_connect(HOSTMYSQL,USERMYSQL,PASSMYSQL,BASEMYSQL) or die("Error database connection aborted"); 
		if ($debug) {// debug
			$result = $dbLink->query($query);
			echo $query; 
		} else {
			$result = $dbLink->query($query);
		}
		mysqli_close($dbLink);
		return $result;
	}
	public function qTest($query=Null){
		if (isset($_SESSION['countqueries'])) $_SESSION['countqueries']++;
		// add ";" if not
		$query = trim($query);
		if (substr($query,-1) != ";") $query .= ";";
		$dbLink = @mysqli_connect(HOSTMYSQL,USERMYSQL,PASSMYSQL,BASEMYSQL) or die("Error database connection aborted"); 
		$result = $dbLink->query($query);
		if ($result) {
			$result = "<span style='color:green;' >OK >>>> ".$query."</span>";
		} else {
			$result = "<span style='color:red;' >NOT >>> ".$query."</span>";
		}
		mysqli_close($dbLink);
		return $result;
	}
	public function fetch($query=Null){
		return @mysqli_fetch_assoc($query);
	}
	public function Where($And=array()){
		$data = "";
		if (count($And) > 0) $data = WHERE.implode(_AND_,$And);
		return $data;
	}
	public function WhereOr($Or=array()){
		$data = "";
		if (count($Or) > 0) $data = WHERE.implode(_OR_,$Or);
		return $data;
	}
	public function site_params(){
		$query = SELECT.ALLFIELDS.FROM.PARAMS.WHERE."ID=1;";
		$query = $this->q($query);
		if ($result = $this->fetch($query)){
			return $result;
		} else {
			exit("error site_params !");
		}
	}
	public function art_params($id){
		$query = SELECT."ID,USERID,HIDE,PAGEID".FROM.ARTICLES.WHERE."ID=".$id;
		$query = $this->q($query);
		if ($result = $this->fetch($query)){
			return $result;
		} else {
			exit("error art_params !");
		}
	}
	public function page_params($id){
		$query = SELECT.ALLFIELDS.FROM.PAGES.WHERE."ID=".$id;
		$query = $this->q($query);
		if ($result = $this->fetch($query)){
			return $result;
		} else {
			exit("error page_params !");
		}
	}
	public function user_params($id){
		$query = SELECT.ALLFIELDS.FROM.USERS.WHERE."ID=".$id;
		$query = $this->q($query);
		if ($result = $this->fetch($query)){
			return $result;
		} else {
			exit("error user_params !");
		}
	}
	public function level($level=0){
		if ($_SESSION['level'] > $level) {
			exit("bad user level !");
		} else {
			return;
		}
	}
}
?>
