<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class Tree extends Ulyxex {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
		$this->parentOf = array();
		$this->name = array();
	}
	public function paths($hide=Null,$userId=Null){
		$paths = array();
		if ($hide){
			$query = SELECT."ID,NAME,TYPEPAGE,PARENTID".FROM.PAGES.WHERE."HIDE != 1";
			if ($userId) $query .= _AND_."USERID = ".$userId;
		} else {
			$query = SELECT."ID,NAME,TYPEPAGE,PARENTID".FROM.PAGES;
			if ($userId) $query .= WHERE."USERID = ".$userId;
		}
		
		$query = $this->q($query);
		
		while ( $line = $this->fetch($query) ){
			$this->parentOf[$line['ID']] = $line['PARENTID'];
			$this->name[$line['ID']] = $line['NAME'];
			$this->type[$line['ID']] = $line['TYPEPAGE'];
		}

		foreach($this->parentOf as $child => $parent){
			$ancestorId = 0;
			$ancestorsNames = array();
			if ($parent == 0){
				// if parent is home
				$lineage = "/".$this->name[$child]."/";
				$paths[$lineage] = array($this->type[$child],$child); 
			} else {
				// if parent other than home exist
				// add parent
				if (isset($this->name[$parent])){
					// check if error in lineage
					if ( in_array($this->name[$parent],$ancestorsNames) ){echo " error tree !";break;}
					$ancestorsNames[] = $this->name[$parent];
				}
				// if a gran parent exist
				if (array_key_exists($parent, $this->parentOf)){ 
					$ancestorId = $this->parentOf[$parent];
					// if others gran gran... parent exist
					while ($ancestorId != 0){
						if (isset($this->name[$ancestorId])){
							// check if error in lineage
							if ( in_array($this->name[$ancestorId],$ancestorsNames) ){echo " error tree !";break;}
							$ancestorsNames[] = $this->name[$ancestorId];
						}
						
						if (isset($this->parentOf[$ancestorId]) && $this->parentOf[$ancestorId] != 0){ 
							$ancestorId = $this->parentOf[$ancestorId];
						} else {
							$ancestorId = 0;
						}
					}
					$ancestorsNames = array_reverse($ancestorsNames);
					$lineage = "/".implode("/",$ancestorsNames)."/".$this->name[$child]."/";
					$paths[$lineage] = array($this->type[$child],$child); 
				}
				
			}
		}
		return $paths;
	}
	public function pathsSelect($selected=Null,$root=1,$type=Null,$userId=Null){
		$paths = $this->paths(Null,$userId);
		ksort($paths);
		$options = array();
		if ($root) $options["ROOT/"] = 0;
		foreach($paths as $path => $idVal){
			$pathDepth = explode("/",$path);
			$path = "ROOT-";
			$path .= str_repeat("/-",count($pathDepth)-3); // 3 for first "/", last "/" and array position
			array_pop($pathDepth);
			$path .= ">".array_pop($pathDepth);
			if ($type) {
				if ($idVal[0] == $type) {
					$options[$path] = $idVal[1];
				} elseif ($idVal[0] == "link") {
					$options[$path." [link]"] = $idVal[1];
				} else {
					$options[$path." [other]"] = $idVal[1];
				}
			} else {
				$options[$path] = $idVal[1];
			}
		}
		return $this->h->select($options,"parent",$selected);
	}
	public function pathTest($page=0,$parent=0){
		$paths = $this->paths();
		$lineage = explode("/",array_search($parent,$paths));
		return (in_array($this->name[$page],$lineage))?0:1;
	}
	public function pathPage($parent=0){
		$paths = $this->paths();
		return array_search($parent,$paths);
	}
	function getMenuLine(){
		$h = $this->h;$t = $this->t;
		$menu = array();
		$query = SELECT."ID,NAME,TYPEPAGE".FROM.PAGES.WHERE."HIDE != 1";
		$query = $this->q($query);
		while ( $line = $this->fetch($query) ){
			$menu[$line['NAME']] = $h->ahref("index.php?k=".$line['TYPEPAGE']."&v=".$line['ID'],$line['NAME'],"class='menu'");
		}
		ksort($menu);
		return implode(" ",array_values($menu));
	}
	function getMenuPosition($pageId=0){
		$h = $this->h; $t = $this->t;
		$menu = array();
		// get child pages
		$query = SELECT."ID,NAME,TYPEPAGE,PARENTID".FROM.PAGES.WHERE."PARENTID = ".$pageId._AND_."HIDE != 1";
		$query = $this->q($query);
		while ( $line = $this->fetch($query) ){
			array_push($menu,$h->ahref("index.php?k=".$line['TYPEPAGE']."&v=".$line['ID'],$line['NAME'],"class='menu'"));
		}
		sort($menu);
		if (count($menu) > 0) { // if child add sign
			array_unshift($menu,"->");
		}
		
		// get current page
		$query = SELECT."ID,NAME,TYPEPAGE,PARENTID".FROM.PAGES.WHERE."ID = ".$pageId._AND_."HIDE != 1";
		$query = $this->q($query);
		$line = $this->fetch($query);
		array_unshift($menu,$h->ahref("index.php?k=".$line['TYPEPAGE']."&v=".$line['ID'],$line['NAME'],"class='menu'"));
		
		// get root childs
		if ($line['PARENTID'] != 0) {
			$query = SELECT."ID,NAME,TYPEPAGE".FROM.PAGES.WHERE."ID = ".$line['PARENTID']._AND_."HIDE != 1";
			$query = $this->q($query);
			$line = $this->fetch($query);
			array_unshift($menu,$h->ahref("index.php?k=".$line['TYPEPAGE']."&v=".$line['ID'],$line['NAME'],"class='menu'")." <-");
		}
		
		//~ ajouter les autres pages racine
		$query = SELECT."ID,NAME,TYPEPAGE".FROM.PAGES.WHERE."PARENTID = 0"._AND_."HIDE != 1"._AND_."ID != ".$pageId;
		$query = $this->q($query);
		array_push($menu,"||");
		while ( $line = $this->fetch($query) ){
			array_push($menu,$h->ahref("index.php?k=".$line['TYPEPAGE']."&v=".$line['ID'],$line['NAME'],"class='menu'"));
		}
		return implode(" ",$menu);
	}
	function getMenuForm($pageId=0,$pageType=""){
		$h = $this->h;$t = $this->t;
		$paths = $this->paths($hide=1); // prevent showing hide links
		ksort($paths);
		$pages_paths = array();
		$selectedPath = $pageType.",".$pageId;
		$navigationTitle = "--".$t->w("Pages").str_repeat("-",20-strlen($t->w("Pages")));
		// pages
		$pages_paths[$navigationTitle] = "limit";
		foreach($paths as $path => $idVal){
			$pathDepth = explode("/",$path); 
			// don't use blank space or - as first char (bug for netspace 2.02 )
			$path = str_repeat(SITETAB,count($pathDepth)-3); // 3 for first "/", last "/" and array position
			if (count($pathDepth)-3 > 0) $path .= " ";
			array_pop($pathDepth);
			$path .= array_pop($pathDepth);
			$pages_paths[$path.$h->comment($idVal[1])] = $idVal[0].",".(string)$idVal[1];
		}
		$options = $pages_paths;
		/* rond ico &#9673 oplus &oplus;, loz ico &loz; phi ico &Phi*/
		$menu = $h->p($h->selectOnChangeMenu($options,"navSelect",$selectedPath)." ".$h->input("submit","OK","&Phi;","class=\"navsubmit\""));
		return $h->form($menu,"navform","post","index.php");
	}
	function getMenuList($type="ul"){
		$h = $this->h;$t = $this->t;
		$paths = $this->paths($hide=1); // prevent showing hide links
		ksort($paths);
		$depth = 2;
		$listMenu = "";
		foreach($paths as $path => $idVal){
			$pathParts = explode("/",$path);
			$name = $pathParts[count($pathParts)-2];
			$pathDepth = count($pathParts);
			// $pathDepth.":"
			if ($pathDepth > $depth) {
				for ($i=0; $i<$pathDepth-$depth; $i++) {
					$listMenu .= "<".$type.">";
				}
				$depth = $pathDepth;
				$listMenu .= $h->li($h->ahref("index.php?k=".$idVal[0]."&v=".$idVal[1],$name));
			} elseif ($pathDepth < $depth) {
				for ($i=0; $i<$depth-$pathDepth; $i++) {
					$listMenu .= "</".$type.">";
				}
				$depth = $pathDepth;
				$listMenu .= $h->li($h->ahref("index.php?k=".$idVal[0]."&v=".$idVal[1],$name));
			} else {
				$listMenu .= $h->li($h->ahref("index.php?k=".$idVal[0]."&v=".$idVal[1],$name));
			}
		}
		return $h->div($listMenu,"id='menulist'");
	}	
	public function pagesNavigation($pageId=0,$pageType="page"){
		$home = $this->site_params();
		$menuStyle = $home['NAVTYPE'];
		if ($menuStyle == "one_line") {
			return $this->getMenuLine();
		} elseif($menuStyle == "line_nav") {
			return $this->getMenuPosition($pageId=$pageId);
		} elseif($menuStyle == "ul_list") {
			return $this->getMenuList();
		} elseif($menuStyle == "ol_list") {
			return $this->getMenuList($type="ol");
		} else { // default tree;
			return $this->getMenuForm($pageId=$pageId,$pageType=$pageType);
		}
	}
}  
?>
