<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class UpdateLang extends Admin {
	function __construct($pageId=Null){
		$this->h = New Htmlz();
		$this->t = New Translate();
        $this->tree = New Tree();
	}
	public function update_lang(){
		$h = $this->h;$t = $this->t;
		$query = UPDATE.ARTICLES.SET."LANG = '".$h->safe($_POST['lang'])."'";
		$query = $this->q($query);
		$query = UPDATE.PAGES.SET."LANG = '".$h->safe($_POST['lang'])."'";
		$query = $this->q($query);
		return $h->h3($t->wx("lang").$t->wl("updated !"),"class='warning'");
	}
	public function data_lang(){
		$h = $this->h;$t = $this->t;
		if ($_SESSION['level'] == 1) {
			$_[] = $h->input("hidden","update",1);
			$_[] = $h->h2($t->w("Update all articles and pages to lang"));
			$_[] = $h->p( $t->wr("lang").$h->input("text","lang",$t->getLang(),"size=3"));
			$_[] = $h->input("submit","save",$t->w("update"));
			$inputs = implode("",$_);
			return $h->form($inputs,"lang","post","admin.php?action=update_lang");
		} else {
			return $h->p("error");
		}
	}
}
?>
