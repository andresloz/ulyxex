<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
/* 
don't modify this script, here there are the basics controllers for ulyxex
if you want to add your own controller put them in "config.php" file
*/

/* 
structure of controller
element [0] = title of action
element [1] = php script to work with or any url
element [2] = "action" to add it as an action in menu or "link" as a link in menu or "null" to hide it in menu
element [3] = level of user [1,2,3,4] to define who can use the controller
*/
$adminController["articles"] 			= array("articles","pages/admin_articles.php","action",4);
$adminController["add_article"] 		= array("add article","pages/admin_article_add.php",null,4);
$adminController["modify_article"] 		= array("modify article","pages/admin_article_modify.php",null,4);
$adminController["list_files"] 			= array("list files","pages/admin_files_dir.php",null,4);

$adminController["pages"]				= array("pages","pages/admin_pages.php","action",2);
$adminController["add_page"] 			= array("add page","pages/admin_page_add.php",null,2);
$adminController["modify_page"] 		= array("modify page","pages/admin_page_modify.php",null,2);

$adminController["files"]				= array("files","pages/admin_files.php","action",3);
$adminController["ftp_files"]			= array("ftp","pages/admin_ftp_files.php","action",2);

$adminController["users"] 				= array("users","pages/admin_users.php","action",1);
$adminController["add_user"] 			= array("add user","pages/admin_user_add.php",null,1);
$adminController["modify_user"] 		= array("modify user","pages/admin_user_modify.php",null,1);
$adminController["my_user_profile"] 	= array("my-profile","admin.php?action=modify_user&amp;user=".$_SESSION['userid'],"link",4);

$adminController["admin_home"] 			= array("site","pages/admin_site.php","action",1);
$adminController["edit_css"]			= array("edit css","pages/admin_edit_css.php","action",1);

$adminController["sitemap"]				= array("create sitemap","pages/admin_sitemap.php",null,1);
$adminController["controllers"]			= array("show controllers","pages/admin_show_controllers.php",null,1);
$adminController["update_lang"]			= array("update lang","pages/admin_update_lang.php",null,1);
$adminController["user_data_transfert"]	= array("data user transfert","pages/admin_user_data_transfert.php",null,1);

$adminController["login"]				= array("login","login.php","link",4);
$adminController["logout"]				= array("logout","logout.php","link",4);
$adminController["help"]				= array("help","http://ulyxex.logz.org/index.php?k=page&amp;v=3","link",4);

$adminController["phpinfo"]				= array("phpinfo","pages/admin_phpinfo.php",null,1);
?>
