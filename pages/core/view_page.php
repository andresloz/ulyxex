<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ViewPage extends Ulyxex {
	function __construct($pageId=0,$sortStyle=0){
		$this->h = New Htmlz();
		$this->pos = isset($_GET['pg'])?$this->h->num($_GET['pg']):0;
		$this->Select = 	SELECT.
								ARTICLES.".ID ARTID,".ARTICLES.".CONTENT,".ARTICLES.".DATEPAGE,".
								USERS.".USERNAME,".USERS.".ID IDUSERID";
		$this->From = 		FROM.
								ARTICLES.
								INNERJOIN.PAGES._ON_.ARTICLES.".PAGEID = ".PAGES.".ID".
								INNERJOIN.USERS._ON_.ARTICLES.".USERID = ".USERS.".ID";
		$this->SelectCount = 	SELECT."COUNT(".ARTICLES.".ID) AS NUM";
		$this->OrderSort = "";
		$this->Cond = array();
		if ($pageId){
			$this->page_data = $this->page_data($pageId,$sortStyle);
			$this->next_pages = $this->next_pages($pageId);
		}
		
	}
	public function page_data($pageId=0,$sortStyle=0){
		$h = $this->h;
		$sortOrder = array("DATEPAGE DESC","SUBJECT DESC","DATEPAGE ASC");
		$pgPos = ($this->pos)?($this->pos - 1) * ARTICLESPAGE:0;
		$this->Cond = array();
		$this->Cond[] = ARTICLES.".HIDE = 0";
		$this->Cond[] = PAGES.".HIDE = 0";
		$this->Cond[] = PAGES.".ID = ".$pageId;
		$this->OrderSort = ORDERBY.$sortOrder[$sortStyle]." LIMIT ".$pgPos.",".ARTICLESPAGE;
		$query = $this->Select.$this->From.$this->Where($this->Cond).$this->OrderSort;
		$query = $this->q($query);
		if ($line = $this->fetch($query)){
			$article = $line['CONTENT'];
			$article .= $h->footer($h->nav($h->ahref("index.php?k=user&amp;v=".$line['IDUSERID'],$line['USERNAME'])." ".$line['DATEPAGE']." ".$h->ahref("index.php?k=article&amp;v=".$line['ARTID']."&amp;t=".$h->clean_short_url($line['CONTENT'],40),"url"),"class='contenturl'"));
			$result = $h->article($article,"id='article_".$line['ARTID']."'");
			while ( $line = $this->fetch($query) ){
				$article = $line['CONTENT'];
				$article .= $h->footer($h->nav($h->ahref("index.php?k=user&amp;v=".$line['IDUSERID'],$line['USERNAME'])." ".$line['DATEPAGE']." ".$h->ahref("index.php?k=article&amp;v=".$line['ARTID']."&amp;t=".$h->clean_short_url($line['CONTENT'],40),"url"),"class='contenturl'"));
				$result .= $h->article($article,"id='article_".$line['ARTID']."'");
			}
			return $result;
		} else {
			return "";
		}
	}
	public function next_pages($pageId=0){
		$this->Cond = array();
		$this->Cond[] = ARTICLES.".HIDE = 0";
		$this->Cond[] = PAGES.".HIDE = 0";
		$this->Cond[] = PAGES.".ID = ".$pageId;
		$query = $this->SelectCount.$this->From.$this->Where($this->Cond);
		$query = $this->q($query);
		if ($line = $this->fetch($query)){
			$this->items = $line['NUM'];
			/* total pages */
			$pages = $this->items/ARTICLESPAGE; 
			$pages++;
			$result = array();
			if ($pages > 2){
				for($i=1;$i < $pages;$i++){
					$num = $i?strval($i):"0";
					if ($i == $this->pos || ($this->pos == 0 && $i == 1)){
						$result[] = "[".$this->h->ahref("index.php?k=page&amp;v=".$pageId."&amp;pg=".$num,$num)."]";
					} else {
						$result[] = $this->h->ahref("index.php?k=page&amp;v=".$pageId."&amp;pg=".$num,$num);
					}
				}
			}
			return implode(" . ",$result);
		}
	}
}
?>
