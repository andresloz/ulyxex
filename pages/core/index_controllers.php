<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
/* 
don't modify this script, here there are the basics controllers for ulyxex
if you want to add your own controller put them in "config.php" file
*/
$viewController["article"]	= "pages/view_article.php";
$viewController["page"] 	= "pages/view_page.php";
$viewController["link"] 	= "pages/view_link.php"; // scrip to manage links
$viewController["user"] 	= "pages/view_user.php";
$viewController["contact"] 	= "pages/view_contact_form.php";
/****************************************/
?>
