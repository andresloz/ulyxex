 <?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class Users extends Admin {
	public function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
		if (isset($_GET['pg'])) {
			$this->pagePos = $this->h->num($_GET['pg']);
			$_SESSION['pgu'] = $_GET['pg'];
		} elseif (isset($_SESSION['pgu']) && !empty($_SESSION['pgu']))  {
			$this->pagePos = $this->h->num($_SESSION['pgu']);
			$_GET['pg'] = $_SESSION['pgu'];
		} else {
			$this->pagePos = 0;
		}
		$this->Select =	SELECT."ID,LOGIN,USERLEVEL,USERNAME,USEROWNER ";
		$this->From = 	FROM.USERS;
		$this->SelectCount = SELECT."COUNT(ID) AS NUM ";
		$this->OrderSort = "";
		$this->Cond = array();
		$this->userPagesTotal = $this->sum_pages_by_users();
		$this->userArtsTotal = $this->sum_arts_by_users();
	}
	public function update_users(){
		$formValues = array();
		$values = $_POST;
		foreach($values as $k=>$v){
			if ( preg_match("/\_/",$k) ){
				list($key,$id) = explode("_",$k);
				$formValues[$id][$key] = $v;
			}
		}
		foreach($formValues as $id=>$value){
			$query = 0;
			if (isset($value['delete']) && $id != 1){ // nevers delete admin
				$query = DELETE.FROM.USERS.WHERE."ID = ".$id;
				$query = $this->q($query);
				// all user's articles, pages and link go to hide, sandbox and admin
				$query = UPDATE.ARTICLES.SET."HIDE = 1, PAGEID = 2, USERID = 1".WHERE."USERID = ".$id;
				$query = $this->q($query);
				$query = UPDATE.PAGES.SET."HIDE = 1, PARENTID = 2, USERID = 1".WHERE."USERID = ".$id;
				$query = $this->q($query);
				$query = UPDATE.FILES.SET."USERID = 1".WHERE."USERID = ".$id;
				$query = $this->q($query);
				$query = UPDATE.USERS.SET."USEROWNER = 1".WHERE."USEROWNER = ".$id;
				$query = $this->q($query);
			}
		}
		return $this->h->h3($this->t->w("some values have been modified !"),"class='warning'");
	}
	public function list_users(){
		$h = $this->h;$t = $this->t;
		$ownerName = array();
		$query = $this->q(SELECT."ID,USERNAME".FROM.USERS);
		while ( $line = $this->fetch($query) ){
			$ownerName[$line['ID']] = $line['USERNAME'];
		}
		$pgPos =($this->pagePos)?($this->pagePos - 1) * ITEMS:0;
		if (isset($_GET['ownerFilter'])) $_SESSION['admin_users_filter'] = USERS.".USEROWNER = ".$h->num($_GET['ownerFilter']);
		if (isset($_POST['search_name'])) $_SESSION['admin_users_search_name'] = " MATCH(".USERS.".USERNAME) AGAINST('".$h->safe($_POST['search_name'])."') ";
		if (isset($_POST['reset'])){
			$_SESSION['admin_users_filter'] = "";
			$_SESSION['admin_users_search_name'] = "";
		}
		if ( isset($_SESSION['admin_users_search_name']) && !empty($_SESSION['admin_users_search_name']) ) $pgPos = 1;
		$this->OrderSort = ORDERBY.USERS.".DATEPAGE DESC LIMIT ".$pgPos.",".ITEMS;
		// 1) can create users etc.
		// 2) can create articles and pages
		// 3) can create articles everywhere
		// 4) can create articles only in sandbox
		// always page position = 1 if use search, only ITEMS first results
		// user level 1 only edit his users
		$this->Cond = array();
		if ($_SESSION['userid'] != 1) $this->Cond[] = USERS.".USEROWNER = ".$_SESSION['userid'];
		if ( isset($_SESSION['admin_users_filter']) && !empty($_SESSION['admin_users_filter']) ) $this->Cond[] = $_SESSION['admin_users_filter'];
		if ( isset($_SESSION['admin_users_search_name']) && !empty($_SESSION['admin_users_search_name']) ) $this->Cond[] = $_SESSION['admin_users_search_name'];

		$query = $this->Select.$this->From.$this->Where($this->Cond).$this->OrderSort;
		$query = $this->q($query);
		
		if (isset($_POST['search_name'])) {
			$searchValue = $_POST['search_name'];
		} else {
			$searchValue = "";
		}
		$searchForm = $this->search_form("search_name","search by name","users",$searchValue);
		$goIdForm = $this->go_id("user","modify_user");
		$resetForm = $this->reset_form("users");
		
		$usersSubmit = $h->p($h->input("submit","",$t->w("update all")));
		$usersListTitle = $h->h3($t->w("users list"));
		$usersList = $h->input("hidden","update",1);
		
		while ( $line = $this->fetch($query) ){
			$id = "id".$h->f4d($line['ID']);
			$pages = (isset($this->userPagesTotal[$line['ID']]))?$h->ahref("admin.php?action=pages&ownerFilter=".$line['ID'],$h->f4d($this->userPagesTotal[$line['ID']])):$h->f4d(0);
			$pages .= " pags";
			$arts = (isset($this->userArtsTotal[$line['ID']]))?$h->ahref("admin.php?action=articles&ownerFilter=".$line['ID'],$h->f4d($this->userArtsTotal[$line['ID']])):$h->f4d(0);
			$arts .= " arts";
			$modify = $h->ahref("admin.php?action=modify_user&amp;user=".$line['ID'],$t->w("modify"),"class='formframe'");
			$login = $t->wx("login").$h->input("text","userlogin",$h->strWidth($line['LOGIN'],12),"readonly='readonly' size='12'");
			$level = "^".$line['USERLEVEL'];	
			
			
			$view = $h->ahref("index.php?k=user&amp;v=".$line['ID'],$h->strWidth(strip_tags($line['USERNAME']),20),"class='formframe' target=_blank");
			
			
			$owner = ($_SESSION['userid'] == 1)?$t->wx("by").
						$h->ahref("admin.php?action=users&amp;ownerFilter=".$line['USEROWNER'],$h->strWidth($ownerName[$line['USEROWNER']],12),
						"class='formframe' title='".$h->uly_htmlentities($ownerName[$line['USEROWNER']])."'"):Null;
			$delete = ($line['ID'] != 1)?$t->wr("delete").$h->input("checkbox","delete_".$line['ID'],"on"):str_repeat("*",strlen($t->w("delete")."**"));
			
			$_ = array($this->bordered($id),$this->bordered($pages),$this->bordered($arts),$modify,$this->bordered($delete),$view,$this->bordered($level),$owner);
			
			$content = implode(" ",$_);
			$usersList .= $h->p($content);
		}
		$nextPages = $this->next_pages();
		$usersList = $usersListTitle.$usersSubmit.$usersList.$usersSubmit.$nextPages;
		return $searchForm.$goIdForm.$resetForm.$h->form($usersList,"users","post","admin.php?action=users&amp;pg=".$this->pagePos);
	}
	public function next_pages(){
		$query = $this->SelectCount.$this->From.$this->Where($this->Cond);
		$query = $this->q($query);
		$line = $this->fetch($query);
		$pagesNumbers = new NextPages($line['NUM'],"action=users");
		return $this->h->p($pagesNumbers->numbers());
	}
}
?>
