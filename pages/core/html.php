<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class Html extends Htmlz {
	//~ to keep retro-compatibility with special scripts
	//~ because name "Html" as a class produce a warning in php7 ?
}
class XmlSitemap {
	public function urlset($val=null){
		$result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?".">\n";
		$result .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
		$result .= $val."\n</urlset>\n";
		return $result;
	}
	function tag($tag=null,$val=null){;
		if ($val) return "<".$tag.">".$val."</".$tag.">";
	}
	/* xml sitemap tags */
	public function url($val=null){
		return $this->tag("url",$val);
	}
	public function loc($val=null){
		return $this->tag("loc",$val);
	}
	public function lastmod($val=null){
		return $this->tag("lastmod",$val);
	}
	public function changefreq($val=null){
		return $this->tag("changefreq",$val);
	}
	public function priority($val=null){
		return $this->tag("priority",$val);
	}
}
class XmlRss {
	public function rss($val=null,$params=null){
		$result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?".">\n";
		$result .= "<rss version=\"2.0\">\n<channel>\n";
		$result .= $val."\n</channel>\n</rss>\n";
		return $result;
	}
	function tag($tag=null,$val=null){
		$params = $this->_sp($params);
		if ($val) return "<".$tag.">".$val."</".$tag.">";
	}
	/* rss tags */
	public function item($val=null){
		return $this->tag("title",$val);
	}
	public function title($val=null){
		return $this->tag("title",$val);
	}
	public function link($val=null){
		return $this->tag("link",$val);
	}
	public function author($val=null){
		return $this->tag("lastmod",$val);
	}
	public function description($val=null){
		return $this->tag("description",$val);
	}
	public function pubdate($val=null){
		return $this->tag("pubdate",$val);
	}
}
#[\AllowDynamicProperties]
class Htmlz {
	function __construct(){
		$this->extUploadAllowed = array("7z","aac","ai","aif","aifc","aiff","arj","asf","asr","asx","au","avi","bin","bmp","cda","dcr","doc","dot","eps","flv","gif","gz","hqx","html","ico","jfif","jpe","jpeg","jpg","m4a","m4b","m4v","mid","mov","mp2","mp3","mp4","mpa","mpe","mpeg","mpg","odb","odc","odf","odg","odi","odm","odp","ods","odt","ogg","pdf","png","pps","ppt","ps","psd","ra","ram","rar","rm","rmi","rtf","sea","sit","sla","stc","std","sti","stw","swf","sxc","sxd","sxg","sxi","sxm","sxw","tar","tgz","tif","tiff","torrent","txt","vcf","wav","wcm","wdb","webm","webp","wks","wma","wmf","wmv","wps","wri","wrl","wrz","xaf","xbm","xla","xlc","xlm","xls","xlt","xlw","xml","xof","xpm","xsd","xsl","xwd","xspf","zip","pict");
	}
	/* ulyxex specific functions */
	public function ulyxCredits($firstLine=True,$secondLine=True){
		$t = new Translate();
		$version =  defined('VERSION')?VERSION:"0.0.0";
		$firstLine = ($firstLine)?$this->p(
			$this->span($this->ahref($this->root_url("index.php?k=contact&amp;v=0"),$t->w("Contact website")),"class='contactsite'")
			." ".
			$this->span($this->ahref($this->root_url("login.php"),$t->w("Login")),"class='linklogin'")
			,"class='contact'"):"";
		$secondLine = ($secondLine)?$this->p( $this->ahref("http://ulyxex.logz.org",$t->wr("Made with Ulyxex").$version),"class='credits'"):"";
		return $firstLine.$secondLine;
	}
	public function countQ(){
		return $this->p("queries = ".$_SESSION['countqueries']);
    }
    /* special change menu */
	public function selectOnChangeMenu($options=null,$name=null,$selected=null){
		if ($name){$name = " id=\"".$name."\" name=\"".$name."\"";}
		return "\n<select".$name." onchange='this.form.submit()' onmouseover='this.click()'>
>".$this->optionsChangeMenu($options,$selected)."\n</select>";
	}
	function optionsChangeMenu($options=null,$selected=null){
		$optionsList = array();
		foreach($options as $item=>$value){
			if ($value == $selected){
				//~ menu icon &#x2630
				$optionsList[] =  "<option value=\"".$value."\" selected=\"selected\">"."&#x2630; ".$item."</option>"; 
			} else {
				$optionsList[] =  "<option value=\"".$value."\">".$item."</option>"; 
			}
		}
		return implode("\n",$optionsList);
	}
	/* html */
	public function html($val=null,$params=null){
		// ulyxex default use html version 5
		return $this->html5($val,$params);
		// return $this->html4($val,$params);
		// return $this->xhtml1($val,$params);
	}
	public function html5($val=null,$params=null){
		$params = $this->_sp($params);
		$result = "<!DOCTYPE HTML>\n";
		$result .= "<html".$params.">";
		$result .= $val."</html>";
		return $result;
	}
	public function html4($val=null,$params=null){
		$params = $this->_sp($params);
		$result = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
		$result .= "<html".$params.">";
		$result .= $val."</html>";
		return $result;
	}
	public function xhtml1($val=null,$params=null){
		$params = $this->_sp($params);
		$result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?".">\n";
		$result .= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/html51/DTD/html51-transitional.dtd\">\n";
		$result .= "<html xmlns=\"http://www.w3.org/1999/xhtml\"".$params.">";
		$result .= $val."</html>\n";
		return $result;
	}

	/* regular tags */
	function tag($tag=null,$val=null,$params=null){
		$params = $this->_sp($params);
		if ($val) return "<".$tag.$params.">".$val."</".$tag.">\n";
	}
	public function head($val=null){
		return $this->tag("head",$val);
	}
	public function body($val=null,$params=null){
		$params = $this->_sp($params);
		return $this->tag("body",$val,$params);
	}
	public function title($val=null){
		return $this->tag("title",$val);
	}
	public function header($val=null,$params=null){
		return $this->tag("header",$val,$params);
	}
	public function article($val=null,$params=null){
		return $this->tag("article",$val,$params);
	}
	public function aside($val=null,$params=null){
		return $this->tag("aside",$val,$params);
	}
	public function mark($val=null,$params=null){
		return $this->tag("mark",$val,$params);
	}
	public function nav($val=null,$params=null){
		return $this->tag("nav",$val,$params);
	}
	public function footer($val=null,$params=null){
		return $this->tag("footer",$val,$params);
	}
	public function h1($val=null,$params=null){
		return $this->tag("h1",$val,$params);
	}
	public function h2($val=null,$params=null){
		return $this->tag("h2",$val,$params);
	}
	public function h3($val=null,$params=null){
		return $this->tag("h3",$val,$params);
	}
	public function p($val=null,$params=null){
		return $this->tag("p",$val,$params);
	}
	public function b($val=null,$params=null){
		return $this->tag("strong",$val,$params);
	}
	public function pre($val=null,$params=null){
		return $this->tag("pre",$val,$params);
	}
	public function div($val=null,$params=null){
		return $this->tag("div",$val,$params);
	}
	public function span($val=null,$params=null){
		return $this->tag("span",$val,$params);
	}
	public function ol($val=null,$params=null){
		return $this->tag("ol",$val,$params);
	}
	public function ul($val=null,$params=null){
		return $this->tag("ul",$val,$params);
	}
	public function li($val=null,$params=null){
		return $this->tag("li",$val,$params);
	}
	public function table($val=null,$params=null){
		return $this->tag("table",$val,$params);
	}
	public function thead($val=null,$params=null){
		return $this->tag("thead",$val,$params);
	}
	public function tbody($val=null,$params=null){
		return $this->tag("tbody",$val,$params);
	}
	public function tfoot($val=null,$params=null){
		return $this->tag("tfoot",$val,$params);
	}
	public function th($val=null,$params=null){
		return $this->tag("th",$val,$params);
	}
	public function tr($val=null,$params=null){
		return $this->tag("tr",$val,$params);
	}
	public function td($val=null,$params=null){
		return $this->tag("td",$val,$params);
	}
	/* unique tags */
	public function meta($action=null,$content=null,$type="http-equiv"){
		if ($action) return "\n<meta ".$type."=\"".$action."\" content=\"".$content."\" />";
	}
	public function ico($val=null){
		if ($val && file_exists($val)) return "<link rel=\"icon\" type=\"image/png\" href=\"".$val."\" />";
	}
	public function br($val=null){
		return $val."<br />\n";
	}
	public function ahref($target=null,$val=null,$params=null){
		$params = $this->_sp($params);
		if ($val && $target) return "<a href=\"".$target."\"".$params.">".$val."</a>";
	}
	public function audio($val=null,$type=null,$params=null){
		$params = $this->_sp($params);
		$result = "\n<audio".$params.">";
		$result .= "\n<source src=\"".$val."\" type=\"".$type."\">";
		$result .= "\n</audio>";
		if ($val) return $result;
	}
	public function video($val=null,$type=null,$params=null){
		$params = $this->_sp($params);
		$result = "\n<video".$params.">";
		$result .= "\n<source src=\"".$val."\" type=\"".$type."\">";
		$result .= "\n</video>";
		if ($val) return $result;
	}
	public function img($val=null,$params=null){
		$params = $this->_sp($params);
		if ($val) return "<img src=\"".$val."\"".$params." />";
	}
	/* special tag */
	public function tag_by_fileType($val=null,$type=null,$params=null) {
		$params = $this->_sp($params);
		$data = explode("/",$type);
		if ($data[0] == "audio") {
			return $this->audio($val,$type,"controls=\"controls\"".$params);
		} else if ($data[0] == "video") {
			return $this->video($val,$type,"controls=\"controls\"".$params);
		} else if ($data[0] == "image") {
			return $this->img($val,$params);
		} else {
			return $this->ahref($val,$val,$params);
		}
	}
	public function text2ascii($text=null) {
		$text = strip_tags($text);
		$text = trim($text);
		$text = $this->replace_accent($text);
		$text = preg_replace("/[^[:alnum:]\.]/","-",$text);
		$text = preg_replace("/-+/","-",$text);
		return $text;
	}
	public function error404(){
		$head = $this->title("404 Not Found");
		$body = $this->h1("Not Found").$this->p("The requested URL ".$_SERVER['PHP_SELF']." was not found on this server.");
		return $this->html($this->head($head).$this->body($body));
	}
	public function alertPage($content="") {
		$head = $this->title("Alert !");
		$body = $this->p("Alert !","style='color:red'");
		$body .= $this->h1($content,"style='color:red'");
		return $this->html($this->head($head).$this->body($body));
	}
	public function color($val="",$color="black") {
		if ($val) return "<span style=\"color:".$color."\">".$val."</span>";
	}
	public function comment($val=null){
		if ($val) return "<!--".$val."-->\n";
	}
	public function css($val=null,$external=null){
		$result = "";
		if ($val!=null && $external==null){
			$result = "\n<style type=\"text/css\">\n".$val."\n</style>";
		} elseif($val!=null && $external!=null){
			$result = "\n<link href=\"".$val."\" rel=\"stylesheet\" type=\"text/css\" />";
		}
		if ($val) return $result;
	}
	public function script($val=null,$external=null){
		$result = "";
		if ($val!=null && $external==null){
			$result = "\n<script type=\"text/javascript\">\n".$val."\n</script>";
		} elseif($val!=null && $external!=null && file_exists($val)){
			$result = "\n<script src=\"".$val."\" type=\"text/javascript\"></script>";
		}
		if ($val) return $result;
	}
	/* form tag */
	public function form($val="",$name=null,$method=null,$action=null,$enctype=null){
		$result = "";
		$result = "\n<form";
		if ($name != null) $result .= " id=\"".$name."\" name=\"".$name."\"";
		if ($method != null) $result .= " method=\"".$method."\"";
		if ($action != null) $result .= " action=\"".$action."\"";
		if ($enctype != null) $result .= " enctype=\"".$enctype."\"";
		$result .= ">";
		$result .= $val;
		$result .= "\n</form>";
		if ($val) return $result;
	}
	public function textarea($val=null,$name=null,$sizes=array(4,80),$params=null){
		$result = "\n<textarea ";
		if ($name != null) $result .= "id=\"".$name."\" name=\"".$name."\" ";
		$result .= "rows=\"".$sizes[0]."\" cols=\"".$sizes[1]."\" ";
		if ($params != null) $result .= $params;
		$result .= ">";
		$result .= $val;
		$result .= "</textarea>";
		return $result;
	}
	public function input($type=null,$name=null,$value=null,$params=null){
		$result = "<input ";
		if ($type != null) $result .= "type=\"".$type."\" ";
		if ($name != null) $result .= "id=\"".$name."\" name=\"".$name."\" ";
		if ($value != null) $result .= "value=\"".$value."\" ";
		if ($params != null) $result .= $params." ";
		$result .= "/>";
		if ($type) return $result;
	}
    public function select($options=null,$name=null,$selected=null,$params=null){
		if ($name){$name = " id=\"".$name."\" name=\"".$name."\"";}
		$params = $this->_sp($params);
		return "\n<select".$name.$params.">".$this->options($options,$selected)."\n</select>";
	}
	function options($options=null,$selected=null){
		$optionsList = array();
		foreach($options as $item=>$value){
			if ($value == $selected){
				$optionsList[] =  "<option value=\"".$value."\" selected=\"selected\">".$item."</option>"; 
			} else {
				$optionsList[] =  "<option value=\"".$value."\">".$item."</option>"; 
			}
		}
		return implode("\n",$optionsList);
	}
	public function fieldset($val=null,$params=null){
		return $this->tag("fieldset",$val,$params);
	}
	public function label($val=null,$params=null){
		return $this->tag("label",$val,$params);
	}
	/* utilities */
	function _sp($val=null){
		if ($val) {
			return " ".$val;
		} else {
			return $val;
		}
	}
	public function crypt($word){
		if (function_exists('sha1')){ // Only in PHP 4.3.0+
			return sha1($word);
		} elseif (function_exists('mhash')){ // Only if Mhash library is loaded
			return bin2hex(mhash(MHASH_SHA1, $word));
		} else {
			return md5($word);
		}
	}
	public function num($num){
		if (!preg_match("/^[0-9]+$/",trim($num))) exit("number entry incorrect !".$num);
		return $num;
	}
	public function word($word){
		if ($word && !preg_match("/^[A-Za-z0-9_\/\.\:]+$/",trim($word))) exit("word entry incorrect ! use only 'A-Z a-z 0-9 or _ / . :' chars");
		return $word;
	}
	public function safe($val=null){
		$val = stripslashes($val);
		$dbLink = mysqli_connect(HOSTMYSQL,USERMYSQL,PASSMYSQL,BASEMYSQL) or die("Error ".mysqli_error($link)); 
		$result = mysqli_real_escape_string($dbLink,$val);
		mysqli_close($dbLink);
		return $result;
	}
	public function strWidthLeft($val=null,$size=128){
		$val = strip_tags(html_entity_decode(utf8_decode(trim($val))));
		if (strlen($val) > $size){
			$result = substr($val,0,$size);
			$words = array_filter(explode(" ",$result));
			// delete last word fragment
			if (count($words) > 1) array_pop($words);
			$result = implode(" ",$words)."...";
		} else {
			$result = $val;
		}
		return utf8_encode($result);
	}
	public function strWidth($val=null,$size=64){
		$val = strip_tags(html_entity_decode(utf8_decode(trim($val))));
		$stringLen = strlen($val);
		if ( $stringLen > $size ){
			$e = floor(($size-2) / 2);
			$result = substr($val,0,$e)."..".substr($val,-$e);
		} else {
			$result = $val;
		}
		if (strlen($result) < $size) $result = $result.str_repeat("&nbsp;",$size-strlen($result));
		return utf8_encode($result);
	}
	public function url($val=Null){
		return urlencode($val);
	}
	public function clean_short_url($val=Null,$length=128){
		$val = html_entity_decode($val);
		$val = $this->uly_strip_tags($val);
		$val = $this->replace_accent($val);
		$val = preg_replace('/[^\w\s]/', '', $val);
		$val = preg_replace('/\s+/', ' ', $val);
		if (strlen($val) > $length){
			$result = substr($val,0,$length);
			$words = array_filter(explode(" ",$result));
			// delete last word fragment
			if (count($words) > 1) array_pop($words);
			$result = implode(" ",$words);
		} else {
			$result = $val;
		}
		return urlencode($result);
	}
	public function uly_htmlentities($val=Null){
		return htmlentities($val,ENT_QUOTES,"UTF-8");
	}
	public function uly_strip_tags($val=Null){
		return strip_tags(trim($val));
	}
	public function f4d($val){
		return sprintf("%'-4d",$val);
	}
	public function echo_arr($arr=array()){
		echo "<pre>",print_r($arr,true),"</pre>";
	}
	public function root_url($val=null){
		if (!defined('HTTPSVAL')) {
			define("HTTPSVAL",null);
		} 
		$http = HTTPSVAL ? "https://":"http://";
		if (isset($_SERVER['REQUEST_URI'])) {
			$script = $_SERVER['REQUEST_URI'];
		} elseif (isset($_SERVER['SCRIPT_NAME'])) {
			$script = $_SERVER['SCRIPT_NAME'];
		} elseif (isset($_SERVER['PHP_SELF'])) {
			$script = $_SERVER['PHP_SELF'];
		} else {
			$script = null;
		}
		if ( preg_match("/\//",$script) ){
			$relativPath = array();
			$relativPath = explode("/",$script);
			array_pop($relativPath);
			$localDir = implode("/",$relativPath);
		} else {
			$localDir = "";
		}
		if (isset($_SERVER['HTTP_HOST'])){
			$result = $http.$_SERVER['HTTP_HOST'].$localDir."/"; 
		} elseif ( isset($_SERVER['SERVER_NAME']) ){
			$result = $http.$_SERVER['SERVER_NAME'].$localDir."/";
		} else {
			$result = $localDir."/";
		}
		if (!empty($val)) $result .= $val;
		return $result;
	}
	function replace_accent($val=null) {
	    return strtr(utf8_decode($val),
	    utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
					'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
	}
	public function get_ext($fileName=null){
		$data = explode(".",$fileName);
		$ext = array_pop($data);
		$ext = strtolower($ext);
		return $ext;
	}
	public function format_file_size($octets) {
		$resultat = $octets;
		for ($i=0; $i < 8 && $resultat >= 1024; $i++) {
			$resultat = $resultat / 1024;
		}
		if ($i > 0) {
			return preg_replace('/,00$/', '', number_format($resultat, 2, ',', '')) . ' ' . substr('KMGTPEZY',$i-1,1) . 'o';
		} else {
			return $resultat . ' o';
		}
	}
	public function sp($list){
		return implode(" ", $list);
	}
}
?>
