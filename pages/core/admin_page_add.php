<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class PageAdd extends Admin {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
		$this->tree = New Tree();
	}
	public function add_page_form(){
		$h = $this->h;$t = $this->t;
		$_[] = $h->input("hidden","add",1);
		$_[] = $h->p( $t->w("child page of").$this->tree->pathsSelect(null,1,"page") );
		$_[] = $h->p($t->wr("redirect to link").$h->input("text","link","","size=".(FIELDWIDTH - 10)));
		$_[] = $h->p( $t->wr("type").$t->w("page").$h->select($this->getPageTypes(),"set_type","page").$t->wlr("or add new type").$h->input("text","type").$t->wlr("lang").$h->input("text","lang","en","size=3") );
		$options = array($t->w("date")=>0,$t->w("subject")=>1,$t->w("reverse date")=>2);
		$_[] = $h->p($t->wl("sort articles by").$h->select($options,"sortstyle").$t->wlr("hide").$t->wr("page").$h->input("checkbox","hide","on") );
		$_[] =$h->p($h->span("&lt;head&gt;","class='tag'").$h->span(" &lt;title&gt; ","class='tag'").$h->input("text","name","","size=".(FIELDWIDTH - 10)).$h->span(" &lt;/title&gt; ","class='tag'") );
		$_[] = $h->p( $h->textarea("","head",array(3,FIELDWIDTH) ).$h->span(" &lt;/head&gt;","class='tag'"));
		$_[] = $h->p( $h->span("&lt;body&gt; &lt;header&gt; ".$t->w("top content"),"class='tag'")." ".$h->input("submit","save",$t->w("save") ) );
		$_[] = $h->p( $t->wr("add file").$h->input("file","addfile","","size=30").$t->wlr("or")."[".$h->ahref("admin.php?action=list_files",$t->w("list files"),"target='_blank'")."]");
		$_[] = $h->p( $h->textarea("","content",array(8,FIELDWIDTH) ) );
		$_[] = $h->p( $h->span("&lt;/header&gt; ... ".$t->w("articles")." ... &lt;/body&gt;","class='tag'")." ".$h->input("submit","save",$t->w("save")));
		$inputs = implode("",$_);
		return $h->form($inputs,"page","post","admin.php?action=add_page","multipart/form-data");
	}
	public function add_page(){
		$h = $this->h;$t = $this->t;
		$now = time();
		$date = date("Y-m-d H:i:s",$now);
		$hide = (isset($_POST['hide']))?1:0;
		if (!empty($_FILES['addfile']["name"])) {
			$fileName = $_FILES['addfile']["name"];
			$ext = $h->get_ext($fileName);
			if ( !in_array($ext, $h->extUploadAllowed) && $_SESSION['level'] > 1) exit("file extension not allowed ! ".$ext);// accept all file in level 1
			$fileName = sprintf("%03d",rand(0,999))."_".$h->text2ascii($fileName);
			$sendFile = move_uploaded_file($_FILES['addfile']["tmp_name"],"files/".$fileName);
			if ($sendFile) {
				$query = INSERTINTO.FILES." (FILENAME,USERID,DATEFILE) VALUES (".
										"'"."files/".$fileName."',".$h->num($_SESSION['userid']).
										",'".$date."');";
				$query = $this->q($query);
				$addFile = $h->br($h->tag_by_fileType("files/".$fileName,$_FILES['addfile']["type"]));
			} else {
				$addFile = $h->br($h->color("file ".$fileName." not uploaded!","red"));
			}
		} else {
			$addFile = "";
		}
		if (empty($_POST['link'])) {
			if (empty($_POST['type'])) $_POST['type'] = $_POST['set_type'];
		} else {
			$_POST['type'] = "link";
		}
		if (empty($_POST['name'])){return $h->h3($t->wr("title empty !"));}
		$query = INSERTINTO.PAGES." (NAME,PARENTID,DESCRIPTION,PAGEHEAD,TYPEPAGE,LANG,LINK,SORTSTYLE,DATEPAGE,HIDE,USERID) VALUES (".
											"'".$h->safe($_POST['name'])."',".
											"'".$h->num($_POST['parent'])."',".
											"'".$addFile.$h->safe($_POST['content'])."',".
											"'".$h->safe($_POST['head'])."',".
											"'".$h->safe($_POST['type'])."',".
											"'".$h->safe($_POST['lang'])."',".
											"'".$h->safe($_POST['link'])."',".
											"'".$h->num($_POST['sortstyle'])."',".
											"'".$date."',".
											$hide.",".$_SESSION['userid'].");";
		$this->q($query);
		return $h->h3($t->wx("page").strip_tags($_POST['name']).$t->wl("created !"));
	}
}
?>
