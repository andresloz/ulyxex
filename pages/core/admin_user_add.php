<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class UserAdd extends Admin {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
	}
	public function add_user(){
		$h = $this->h;$t = $this->t;
		/* check if login is in use */
		$now = time();
		$date = date("Y-m-d H:i:s",$now);
		$query = SELECT."ID".FROM.USERS.WHERE."LOGIN = '".$h->safe($_POST['userlogin'])."'";
		$query = $this->q($query);
		if ($this->fetch($query)) return $h->h3($t->w("login in use !"),"class='warning'");
		/* create user */
		$h->word($_POST['userlogin']);
		if (empty($_POST['password'])){
			$result = $h->h3($t->w("password empty !"),"class='warning'");
		} elseif ($_POST['password'] != $_POST['confirm'] || $h->num($_POST['level']) < $_SESSION['level']){
			$result = $h->h3($t->w("wrong password or level !"),"class='warning'");
		} elseif ($_POST['userlogin'] == ""){
			$result = $h->h3($t->w("login field empty !"),"class='warning'");
		} elseif ($_POST['username'] == ""){
			$result = $h->h3($t->w("username field empty !"),"class='warning'");
		} elseif (!preg_match("/^[A-Za-z0-9]+$/",$_POST['userlogin'])){
			$result = $h->h3($t->w("only A-Z a-z 0-9 chars allowed for login value"),"class='warning'");
		} else {
			$query = INSERTINTO.USERS." (LOGIN,PASSWORD,USERNAME,MAIL,DESCRIPTION,USERLEVEL,DATEPAGE,USEROWNER) VALUES (".
											"'".$h->safe($_POST['userlogin'])."','".$h->crypt($_POST['password'])."','".
											$h->safe($_POST['username'])."','".$h->safe($_POST['mail'])."','".
											$h->safe($_POST['description'])."',".$h->num($_POST['level']).",'".$date."',".
											$_SESSION['userid'].")";
			$query = $this->q($query);
			$result = $h->h3($t->wr("user").strip_tags($_POST['username']).$t->wl("created !"));
		}
		return $result;
	}
	public function add_user_form(){
		$h = $this->h;$t = $this->t;
		$_[] = $h->input("hidden","add",1);
		$_[] = $h->p($t->wr("login").$h->input("text","userlogin"));
		$_[] = $h->p($t->wr("password").$h->input("password","password"));
		$_[] = $h->p($t->wr("confirm password").$h->input("password","confirm"));
		$_[] = $h->p( $h->input("checkbox","checkbox",null,"onclick=\"showPassword(['password','confirm'])\"").$t->wl("show password") );
		$scaleLevel = 4;
		$levels = array();
		if ($_SESSION['userid'] == 1) $scaleLevel = 5;
		for ($i=0;$i < ($scaleLevel - $_SESSION['level']);$i++){
			$x = 4 - $i;
			$levels["level ".$x] =  $x;
		}
		$_[] = $h->p($t->wr("level").$h->select($levels,"level"));
		$_[] = $h->p($t->w("name")).$h->p($h->input("text","username"));
		$_[] = $h->p($t->w("description")).$h->p($h->textarea("","description",array(8,FIELDWIDTH)));
		$_[] = $h->p($t->wr("mail").$h->input("text","mail"));
		$_[] = $h->p($h->input("submit","save",$t->w("save")));
		$content = implode("",$_);
		return $h->form($content,"user","post","admin.php?action=add_user");
	}
}
?>
