<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ViewLink extends Ulyxex {
	function __construct($pageId=Null){
		$this->data = $this->link_data($pageId);
	}
	function link_data($pageId){
		$query = SELECT."LINK".FROM.PAGES.WHERE."ID = ".$pageId._AND_."HIDE = 0"._AND_."LINK != ''";
		$query = $this->q($query);
		if ($line = $this->fetch($query)){
			return $line['LINK'];
		} else {
			return "nolink";
		}
	}
}
?>