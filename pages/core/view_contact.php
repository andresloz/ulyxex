<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ViewContact extends Ulyxex {
	public function __construct($paramId=0){
		$this->h = New Htmlz();
		$this->t = New Translate();
		$this->id = $paramId;
		list($this->name,$this->mail) = $this->mail_adress($paramId);
		//~ problème ionos avec make_seed ?
		//~ srand($this->make_seed());
	}
	function mail_adress($paramId){
		if ($this->h->num($paramId) != 0){
			$dat = $this->user_params($paramId);
			return array($dat['USERNAME'],$dat['MAIL']);
		} else {
			$dat = $this->site_params();
			return array("website",$dat['MAIL']);
		}
	}
	function generateRandomString($length = 10) {
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	public function send_mail(){
		if ($this->h->num($_POST[$_SESSION['inputname']]) != $_SESSION['captcha']){
			return $this->h->h1("error captcha");
		}
		preg_match_all("/\@/",$_POST["xeepidrot"],$out);
		if (count($out[0]) > 1){
			return $this->h->h1("mail incorrect");
		}
		if (empty($_POST["xeepidrot"]) or empty($_POST["busejct"]) or empty($_POST["semaseg"])){
			return $this->h->h1("some field is empty");
		}
		$end = " ";
		$mailHeader = "From: ".$_POST["xeepidrot"].$end;
		$mailBody   = "origin : ".getenv("SERVER_NAME").getenv('REQUEST_URI').$end;
		$mailBody  .= "origin IP : ".getenv("REMOTE_ADDR").$end;
		$mailBody  .= "message : ".$_POST["semaseg"].$end;

		if (empty($this->mail) or $this->mail == "none" or $this->mail == "none@none.org"){
			exit("error mail empty or none");
		} else {
			mail($this->mail,$this->h->safe($_POST["busejct"]),$this->h->safe($mailBody),$this->h->safe($mailHeader));
			return $this->h->h1("message ok");
		}
	}
	public function mail_form(){
		$h = $this->h;$t = $this->t;
		$a = rand(1,90);
		$b = rand(1,10);
		$_SESSION['captcha'] = $a + $b;
		$_SESSION['inputname'] = $this->generateRandomString();
		$_[] = $h->p($t->wr("write to").$h->b($this->name));
		$_[] = $h->input("hidden","densamil",1);
		$_[] = $h->p($t->w("your mail")).$h->p($h->input("text","xeepidrot","","size=".FIELDWIDTH));
		$_[] = $h->p($t->w("subject")).$h->p($h->input("text","busejct","","size=".FIELDWIDTH));
		$_[] = $h->p($t->w("message")).$h->p($h->textarea("","semaseg",array(6,FIELDWIDTH)));
		$_[] = $h->p($t->wr("solve").$h->b($a)." + ".$h->b($b)." = ".
		$h->input("text",$_SESSION['inputname'],"?","size='2' style='font-weight:bold;text-align:center;'")."&nbsp; ".$h->input("submit"));
		$inputs = implode("",$_);
		if (empty($this->mail) or $this->mail == "none" or $this->mail == "none@none.org"){
			return $h->h1("no mail");
		} else {
			return $h->form($inputs,"iamlre","post","index.php?k=contact&v=".$this->id);
		}
	}
	function make_seed(){
	  list($usec, $sec) = explode(' ', microtime());
	  return (float) $sec + ((float) $usec * 100000);
	}
}
?>
