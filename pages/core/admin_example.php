<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class Example extends Admin {
	function __construct(){
		$this->h = New Htmlz();
		$this->t = New Translate();
	}
	public function example_data(){
		$h = $this->h;$t = $this->t;
		$data[] = $h->h1("this is the example test admin !");
		$data[] = $h->p("here you can manage example from here");
		return implode("\n",$data);
	}
}
?>