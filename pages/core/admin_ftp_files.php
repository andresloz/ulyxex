<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ViewFiles extends Admin {
	function __construct($dir=null){
		$this->dir = $dir;
		$this->h = New Htmlz();
		$this->t = New Translate();

		if ($_SESSION['level'] < 2) {
			$this->Select = SELECT.FILES.".ID,".FILES.".FILENAME,".USERS.".USERNAME".FROM.FILES.INNERJOIN.USERS._ON_.FILES.".USERID = ".USERS.".ID";
		} else {
			$_[] = "USERID='".$_SESSION['userid']."'";
			$query = $this->q(SELECT."ID,USERNAME".FROM.USERS.WHERE." USEROWNER='".$_SESSION['userid']."'");
			while ( $line = $this->fetch($query) ){
				$_[] = "USERID='".$line['ID']."'";
			}
			$this->Select = SELECT.FILES.".ID,".FILES.".FILENAME,".USERS.".USERNAME".FROM.FILES.INNERJOIN.USERS._ON_.FILES.".USERID = ".USERS.".ID".$this->whereOr($_);
		}
	}
	public function update_ftp_files(){
		$h = $this->h;
		$t = $this->t;
		$data = "";
		$values = $_POST;
		foreach($values as $k=>$v){
			if ( preg_match("/delete_/",$k) ){
				$file = $_POST['file_'.substr($k,7)];
				@unlink($file);
			} elseif ( preg_match("/add_/",$k) ){
				$now = time();
				$date = date("Y-m-d H:i:s",$now);
				$query = INSERTINTO.FILES." (FILENAME,USERID,DATEFILE) VALUES (".
										"'".$_POST['file_'.substr($k,4)]."',".
										$h->num($_POST['userid']).",'".
										$date."');";				
				$query = $this->q($query);
			}
		}
		if (!empty($_FILES['addfile']["name"])) {
			$fileName = $_FILES['addfile']["name"];
			$ext = $h->get_ext($fileName);
			if ( !in_array($ext, $h->extUploadAllowed) && $_SESSION['level'] > 1) exit("file extension not allowed ! ".$ext);// accept all file in level 1
			$sendFile = move_uploaded_file($_FILES['addfile']["tmp_name"],"files/".$fileName);
		}
		return $this->h->h3($t->w("some values have been modified !"),"class='warning'");
	}
	public function notInDataBase(){
		// warning ! this function return all the files in directory with no limit
		$total = 0;
		$h = $this->h;
		$t = $this->t;
		$files = array();
		$filesNotInBase = array();
		$_ = array();
		$query = SELECT."FILENAME".FROM.FILES;
		$query = $this->q($query);
		while ( $line = $this->fetch($query) ) {
			$files[] = $line["FILENAME"];
		}
		$handle = opendir($this->dir);
		while ($file = readdir($handle)){
			if( // don't show this files
				preg_match("/^\./",$file) || // don't show .htaccess etc.
				$file == "index.php" ||
				$file == "index.htm"
				){
				continue;
			} else {
				if (!in_array($this->dir.$file,$files)) {
					$total++;
					$id = rand(10000,99999);
					$size = $h->format_file_size(filesize($this->dir.$file));
					$params = stat($this->dir.$file);
					$date = date("Y-m-d",$params[9]);
					$_[] = $h->p(
						$h->input("hidden","file_".$id,$this->dir.$file).$this->bordered($date).
						$this->bordered($t->wr("delete").$h->input("checkbox","delete_".$id,"on"))." ".
						$this->bordered($t->wr("assign").$h->input("checkbox","add_".$id,"on"))." ".
						$h->ahref($h->root_url($this->dir.$file),$h->root_url($this->dir.$file),"target='_blank'")." : ".$size
					);
				}
			}
		} // end while
		closedir($handle);
		$filesNotInBase = $h->h3($t->w("list of uploaded files by FTP in \"files\" directory you can assign to user"));
		$filesNotInBase .= $t->wr("choose user").$this->array_users(1).$t->wlr("total files").": ".$total.$h->br();
		$filesNotInBase .= $h->input("hidden","update",1);
		$filesNotInBase .= $h->p($h->input("submit","save",$t->w("update")));
		//~ $filesNotInBase .= $t->wr("add file").$h->input("file","addfile","","size=30");
		$filesNotInBase .= implode(" ",$_);
		$filesNotInBase .= $h->p($h->input("submit","save",$t->w("update")));
		$filesNotInBase = $h->form($filesNotInBase,"ftp_files","post","admin.php?action=ftp_files","multipart/form-data");
		return $filesNotInBase;
	}
}
?>
