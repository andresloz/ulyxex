<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
class ViewArticle extends Ulyxex {
	function __construct($artId=0){
		$this->lang = ""; # default val to avoid error
		$this->artId = $artId;
		$this->pageId = 0; // init in function article_data()
		$this->pageType = "";
		$this->h = New Htmlz();
		$this->t = New Translate();
		$this->Select = 	SELECT.
								ARTICLES.".*,".
								USERS.".USERNAME,".
								PAGES.".TYPEPAGE,".
								PAGES.".PAGEHEAD";
		$this->From = 		FROM.
								ARTICLES.
								INNERJOIN.PAGES._ON_.ARTICLES.".PAGEID = ".PAGES.".ID".
								INNERJOIN.USERS._ON_.ARTICLES.".USERID = ".USERS.".ID";
		$this->Cond = array();
		$this->title = "";
		($artId)?$this->article_data = $this->article_data():$this->article_data = "no article !"; # default val to avoid error
	}
	public function article_data($artId=0){
		$h = $this->h;$t = $this->t;
		if ($artId) $this->artId = $artId;
		$this->Cond[] = ARTICLES.".ID = ".$this->artId;
		$this->Cond[] = ARTICLES.".HIDE = 0";
		$this->Cond[] = PAGES.".HIDE = 0";
		$query = $this->Select.$this->From.$this->Where($this->Cond);
		$query = $this->q($query);
		
		if ($line = $this->fetch($query)){
			$this->pageId = $line['PAGEID'];
			$this->pageType = $line['TYPEPAGE'];
			$this->title = $line['SUBJECT'];
			$this->pageHead = $line['PAGEHEAD'];
			$this->lang = $line['LANG'];
			$article = $line['CONTENT'];
			$footer = $h->span($h->ahref("index.php?k=user&amp;v=".$line['USERID'],$line['USERNAME'])." ".$line['DATEPAGE'],"class='contenturl'");
			$footer .= " ".$h->span($h->ahref("index.php?k=".$line['TYPEPAGE']."&amp;v=".$line['PAGEID'],$t->wr("page")."url"),"class='pageurl'");
			$result = $h->article($article.$h->footer($footer),"id='article_".$line['ID']."'");
			return $result;
		} else {
			return $h->h3($t->w("no article !"));
		}
	}
}
?>
