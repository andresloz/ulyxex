<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");
require_once("core/admin_users.php");
/****************************************/
/* page elements						*/
$o_usr = new Users();
$o_usr->level(1);

$userInfos = $o_usr->user_params($_SESSION['userid']);
$css = $h->css("css/".ADMINCSS,"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title($t->wr("users of").$userInfos['USERNAME']);
$head = $h->head($ico.$meta.$css.$title);

$topPage = $h->h1($t->wr("users of").$h->span($userInfos['USERNAME'],"class='username'"));
$topPage .= $h->h2($adminNavigation->links);
$topPage .= $h->h2($h->ahref("admin.php?action=add_user",$t->w("add user"),"class='action-big'"));

$bottomPage = $h->ulyxCredits($firstLine=False);
/****************************************/
/* page data							*/	
$data = isset($_POST['update'])?$o_usr->update_users():"";

$data .= $o_usr->list_users();
/****************************************/
/* show									*/
$bottomPage .= $h->countQ();
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
