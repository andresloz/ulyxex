<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");
require_once("core/admin_page_add.php");
/****************************************/
/* page elements						*/
$o_pag = new PageAdd();
$o_pag->level(2);

$userInfos = $o_pag->user_params($_SESSION['userid']);
$css = $h->css("css/".ADMINCSS,"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title($t->wr("add page of").$userInfos['USERNAME']);
$scripts = $h->script("js/tinymce/tinymce.min.js","external").$h->script("js/tinymce.js","external"); // if script tinyMce
$head = $h->head($ico.$meta.$css.$scripts.$title);

$topPage = $h->h1($t->wr("add page of").$h->span($userInfos['USERNAME'],"class='username'"));
$topPage .= $h->h2($adminNavigation->links);

$bottomPage = $h->ulyxCredits($firstLine=False);
/****************************************/
/* page data							*/			
$data = isset($_POST['add'])?$o_pag->add_page():"";

$data .=  $o_pag->add_page_form();
/****************************************/
/* show									*/
$bottomPage .= $h->countQ();
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
