<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($index)) exit("out of index site");
require_once("core/view_article.php");
/****************************************/
/* page elements						*/
$o_art = new ViewArticle($h->num($paramId));
$o_tree = new Tree();

$css = $h->css($home['CSS'],"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title($o_art->title);

$topPage = $h->nav($o_tree->pagesNavigation($pageId=$o_art->pageId,$pageType=$o_art->pageType),"class='navigation'");

$bottomPage = $h->footer($h->ulyxCredits());
/****************************************/
/* page data							*/	
$data = $o_art->article_data;
/****************************************/
/* show									*/
$head = $h->head($ico.$meta.$css.$title);
$body = $h->body($topPage.$data.$bottomPage,"id='page_".$paramId."'");
$page = $h->html($head.$body,"lang='".$o_art->lang."'");

echo $page;
?>
