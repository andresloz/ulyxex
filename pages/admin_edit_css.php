<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");
require_once("core/admin_edit_css.php");
/****************************************/
/* page elements						*/
$o_editcss = new EditCss();
$o_editcss->level(2);

$userInfos = $o_editcss->user_params($_SESSION['userid']);
$css = $h->css("css/".ADMINCSS,"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title($t->wr("edit css"));
$head = $h->head($ico.$meta.$css.$title);

$topPage = $h->h1($t->wr("edit css"));
$topPage .= $h->h2($adminNavigation->links);

$bottomPage = $h->ulyxCredits($firstLine=False);
/****************************************/
/* page data							*/
if (!empty($_POST['choose'])) {
	 $data = $o_editcss->form_css();
} else {
	$data = $o_editcss->get_css_list();
}
if (!empty($_POST['editcss'])) {
	$data = $o_editcss->updated_css();
}
/****************************************/
/* show									*/
$bottomPage .= $h->countQ();
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
