<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($index)) exit("out of index site");
require_once("core/view_example.php");
/****************************************/
/* page elements						*/
$o_example = New ViewExample($h->word($paramId));
/****************************************/
/* page elements						*/
$o_tree = new Tree();

$css = $h->css($home['CSS'],"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title(strip_tags($o_example->name));
$head = $h->head($ico.$meta.$css.$title);

$topPage = $h->div($o_tree->pagesNavigation($pageId=$paramId,$pageType=$typePage));

$bottomPage = $h->ulyxCredits();
/****************************************/
/* page data							*/	
$data = $o_example->example_data();
/****************************************/
/* show									*/
$body = $h->body($topPage.$data.$bottomPage,"id='example_".$paramId."'");
$page = $h->html($head.$body);

echo $page;
?>