<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($index)) exit("out of index site");
require_once("core/view_link.php");
/****************************************/
/* page elements						*/
$o_link = New ViewLink($h->num($paramId));
/****************************************/
/* page view							*/
if ($o_link->data == "nolink"){
	echo $h->h3($t->w("no link !"),"class='warning'");
} else {
	// redirect link
	header("Location: ".$o_link->data);
}
?>