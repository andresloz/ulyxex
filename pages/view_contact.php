<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($index)) exit("out of index site");
$h->num($paramId);
session_start();
require_once("core/view_contact.php");
/****************************************/
/* page elements						*/
$o_mail = new ViewContact($paramId);
$o_tree = new Tree();
$css = $h->css($home['CSS'],"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title($t->w("contact"));
$head = $h->head($ico.$meta.$css.$title);

$topPage = $h->nav($o_tree->pagesNavigation(),"class='navigation'");

$bottomPage = $h->footer($h->ulyxCredits());
/****************************************/
/* page data 							*/	
$data = $h->p($t->wr("write to").$o_mail->mail,"class='usermail'");
/****************************************/
/* show									*/
$body = $h->body($topPage.$data.$bottomPage,"id='viewcontact_".$paramId."'");
$page = $h->html($head.$body);

echo $page;
?>
