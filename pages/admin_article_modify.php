<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");
require_once("core/admin_article_modify.php");
/****************************************/
/* page vars							*/
/* check post 							*/
$artId = isset($_GET['art'])?$h->num($_GET['art']):$h->num($_POST['art']);
/****************************************/
/* page elements						*/
$o_art = new ArticleModify($artId);
$o_art->level(4);

$userInfos = $o_art->user_params($_SESSION['userid']);
if ($_SESSION['level'] > 1){
	/* check if user own the article 		*/
	$artdat = $o_art->art_params($artId);
	if ($userInfos['ID'] != $artdat['USERID']) exit("you can't modify this article !");
}

$css = $h->css("css/".ADMINCSS,"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title($t->wr("modify article of").$userInfos['USERNAME']);
$scripts = $h->script("js/tinymce/tinymce.min.js","external").$h->script("js/tinymce.js","external"); // if script tinyMce
$head = $h->head($ico.$meta.$css.$scripts.$title);

$topPage = $h->h1( $t->wr("modify article of").$h->span($userInfos['USERNAME'],"class='username'"));
$topPage .= $h->h2($adminNavigation->links);

$bottomPage = $h->ulyxCredits($firstLine=False);
/****************************************/
/* page data							*/
$data = isset($_POST['update'])?$o_art->update_article():"";

$data .= $o_art->data_article();
/****************************************/
/* show									*/
$bottomPage .= $h->countQ();
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
