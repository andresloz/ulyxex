<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");
if ($_SESSION['userid'] != 1) exit("you can't access to this !");
require_once("core/admin_site.php");
/****************************************/
/* page elements						*/
$o_adminSite = New AdminSite();
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$css = $h->css("css/".ADMINCSS,"external");
$ico = $h->ico("favicon.png");
$js = $h->script("js/ulyxex.js","external");
$title = $h->title($t->wr("site by")."admin");
$head = $h->head($ico.$meta.$css.$js.$title);

$topPage = $h->h1($t->wr("site by").$h->span("admin","class='username'"));
$topPage .= $h->h2($adminNavigation->links);

$tinyMceVers = "unknown";
if (file_exists("js/tiny-version.txt")) {
	$Lines = file("js/tiny-version.txt");
	if ($Lines) {
		foreach($Lines as $Line) {
			if (substr($Line, 0, 7) == 'TinyMCE') {
				$tinyMceVers = substr($Line, 8);
			}
		}
	}
}
$bottomPage = $h->h3("Site Infos:");
$_ = array(
	"1"					=> str_pad("HTTP SERVER ",80,"."),
	"address"			=> $_SERVER["SERVER_ADDR"],
	"name"				=> $_SERVER["SERVER_NAME"],
	"user agent"		=> $_SERVER['HTTP_USER_AGENT'],
	"document root"		=> $_SERVER["DOCUMENT_ROOT"],
	"software"			=> $_SERVER["SERVER_SOFTWARE"],
	"protocol"			=> $_SERVER["SERVER_PROTOCOL"],
	"gateway interface"	=> $_SERVER["GATEWAY_INTERFACE"],
	"http accept"		=> $_SERVER["HTTP_ACCEPT"],
	"http accept"		=> $_SERVER["HTTP_ACCEPT"],
	"http accept encoding"	=> $_SERVER["HTTP_ACCEPT_ENCODING"],
	"http accept language"	=> $_SERVER["HTTP_ACCEPT_LANGUAGE"],
	"2"					=> str_pad("MYSQL ",80,"."),
	"mysql-version"		=> $o_adminSite->mysql_version(),
	"host"				=> HOSTMYSQL,
	"base"				=> BASEMYSQL,
	"prefix"			=> $prefixTableMySql,
	"user"				=> USERMYSQL,
	"password"			=> PASSMYSQL,
	"server charset"	=> $o_adminSite->mysql_charset(),
	"3"					=> str_pad("PHP/HTML ",80,"."),
	"php-version"		=> phpversion(),
	"root directory"	=> getcwd(),
	"field width default"		=> FIELDWIDTH,
	"tinyMce-version"   => $tinyMceVers,
);

foreach($_ as $k=>$v) {
	$bottomPage .= $h->p("- ".ucfirst($k)." : ".$h->b($v));
}
$bottomPage .= $h->p("- ".$h->ahref("admin.php?action=phpinfo","More phpinfo"));
$bottomPage .= $h->ulyxCredits($firstLine=False);
/****************************************/
/* page data							*/	
$data = "";
if (!empty($_POST['update'])) $data .= $o_adminSite->update_site_params();
$data .= $o_adminSite->site_params();
$data .= $h->p($h->b("-".str_pad("Site Map & Robots.txt",80,"-")));
$data .= $h->p($h->ahref("admin.php?action=sitemap",$t->wr("add").$t->wr("files").": sitemap.xml & robots.txt","class='action'"));

$data .= $h->p($h->b("-".str_pad("Show All Controllers",80,"-")));
$data .= $h->p($h->ahref("admin.php?action=controllers",$t->w("show controllers"),"class='action'"));

$data .= $h->p($h->b("-".str_pad("Set Default Lang",80,"-")));
$data .= $h->p($h->ahref("admin.php?action=update_lang",$t->w("Update all articles and pages to lang"),"class='action'"));

$data .= $h->p($h->b("-".str_pad("Transfert data to user",80,"-")));
$data .= $h->p($h->ahref("admin.php?action=user_data_transfert",$t->w("Transfert data from user (a) to user (b)"),"class='action'"));
/****************************************/
/* show									*/
$bottomPage .= $h->countQ();
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
