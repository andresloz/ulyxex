 <?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($index)) exit("out of index site");
/****************************************/
/* page elements						*/
require_once("core/view_page.php");
$pg_params = $u->page_params($paramId);
$o_pag = new ViewPage($paramId,$pg_params['SORTSTYLE']);
$o_tree = new Tree();

$css = $h->css($home['CSS'],"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title(strip_tags($pg_params['NAME']));
$pageHead = $pg_params['PAGEHEAD'];
$head = $h->head($ico.$meta.$css.$title.$pageHead);

$topPage = $h->nav($o_tree->pagesNavigation($pageId=$paramId,$pageType=$typePage),"class='navigation'");
$topPage .= $h->header($pg_params['DESCRIPTION']);

$nextPages = $h->p($o_pag->next_pages,"class='pagenumbers'");
$navPages = $h->p($h->ahref("index.php?k=".$typePage."&amp;v=".$paramId,$t->wr("page")."url"),"class='pageurl'");
$credits = $h->ulyxCredits();
$bottomPage = $h->footer($nextPages.$navPages.$credits);
/****************************************/
/* page data							*/	
$data = $o_pag->page_data;
/****************************************/
/* show									*/
$body = $h->body($topPage.$data.$bottomPage,"id='page_".$paramId."'");
$page = $h->html($head.$body,"lang='".$pg_params['LANG']."'");

echo $page;
?>