<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");
require_once("core/admin_user_add.php");
/****************************************/
/* page elements						*/
$o_usr = new UserAdd();
$o_usr->level(1);

$userInfos = $o_usr->user_params($_SESSION['userid']);
$css = $h->css("css/".ADMINCSS,"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$js = $h->script("js/ulyxex.js","external");
$title = $h->title($t->wr("add user of").$userInfos['USERNAME']);
$head = $h->head($ico.$meta.$css.$js.$title);

$topPage = $h->h1($t->wr("add user of").$h->span($userInfos['USERNAME'],"class='username'"));
$topPage .= $h->h2($adminNavigation->links);

$bottomPage = $h->ulyxCredits($firstLine=False);
/****************************************/
/* page data							*/			
$data = isset($_POST['add'])?$o_usr->add_user():"";

$data .= $o_usr->add_user_form();
/****************************************/
/* show									*/
$bottomPage .= $h->countQ();
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
