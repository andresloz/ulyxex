<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($index)) exit("out of index site");
$h->num($paramId);
session_start();
require_once("core/view_contact.php");
/****************************************/
/* page elements						*/
$o_mail = new ViewContact($paramId);
$o_tree = new Tree();
$css = $h->css($home['CSS'],"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$meta .= $h->meta("googlebot","noarchive");
$meta .= $h->meta("cache-control","no-cache");
$meta .= $h->meta("pragma","no-cache");
$meta .= $h->meta("cache","no store");
$meta .= $h->meta("expires","0"); 
$title = $h->title($t->w("contact"));
$noscript = "\n<object><noscript><meta http-equiv='refresh' content='0;URL=404.htm' /></noscript></object>";
$script = $h->script("if (!navigator.cookieEnabled){location.href = '404.htm' }");

$head = $h->head($ico.$meta.$script.$noscript.$css.$title);

$topPage = $h->nav($o_tree->pagesNavigation(),"class='navigation'");

$bottomPage = $h->footer($h->ulyxCredits());
/****************************************/
/* page data with mail server			*/	
$data = "";
if (isset($_POST['densamil'])){
	$data .= $o_mail->send_mail();
} else {
	$data .= $o_mail->mail_form();
}
/****************************************/
/* show									*/
$body = $h->body($topPage.$data.$bottomPage,"id='viewcontact_form_".$paramId."'");
$page = $h->html($head.$body);

echo $page;
?>
