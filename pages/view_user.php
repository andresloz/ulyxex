<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($index)) exit("out of index site");
require_once("core/view_user.php");
/****************************************/
/* page elements						*/
$o_user = new ViewUser();
$o_tree = new Tree();
$css = $h->css($home['CSS'],"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title($t->w("user"));
$head = $h->head($ico.$meta.$css.$title);

$topPage = $h->nav($o_tree->pagesNavigation(),"class='navigation'");

$bottomPage = $h->footer($h->ulyxCredits());
/****************************************/
/* page data							*/	
$data = $o_user->user_data($paramId);
$data .= $o_user->user_links($paramId);
/****************************************/
/* show									*/
$body = $h->body($topPage.$data.$bottomPage,"id='viewuser_".$paramId."'");
$page = $h->html($head.$body);

echo $page;
?>