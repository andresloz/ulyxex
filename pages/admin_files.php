<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");
require_once("core/admin_files.php");
/****************************************/
/* page elements						*/
$o_files = new ViewFiles("files/");
$o_files->level(4);

$userInfos = $o_files->user_params($_SESSION['userid']);
$css = $h->css("css/admin_style.css","external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title($t->wr("files of").$userInfos['USERNAME']);
$head = $h->head($ico.$meta.$css.$title);

$topPage = $h->h1($t->wr("files of").$h->span($userInfos['USERNAME'],"class='username'"));
$topPage .= $h->h2($adminNavigation->links);

$bottomPage = $h->ulyxCredits($firstLine=False);

$data = isset($_POST['update'])?$o_files->update_files():"";

$data .= $o_files->getFilesUrlInput();
/****************************************/
/* show									*/
$bottomPage .= $h->countQ();
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);
echo $page;
?>
