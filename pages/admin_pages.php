<?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");

require_once("core/admin_pages.php");
/****************************************/
/* page elements						*/
$o_pag = new Pages();
$o_pag->level(2);

$userInfos = $o_pag->user_params($_SESSION['userid']);
$css = $h->css("css/".ADMINCSS,"external");
$ico = $h->ico("favicon.png");
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$title = $h->title($t->wr("pages of").$userInfos['USERNAME']);
$head = $h->head($ico.$meta.$css.$title);

$topPage = $h->h1($t->wr("pages of").$h->span($userInfos['USERNAME'],"class='username'"));
$topPage .= $h->h2($adminNavigation->links);
$topPage .= $h->h2($h->ahref("admin.php?action=add_page",$t->w("add page"),"class='action-big'"));

$bottomPage = $h->ulyxCredits($firstLine=False);
/****************************************/
/* page data							*/	
$data = isset($_POST['update'])?$o_pag->update_pages():"";

$data .= $o_pag->list_pages();
/****************************************/
/* show									*/
$bottomPage .= $h->countQ();
$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
