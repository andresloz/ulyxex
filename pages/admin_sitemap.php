 <?php
/* Ulyxex version 1.5.4.4 ***************/
/* code http://ulyxex.logz.org **********/
/* Andre Lozano http://andre-lozano.org */
if (!isset($admin)) exit("not in admin !");
if ($_SESSION['userid'] != 1) exit("you can't access to this !");
require_once("core/admin_site.php");
/****************************************/
/* page elements						*/
$o_adminSite = New AdminSite();
$meta = $h->meta("Content-Type","text/html; charset=UTF-8");
$meta .= $h->meta("viewport","width=device-width, initial-scale=1.0","name");
$css = $h->css("css/".ADMINCSS,"external");
$ico = $h->ico("favicon.png");
$js = $h->script("js/ulyxex.js","external");
$title = $h->title($t->wr("site by")."admin");
$head = $h->head($ico.$meta.$css.$js.$title);

$topPage = $h->h1($t->wr("site by").$h->span("admin","class='username'"));
$topPage .= $h->h2($adminNavigation->links);
/****************************************/
/* sitemap elements						*/
require_once("core/admin_sitemap.php");


/****************************************/
/* sitemap data							*/	
$x = new XmlSitemap();
$o_sitemap = new SiteMap();
$data_sitemap = $o_sitemap->sitemap_page_data();
$data_sitemap .= $o_sitemap->sitemap_article_data();
/****************************************/
/* create								*/
$sitemap = $x->urlset($data_sitemap);
$data = "";
if ($f = fopen("sitemap.xml","w")){
	fwrite($f,$sitemap);
	fclose($f);
	$data .= $h->h3($t->w("created !")." sitemap.xml","class='warning'");
	$data .= $h->p($t->wr("link").$h->ahref($h->root_url("sitemap.xml"),"sitemap.xml","target='blank'"));
} else {
	exit("Can't write sitemap.xml");
}
if ($f = fopen("robots.txt","w")){
	fwrite($f,"User-agent: *\nAllow: /\nDisallow: /css/\nDisallow: /js/\nDisallow: /pages/\n\nSitemap: ".$h->root_url("sitemap.xml")."\n");
	fclose($f);
	$data .= $h->h3($t->w("created !")." robots.txt","class='warning'");
	$data .= $h->p($t->wr("link").$h->ahref($h->root_url("robots.txt"),"robot.txt","target='blank'"));
} else {
	exit("Can't write robots.txt");
}
$bottomPage = $h->ulyxCredits($firstLine=False);
$bottomPage .= $h->countQ();

$body = $h->body($topPage.$data.$bottomPage);
$page = $h->html($head.$body);

echo $page;
?>
